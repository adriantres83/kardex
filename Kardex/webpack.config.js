const path = require('path');
const webpack = require('webpack');

module.exports = {
    devtool: 'source-map',
    entry: {
        global: './WebContent/ts/global.index',
        inicio:  './WebContent/ts/general/inicio',
        kardex:  './WebContent/ts/operaciones/kardex',
        error: './WebContent/ts/general/error',
        mensajes: './WebContent/ts/general/mensajes',
        bloqueo_pantalla: './WebContent/ts/bloqueo_pantalla',
        template: './WebContent/ts/template',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'WebContent/js'),
        library: "[name]",
        libraryTarget: "var"
    },
    resolve: {
        // Add '.ts' and '.tsx' as a resolvable extension.
        extensions: ['.ts', '.js']
    },
    module: {
        loaders: [
            // all files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'
            {
                test: /\.ts?$/,
                loader: 'ts-loader',

            }
        ],
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules|target/,
            },
        ]
    },
    plugins:[
        new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            sourceMap: true,
            include: /\.min\.js$/,
        })
    ],
    target: 'web'
};