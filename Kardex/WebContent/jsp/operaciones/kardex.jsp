<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:url var="home" value="/" scope="request"/>

<script src=<c:url value="/js/jquery-1.12.3.min.js"/>></script>

<!--main content start-->
<section id="main-content">
    <!--wrapper start-->
    <section class="wrapper">
        <form:form class="form-inline" role="form" id="kardexForm" autocomplete="off"
                   name="kardexForm" modelAttribute="kardex" action="kardexs">
        <!-- CONTENIDO -->
        <div class="row mt">
            <!-- col-md-12 start -->
            <div class="col-md-12">
                <!-- content-panel -->
                <div class="form-panel" style="overflow-x: auto">
                    <br/>
                    <table>
                        <tr>
                            <td>
                                
                                
                                <!-- boton nuevo -->
                                <button type="button" class="btn btn-theme" id="nuevoBtn"
                                        data-toggle="modal" data-target="#myModal">
                                    <spring:message code="nuevo" text="default text"/>
                                </button>
                                
                            <td>
                                <div id="loading2" hidden="hidden">
                                    &nbsp;<i class="fa fa-circle-o-notch fa-spin"
                                             style="font-size: 20px"></i>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br/><br/>

                        <div class="tabla1 base"
                             style="float: left; width:calc(100% - 80px); min-height: 72px;">
                            <table class="table table-bordered table-striped table-condensed">
                                <thead>
                                <tr>
                                    <!-- Mensajes para los Tooltips -->
                                    <spring:message code="kardexFechaTooltip" var="kardexFechaTooltip"/>
                                    <spring:message code="kardexProductoTooltip" var="kardexProductoTooltip"/>
                                    <spring:message code="kardexTipoOperacionTooltip" var="kardexTipoOperacionTooltip"/>
                                    <spring:message code="kardexCantidadEntradaTooltip" var="kardexCantidadEntradaTooltip"/>
                                    <spring:message code="kardexValorUnitarioEntradaTooltip" var="kardexValorUnitarioEntradaTooltip"/>
                                    <spring:message code="kardexValorTotalEntradaTooltip" var="kardexValorTotalEntradaTooltip"/>
                                    <spring:message code="kardexCantidadSalidaTooltip" var="kardexCantidadSalidaTooltip"/>
                                    <spring:message code="kardexValorUnitarioSalidaTooltip" var="kardexValorUnitarioSalidaTooltip"/>
                                    <spring:message code="kardexValorTotalSalidaTooltip" var="kardexValorTotalSalidaTooltip"/>
                                    <spring:message code="acciones" var="accionesV"/>
                                    <spring:message code="accionesTooltip" var="accionesTooltipV"/>

                                    <!-- Mensajes para los titulos -->
                                    <spring:message code="kardexFecha" var="kardexFecha"/>
                                    <spring:message code="kardexProducto" var="kardexProducto"/>
                                    <spring:message code="kardexTipoOperacion" var="kardexTipoOperacion"/>
                                    <spring:message code="kardexCantidadEntrada" var="kardexCantidadEntrada"/>
                                    <spring:message code="kardexValorUnitarioEntrada" var="kardexValorUnitarioEntrada"/>
                                    <spring:message code="kardexValorTotalEntrada" var="kardexValorTotalEntrada"/>
                                    <spring:message code="kardexCantidadSalida" var="kardexCantidadSalida"/>
                                    <spring:message code="kardexValorUnitarioSalida" var="kardexValorUnitarioSalida"/>
                                    <spring:message code="kardexValorTotalSalida" var="kardexValorTotalSalida"/>
                                    
                                    <!-- Mensajes para los Tooltips del icono lapiz -->
                                    <spring:message 
                                    		code="TAfcPlanilla_detalle_title_tooltip" var="modificarTitleTooltip" />
                                    <spring:message 
                                    		code="modificar" var="modificarTooltip" />

                                    <th>
                                    	<label class="control-label" id="kardexFecha"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexFechaTooltip}"
                                               data-content="${kardexFechaTooltip}">
                                        	<spring:message code="kardexFecha" text="default text"/>
                                    	</label>
                                    </th>
									<th>
                                    	<label class="control-label" id="kardexProducto"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexProductoTooltip}"
                                               data-content="${kardexProductoTooltip}">
                                        	<spring:message code="kardexProducto" text="default text"/>
                                    	</label>
                                    </th>
                                    <th>
                                    	<label class="control-label" id="kardexTipoOperacion"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexTipoOperacionTooltip}"
                                               data-content="${kardexTipoOperacionTooltip}">
                                        	<spring:message code="kardexTipoOperacion" text="default text"/>
                                    	</label>
                                    </th>
                                    <th>
                                    	<label class="control-label" id="kardexCantidadEntrada"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexCantidadEntradaTooltip}"
                                               data-content="${kardexCantidadEntradaTooltip}">
                                        	<spring:message code="kardexCantidadEntrada" text="default text"/>
                                    	</label>
                                    </th>
                                    <th>
                                    	<label class="control-label" id="kardexValorUnitarioEntrada"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexValorUnitarioEntradaTooltip}"
                                               data-content="${kardexValorUnitarioEntradaTooltip}">
                                        	<spring:message code="kardexValorUnitarioEntrada" text="default text"/>
                                    	</label>
                                    </th>
                                    <th>
                                    	<label class="control-label" id="kardexValorTotalEntrada"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexValorTotalEntradaTooltip}"
                                               data-content="${kardexValorTotalEntradaTooltip}">
                                        	<spring:message code="kardexValorTotalEntrada" text="default text"/>
                                    	</label>
                                    </th>
                                    <th>
                                    	<label class="control-label" id="kardexCantidadSalida"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexCantidadSalidaTooltip}"
                                               data-content="${kardexCantidadSalidaTooltip}">
                                        	<spring:message code="kardexCantidadSalida" text="default text"/>
                                    	</label>
                                    </th>
                                    <th>
                                    	<label class="control-label" id="kardexValorUnitarioSalida"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexValorUnitarioSalidaTooltip}"
                                               data-content="${kardexValorUnitarioSalidaTooltip}">
                                        	<spring:message code="kardexValorUnitarioSalida" text="default text"/>
                                    	</label>
                                    </th>
                                    <th>
                                    	<label class="control-label" id="kardexValorTotalSalida"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexValorTotalSalidaTooltip}"
                                               data-content="${kardexValorTotalSalidaTooltip}">
                                        	<spring:message code="kardexValorTotalSalida" text="default text"/>
                                    	</label>
                                    </th>
                                    <th>
                                    	<label class="control-label" id="kardexCantidadSaldo"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexCantidadSaldoTooltip}"
                                               data-content="${kardexCantidadSaldoTooltip}">
                                        	<spring:message code="kardexCantidadSaldo" text="default text"/>
                                    	</label>
                                    </th>
                                    <th>
                                    	<label class="control-label" id="kardexValorUnitarioSaldo"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexValorUnitarioSaldoTooltip}"
                                               data-content="${kardexValorUnitarioSaldoTooltip}">
                                        	<spring:message code="kardexValorUnitarioSaldo" text="default text"/>
                                    	</label>
                                    </th>
                                    <th>
                                    	<label class="control-label" id="kardexValorTotalSaldo"
                                               data-toggle="popover" data-placement="top"
                                               title="${kardexValorTotalSaldoTooltip}"
                                               data-content="${kardexValorTotalSaldoTooltip}">
                                        	<spring:message code="kardexValorTotalSaldo" text="default text"/>
                                    	</label>
                                    </th>
                                    
                               		</tr>
                                </thead>
                            </table>
                        </div>
                        <div class="tabla3 base4"
                             style="float: right; width: 80px; min-height: 72px;">
                            <table class="table table-bordered table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th style="width: 60px;">
                                        <div style="height: 60px;">
                                            <label class="control-label" id="Acciones"
                                                   data-toggle="popover" data-placement="top"
                                                   title="${accionesV}" data-content="${accionesTooltipV}">
                                                <spring:message code="acciones" text="default text"/>
                                            </label>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <c:if test="${lstKardex.size() == 0 }">
                            <div class="tabla2 base2"
                                 style="float: left; width: calc(100% - 80px); height: 15px;">
                                <table class="table table-bordered table-striped table-condensed">
                                    <tr>
                                        <td>
                                            <div style="width: 400px;"></div>
                                        </td>
                                        <td>
                                            <div style="width: 400px;"></div>
                                        </td>
                                        <td>
                                            <div style="width: 300px;"></div>
                                        </td>
                                        <td>
                                            <div style="width: 200px;"></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </c:if>
                        <c:if test="${lstKardex.size() > 0 }">
                            <div class="tabla2 base2"
                                 style="float: left; width: calc(100% - 80px); padding-top: 0;">
                                <table class="table table-bordered table-striped table-condensed">
                                    <tbody>
                                    <c:forEach items="${lstKardex}" var="kardex">
                                        <tr id="${kardex.idKardex}">
                                            <td>
                                                <div style="height: 26px; width: 400px;">
                                                    <c:out value="${kardex.fecha}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 400px;">
                                                    <c:out value="${kardex.producto}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 400px;">
                                                    <c:out value="${kardex.tipoOperacion}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 200px;">
                                                    <c:out value="${kardex.cantidadEntrada}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 200px;">
                                                    <c:out value="${kardex.valorUnitarioEntrada}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 200px;">
                                                    <c:out value="${kardex.valorTotalEntrada}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 200px;">
                                                    <c:out value="${kardex.cantidadSalida}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 200px;">
                                                    <c:out value="${kardex.valorUnitarioSalida}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 200px;">
                                                    <c:out value="${kardex.valorTotalSalida}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 200px;">
                                                    <c:out value="${kardex.cantidadSaldo}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 200px;">
                                                    <c:out value="${kardex.valorUnitarioSaldo}"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 26px; width: 200px;">
                                                    <c:out value="${kardex.valorTotalSaldo}"/>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>

                            <div class="tabla4 base5"
                                 style="float:left; width:80px; padding-top: 0;">
                                <table class="table table-bordered table-striped table-condensed">
                                    <tbody>
                                    <c:forEach items="${lstKardex}" var="kardex">
                                        <tr>
                                            <td align="center">
                                                <div style="height: 26px;">
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </c:if>

                        <!-- Modal -->
                        <div aria-hidden="true"
                             aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
                             id="myModal" class="modal fade in" data-keyboard="false"
                             data-backdrop="static">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" id="closeBtn2" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                        </button>
                                        <h4 class="modal-title">
                                            <spring:message code="kardexs_title" text="default text"/>
                                        </h4>
                                    </div>
                                    <!-- Popup -->
                                    <div id="feedback"></div>
                                    <div class="modal-body">
                                        <div class="form-group" style="min-width: 100%;">
                                            <form:input class="form-control" id="idKardex" path="idKardex"
                                                        type="hidden"/>
                                            

                                            <!-- Producto -->
                                            <label class="col-sm-3 control-label" id="kardexProducto"
                                                   data-toggle="popover" data-placement="top"
                                                   title="${kardexProductoTooltip}"
                                                   data-content="${kardexProductoTooltip}">
                                                <spring:message code="kardexProducto" text="default text"/>
                                                <span style="color: red"><c:out value="  *"/></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <form:input class="form-control" id="producto" path="producto"
                                                            maxlength="20" readonly="true"/>
                                                <span>
													<form:errors path="producto"/>
												</span>
                                            </div>
                                            <br/><br/><br/>

                                            <!-- Tipo operacion -->
                                            <label class="col-sm-3 control-label" id="kardexTipoOperacion"
                                                   data-toggle="popover" data-placement="top"
                                                   title="${kardexTipoOperacionTooltip}"
                                                   data-content="${kardexTipoOperacionTooltip}">
                                                <spring:message code="kardexTipoOperacion" text="default text"/>
                                                <span style="color: red"><c:out value="  *"/></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <form:input class="form-control" id="tipoOperacion" path="tipoOperacion"
                                                            maxlength="20" readonly="true"/>
                                                <span>
													<form:errors path="tipoOperacion"/>
												</span>
                                            </div>
                                            <br/> <br/> <br/>
											
											<c:if test="${tipoOperacion eq 'Comprar'}">
												<!-- Cantidad entrada -->
	                                            <label class="col-sm-3 control-label" id="kardexCantidadEntrada"
	                                                   data-toggle="popover" data-placement="top"
	                                                   title="${kardexCantidadEntradaTooltip}"
	                                                   data-content="${kardexCantidadEntradaTooltip}">
	                                                <spring:message code="kardexCantidadEntrada" text="default text"/>
	                                                <span style="color: red"><c:out value="  *"/></span>
	                                            </label>
	                                            <div class="col-sm-7">
	                                                <form:input class="form-control" id="cantidadEntrada" path="cantidadEntrada"
	                                                            maxlength="20"/>
	                                                <span>
														<form:errors path="cantidadEntrada"/>
													</span>
	                                            </div>
	                                            <br/> <br/> <br/>
	                                            <!-- Valor total entrada -->
	                                            <label class="col-sm-3 control-label" id="kardexValorTotalEntrada"
	                                                   data-toggle="popover" data-placement="top"
	                                                   title="${kardexValorTotalEntradaTooltip}"
	                                                   data-content="${kardexValorTotalEntradaTooltip}">
	                                                <spring:message code="kardexValorTotalEntrada" text="default text"/>
	                                                <span style="color: red"><c:out value="  *"/></span>
	                                            </label>
	                                            <div class="col-sm-7">
	                                            	<fmt:formatNumber type="currency" value="${valorTotalEntrada}" var="valorTotalEntradaNumber"/>
			                                        <form:input tabindex="5" class="form-control caracteres_moneda goright" path="valorTotalEntrada" value="${valorTotalEntradaNumber}"
			                                                    id="valorTotalEntrada"
			                                                    type="text"
			                                                    maxlength="22"/>
	                                                
	                                                <span>
														<form:errors path="valorTotalEntrada"/>
													</span>
	                                            </div>
	                                            <br/> <br/> <br/>
	                                            <form:input class="form-control" id="cantidadSalida" path="cantidadSalida"
                                                        type="hidden"/>
											</c:if>
											<c:if test="${tipoOperacion eq 'Vender'}">
												<!-- Cantidad salida -->
	                                            <label class="col-sm-3 control-label" id="kardexCantidadSalida"
	                                                   data-toggle="popover" data-placement="top"
	                                                   title="${kardexCantidadSalidaTooltip}"
	                                                   data-content="${kardexCantidadSalidaTooltip}">
	                                                <spring:message code="kardexCantidadSalida" text="default text"/>
	                                                <span style="color: red"><c:out value="  *"/></span>
	                                            </label>
	                                            <div class="col-sm-7">
	                                                <form:input class="form-control" id="cantidadSalida" path="cantidadSalida"
	                                                            maxlength="20"/>
	                                                <span>
														<form:errors path="cantidadSalida"/>
													</span>
	                                            </div>
	                                            <br/> <br/> <br/>
	                                            <form:input class="form-control" id="cantidadEntrada" path="cantidadEntrada"
                                                        type="hidden"/>
                                                <form:input class="form-control" id="valorTotalEntrada" path="valorTotalEntrada"
                                                        type="hidden"/>
											</c:if>
                                            
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal"
                                                    class="btn btn-default" id="btnCancelar" type="button">
                                                <spring:message code="cancelar" text="default text"/>
                                            </button>
                                            
                                            <button type="button" class="btn btn-theme" id="btnGuardar"
                                                    data-toggle="modal" data-target="#myModalConfirmar"
                                                    disabled="disabled">
                                                <spring:message code="guardar" text="default text"/>
                                            </button>
                                            
                                            <div id="loading" align="center" hidden="hidden">
                                                &nbsp; <i class="fa fa-circle-o-notch fa-spin" style="font-size: 20px"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Fin modal -->

                        <!-- Modal confirmacion -->
                        <div aria-hidden="true"
                             aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
                             id="myModalConfirmar" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" id="closeBtn3" class="close" data-dismiss="modal">&times;
                                        </button>
                                        <h4 class="modal-title">
                                            <spring:message code="confirmacion" text="default text"/>
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <h5>
                                            <spring:message code="desea_guardar" text="default text"/>
                                        </h5>
                                        <p class="text-warning">
                                            <small><spring:message code="si_no_guarda" text="default text"/></small>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button"
                                                id="btnCancelar" class="btn btn-default"
                                                data-dismiss="modal">
                                            <spring:message code="cancelar" text="default text"/>
                                        </button>
                                        <button type="button" class="btn btn-theme" id="btnGuardar"
                                                data-toggle="modal">
                                            <spring:message code="aceptar" text="default text"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Fin modal confirmación-->
                        <c:if
                                test="${lstKardex.size() eq 0 && hayDatosAMostrar eq true}">
                            <div class="div_tabla" style="width: 100%;">
                                <table
                                        class="table table-bordered table-striped table-condensed">
                                    <tr>
                                        <td colspan="4"><spring:message code="noHayDatos" text="default text"/></td>
                                    </tr>
                                </table>
                            </div>
                        </c:if>

                        <input type="hidden" name="consultarTabla"
                               id="consultarTabla" value="${consultarTabla}">
                        <input type="hidden" name="iEndPage"
                               value="${iEndPage}"/>
                        <input type="hidden" name="lonPage"
                               value="${lonPage}"/>
                        <input type="hidden" name="iStartPage"
                               value="${iStartPage}"/>
                        <input type="hidden" name="iUltimaPagina"
                               value="${iUltimaPagina}"/>
                        <input type="hidden"
                               name="lonTotalRegistrosConsulta"
                               value="${lonTotalRegistrosConsulta}">
                        <input type="hidden" name="sizeLista"
                               id="sizeLista" value="${sizeLista}">
                        <input type="hidden" name="idKardexEliminar">
                        <input type="hidden" name="consultaPaginacion" id="consultaPaginacion"> 
                    </form:form>

                    <!-- Total de registros -->
                    <c:if test="${lstKardex.size() > 0}">
                        <div class="footer"
                             style="width: 100%; overflow-x: hidden; min-height: 1px; ">
                            <div style="overflow-x: hidden;"
                                 class="col-sm-4">
                                <i class="fa fa-angle-right"></i>
                                <spring:message code="en_total_hay" text="default text"/>
                                    ${sessionScope.lonTotalRegistrosConsulta}
                                <spring:message code="registros" text="default text"/>
                            </div>
                        </div>
                    </c:if>
                    <!--Fin Total de registros -->

                    <!-- div que permite que el div padre envuelva a los div hijos -->
                    <div id="clear" style="clear: both;"></div>
                    <!-- Paginador -->
                    <div align="center">
                        <nav>
                        <c:if
                                test="${lstKardex.size() != 0}">
                            <ul class="pagination">
                                <c:if test="${sessionScope.lonPage > 1}">
                                    <li><a aria-label="Previous"
                                           style="cursor: pointer" class="pagination-log" data-page=1><span
                                            aria-hidden="true">&laquo;</span></a>
                                    </li>
                                </c:if> <c:forEach begin="${sessionScope.iStartPage}" end="${sessionScope.iEndPage}" var="p">
                                <c:choose>
                                    <c:when test="${sessionScope.lonPage eq p}">
                                        <li class="active">
                                            <a style="cursor: pointer"
                                               class="pagination-log" data-page="${p}"><span
                                                    aria-hidden="true">${p}</span> </a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li><a style="cursor: pointer"
                                               class="pagination-log" data-page="${p}">
                                            <span aria-hidden="true">${p}</span> </a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach> <c:if
                                    test="${sessionScope.iUltimaPagina > 0}">
                                <c:if
                                        test="${sessionScope.lonPage != sessionScope.iUltimaPagina}">
                                    <li><a aria-label="Next"
                                           style="cursor: pointer"
                                           class="pagination-log" data-page="${sessionScope.iUltimaPagina}"><span
                                            aria-hidden="true">&raquo;</span> </a></li>
                                </c:if>
                            </c:if>
                            </ul>
                            </c:if>
                        </nav>
                    </div>

                </div>
                <script src=<c:url value="/js/kardex.js?v=1.0.0"/>></script>
            </div>
            <!-- col-md-12 end-->
        </div>
        <!-- FIN CONTENIDO -->
    </section>
    <!--wrapper end-->
</section>
<!--main content end-->
