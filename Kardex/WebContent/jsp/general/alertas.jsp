<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${not empty error_type and not empty message}">
	<!-- error -->
	<c:if test="${error_type eq 'E'}">
	<br>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
			<button type="button" class="close" id="closeBtn" data-dismiss="alert" aria-label="Cerrar">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>Error. <br/><c:out value="${message}"/></strong> 
		</div>
	</c:if>
	<!-- alerta -->
	<c:if test="${error_type eq 'A'}">
	<br>
		<div class="alert alert-warning alert-dismissible" role="alert">
			<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
			<button type="button" id="closeBtn" class="close" data-dismiss="alert" aria-label="Cerrar">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>Alerta. <br><c:out value="${message}"/></strong> 
		</div>
	</c:if>
	<!-- confirmacion -->
	<c:if test="${error_type eq 'C'}">
	<br>
		<div class="alert alert-success alert-dismissible" role="alert">
			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
			<button type="button" class="close" id="closeBtn" data-dismiss="alert" aria-label="Cerrar">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>Exitoso. <br><c:out value="${message}"/></strong> 
		</div>
	</c:if>
	<c:if test="${error_type eq 'CA'}">
	<br>
		<div class="alert alert-success alert-dismissible" role="alert">
			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
			<button type="button" class="close" id="closeBtn" data-dismiss="alert" aria-label="Cerrar">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>Exitoso. <br> <c:out value="${message}"/></strong> 
		</div>
		<div class="alert alert-warning alert-dismissible" role="alert">
			<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
			<button type="button" id="closeBtn" class="close" data-dismiss="alert" aria-label="Cerrar">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>Alerta. <br> <c:out value="${message1}"/></strong> 
		</div>
	</c:if>
	<!-- confirmacion dividir planilla-->
	<c:if test="${error_type eq 'CDP'}">
	<br>
		<div class="alert alert-success alert-dismissible" role="alert">
			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
			<button type="button" class="close" id="closeBtn" data-dismiss="alert" aria-label="Cerrar">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>
				Exitoso. <br>
				<c:if test="${sessionScope['mapMenuPermisos']['5']['consultar'] == 'S'}">
					<c:out value="${message}"/>
					<a id="btPlanilla" target="_blank" lang="<c:out value="${codigo}"/>" style="color: #3c763d; text-decoration: underline; cursor: pointer;">
						ver planillas procesadas
					</a>
				</c:if>
				<c:if test="${sessionScope['mapMenuPermisos']['5']['consultar'] != 'S'}">
					<c:out value="${message}"/>
				</c:if>
			</strong>
		</div>
	</c:if>
</c:if>
