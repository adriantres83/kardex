<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="../img/afc_logo.ico">

    <title><spring:message code="titulo_Inicio_sistema" text="default text"/></title>
    <!-- JAVASCRIPT -->
    <script src="<c:url value='/js/jquery.js'/>"></script>
    <script src="<c:url value='/js/bootstrap.min.js'/>"></script>
    <script src="<c:url value='/js/jquery-1.12.3.min.js'/>"></script>
    <script src="<c:url value='/js/jquery.min.js'/>"></script>
    <script src="<c:url value='/js/jquery-ui.min.js'/>"></script>

    <!-- CSS -->
    <link href="<c:url value='/css/bootstrap-iso.css'/>" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<c:url value='/css/bootstrap.css'/>">
    <link rel="stylesheet" href="<c:url value='/font-awesome/css/font-awesome.css'/>">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<c:url value='/css/style.css'/>">
    <link rel="stylesheet" href="<c:url value='/css/style-responsive.css'/>">
    <script>
        window.vars = {
            mensaje: ${mensaje ne null}
        };
    </script>
</head>

<body>

<!-- **********************************************************************************************************************************************************
MAIN CONTENT
*********************************************************************************************************************************************************** -->

<div id="login-page">
    <div class="container">

        <form class="form-login" action="/Kardex/j_spring_security_check" method="post">
            <h2 class="form-login-heading">
                <img class="logoAplicacion" src="<c:url value="/img/afc_logo.png"/>">&nbsp;<spring:message code="Way2Home" text="default text"/>
            </h2>
            <div class="login-wrap">
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                    <input type="text" class="form-control" placeholder="Usuario" autofocus id='j_username' name='j_username'>
                </div>
                <spring:message code="mayusculas_activas" var="MayusA"/>

                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>

                    <input type="password" class="form-control" placeholder="Contraseña" id='j_password' name='j_password' data-toggle="popover" data-placement="rigth"
                           data-trigger="focus" title="${MayusA}" data-content="">
                </div>

                <c:choose>
                    <c:when test="${lstBancos.size() == 1}">
                        <input type="hidden" name="j_banco" id="j_banco" value="${cIdBanco}"/>
                    </c:when>
                    <c:otherwise>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                            <select class="form-control" name="j_banco" id="j_banco">
                                <c:forEach items="${lstBancos}" var="registro">
                                    <option value="${registro.id}">${registro.descripcion}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </c:otherwise>
                </c:choose>

                <button class="btn btn-login btn-block" id="iniciarBtn" type="submit"><i class="fa fa-lock"></i> Iniciar Sesión</button>

            </div>

            <!-- Modal -->
            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" id="Olvido_contraseña" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Forgot Password ?</h4>
                        </div>
                        <div class="modal-body">
                            <h5><c:out value="${message}"/></h5>
                            <input type="text" name="email" id="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" id="cancelarBtn" type="button">Cancel</button>
                            <button class="btn btn-theme" id="SubmitBtn" type="button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal -->

            <!-- Modal -->
            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal1" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" id="closeBtn" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">${title}</h4>
                        </div>
                        <div class="modal-body">
                            <h5><c:out value="${mensaje}"/></h5>

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn btn-primary" id="okBtn" type="button"><spring:message code="aceptar" text="default text" /></button>
                        </div>
                    </div>
                </div>

            </div>
            <!-- modal -->

        </form>

    </div>
</div>

<!-- js placed at the end of the document so the pages load faster -->
<script src="<c:url value='/js/jquery.js'/>"></script>
<script src="<c:url value='/js/bootstrap.min.js'/>"></script>

<!--BACKSTRETCH-->
<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
<script src="<c:url value='/js/jquery.backstretch.min.js'/>"></script>
<script>
    $.backstretch('<c:url value='${sessionScope.imgLogin}'/>', {speed: 500});
</script>
<script src="<c:url value='/js/login.js?v=1.0.0'/>"></script>


</body>
</html>
