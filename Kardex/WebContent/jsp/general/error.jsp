<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><spring:message code="error_title" text="default text"/></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href=
    <c:url value="/css/bootstrap.css"/>>
    <link rel="stylesheet" href=
    <c:url value="/font-awesome/css/font-awesome.css"/>>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href=
    <c:url value="/css/style.css"/>>
    <link rel="stylesheet" href=
    <c:url value="/css/style-responsive.css"/>>
</head>

<body>

<!-- **********************************************************************************************************************************************************
MAIN CONTENT
*********************************************************************************************************************************************************** -->

<div id="login-page">
    <div class="container">

        <form class="form-login" action="/Kardex/irInicio.jsp" method="get">
            <h2 class="form-login-heading">
                <spring:message code="error_nombre_app" text="default text"/>
            </h2>


            <div class="login-wrap">
                <div align=center>
                    <h2 align=center style="font-weight: bold"><!-- <spring:message code="error_message" text="default text" />  ${pageContext.errorData.statusCode} -->
                        Sesión caducada</h2>
                </div>
                <div>
                    <p align=center>
                        <!--  ${pageContext.request.scheme}://${header.host}${pageContext.errorData.requestURI}--> Por favor vuelva a ingresar.</p>
                </div>

                <button class="btn btn-login btn-block" id="inicioBtn" type="submit"><i class="fa fa-lock"></i> Ir al Inicio</button>
                <p align=center id='CuentaAtras'></p>

            </div>
        </form>

    </div>
</div>

<!-- js placed at the end of the document so the pages load faster -->
<script src=<c:url value="/js/jquery.js"/>></script>
<script src=<c:url value="/js/bootstrap.min.js"/>></script>

</body>
</html>