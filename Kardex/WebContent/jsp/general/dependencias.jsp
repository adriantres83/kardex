<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<!-- js placed at the end of the document so the pages load faster -->
<script src=<c:url value="/js/jquery.js"/>></script>
<script src=<c:url value="/js/bootstrap.min.js"/>></script>
<script src=<c:url value="/js/jquery.dcjqaccordion.2.7.js"/>></script>
<%--<script src=<c:url value="/js/jquery.scrollTo.min.js"/>></script>--%>
<script src= <c:url value="/js/jquery.nicescroll.js"/>></script>
<%--<script src=<c:url value="/js/jquery.sparkline.js"/>></script>--%>
<script src=<c:url value="/js/common-scripts.js"/>></script>
<%--<script src= <c:url value="/js/gritter/js/jquery.gritter.js"/>></script>--%>
<%--<script src= <c:url value="/js/gritter-conf.js"/>></script>--%>
<%--<script src=<c:url value="/js/jquery.mousewheel.min.js?123"/>></script>--%>
<script src=<c:url value="/js/template.js"/>></script>