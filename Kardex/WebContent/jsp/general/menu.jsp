<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!--sidebar start-->
<aside>
	<div id="sidebar" class="nav-collapse ">
		<!-- sidebar menu start-->
		<ul class="sidebar-menu" id="nav-accordion">
				<!-- USUARIO -->
			<div>
				<h6 class="centered titleUsuario titleColorIni">
					<b><c:out value="${sessionScope.usuario}" /></b>
				</h6>

				<h6 class="centered titleColorIni">
					<b><c:out value="${sessionScope.fechaHoy}" /></b>
				</h6>
				<h6 class="centered titleColorIni ipSesionIni">
					<spring:message code="direccionIP" text="default text" />
					<c:out value="${sessionScope.ipSession}" />
				</h6>
			</div>
			<!-- Formulario -->
			<form:form id="menu" name="menu">
			
			<c:forEach items="${sessionScope['lstMenuPadre']}" var="menuPadre">
				<li class="sub-menu"><a href="${menuPadre.link}" id="OpcionMenu${menuPadre.idMenu}"> <i class="${menuPadre.icono}"></i> <span>${menuPadre.descripcion}</span></a>
					<c:if test="${menuPadre.link eq null}">
						<c:forEach items="${sessionScope['lstMenuHijo']}" var="menuHijo">
							
								<c:if test="${menuHijo.idMenuPadre eq menuPadre.idMenu}">
									<ul class="sub">
									<c:choose>
							    		<c:when test="${fn:contains(menuHijo.link, '')}">
											<li>
												<div>
													<a id="OpcionMenu${menuHijo.idMenu}" class="selected" 
														onclick="javascript:ir_opcion('${menuHijo.link}','${menuPadre.descripcion}','${menuHijo.descripcion}');" 
														style="cursor:pointer">
													<span aria-hidden="true">
														${menuHijo.descripcion}
													</span>
													</a>
												</div>
												
											</li>
										</c:when>
									    <c:otherwise>
									    	<li><a href="${menuHijo.link}">${menuHijo.descripcion}</a></li>
									    </c:otherwise>
									</c:choose>
									</ul>
								</c:if>
			        	</c:forEach>
			        </c:if>
	        	</li>
		    </c:forEach>
		  			<input type="hidden" name="menuPadreDescripcion" id="menuPadreDescripcion">
		  			<input type="hidden" name="menuHijoDescripcion" id="menuHijoDescripcion">
		    </form:form>
		    
			<!-- NOTAS CREDITO -->
<!-- 			<li class="mt"><a href="notas_credito"> <i class="fa fa-book"></i> <span>Notas cr�dito</span></a></li> -->
			
			<!-- CONSULTA DEPOSITOS -->
<!-- 			<li class="sub-menu"><a href="consultar_depositos"> <i class="fa fa-tasks"></i> <span>Consultas dep�sitos</span></a></li> -->

			<!-- DISTRIBUCION RECAUDOS -->
<!-- 			<li class="sub-menu"><a href="distribucion_recaudos"> <i class="fa fa-dashboard"></i> <span>Distribuci�n recaudos</span></a></li> -->


			<!-- MOVIMIENTOS -->
<!-- 			<li class="sub-menu"> -->
<!-- 				<a href="javascript:;"><i class="fa fa-desktop"></i> <span>Movimientos</span></a> -->
<!-- 				<ul class="sub"> -->
<!-- 					<li><a href="deposito_directo">Dep�sito directo</a></li> -->
<!-- 					<li><a href="deposito_planilla">Ingreso plantilla</a></li> -->
<!-- 					<li><a href="deposito_planilla">Traslado</a></li> -->
<!-- 					<li><a href="deposito_planilla">Retiro especial</a></li> -->
<!-- 				</ul> -->
<!-- 			</li> -->

			<!-- CONFIGURACION -->
<!-- 			<li class="sub-menu"> -->
<!-- 				<a href="javascript:;"><span><i class="fa fa-cogs"></i> Configuraci�n</span></a> -->
<!-- 				<ul class="sub"> -->
<!-- 					<li><a href="param_app">Par�metros alicaci�n</a></li> -->
<!-- 					<li><a href="param_ld">Cat�logos</a></li> -->
<!-- 				</ul> -->
<!-- 			</li> -->
		
		</ul>
		<!-- sidebar menu end-->
	</div>
	<script>
	
	  	//Funcion para ir a nuevo retiro
	  	//Se cambio la funcion del submit del HTMLFormElement para cuando halla bloqueo pantalla
		function ir_opcion(opcion,menuPadreDescripcion2,menuHijoDescripcion2){
			realizandoSubmit=true;	
		    var form = document.menu;
		    form.action = opcion;		   	
		    form.ismenu=true;
		   	form.menuPadreDescripcion.value = menuPadreDescripcion2;
		    form.menuHijoDescripcion.value = menuHijoDescripcion2;
		    $('.selected').click(function() {
		    	realizandoSubmit=true;	
				$(this).removeClass("active");
				form.submit();
			});
		}
	  
    </script>
  
</aside>
<!--sidebar end-->