
<!-- TAGLIBS -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<!-- PAGES -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- URL -->
<c:url var="home" value="/" scope="request" />
<style> .ds .desc{padding: 11px 0;} .ds h3{margin-left: 0;} </style>
<!-- JAVASCRIPT  -->
<script src='<c:url value="/js/jquery-1.12.3.min.js"/>'></script>
<script>
	window.LOCALS.vars = {
			home : "${home}"
	};
</script>

<!-- TITULO -->
<c:if test="${tAfcMensajeForm.listMensajesGestion.size() ne 0 && tAfcMensajeForm.listMensajesInformativos.size() ne 0}">
	
<h3 class="tittle1">
    <spring:message code="mensajes_title" text="default text"/>
</h3>
</c:if>

<%-- 	<c:if test="${tAfcMensajeForm.listMensajesGestion.size() eq 0 && tAfcMensajeForm.listMensajesInformativos.size() eq 0}"> --%>
<%-- 	<h4><spring:message code="mensajes_no_hay" text="default text" /></h4> --%>
<%-- 	</c:if> --%>

<!--sidebar start-->
<div class="row mt">
	<form:form id="mensajes" name="mensajes" action="consultarMensajes" role="form" commandName="tAfcMensajeForm">
		<form:hidden path="idMensaje" value="" />
	
	
	
	
		<!--MENSAJES DE GESTION-->
		<c:if test="${tAfcMensajeForm.listMensajesGestion.size() > 0 }">
			<div class="col-lg-6 ds">
				<h3><spring:message code="mensajes_gestion_title" text="default text" /></h3>
				
				<c:forEach items="${tAfcMensajeForm.listMensajesGestion}" var="mensajeG" varStatus="loop">
					<div class="desc">
						<!-- descripcion del mensaje -->
						<div class="thumb">
							<div style="height: 20px;">
								<c:if test="${mensajeG.gestionado ne 'S'}">
									<button type="button" id="ModificarBtn" class="btn btn-primary btn-xs" style="width: 80%;" data-toggle="modal" 
										data-target="#myModal<c:out value="${mensajeG.idMensaje}"/>">
										<i class="fa fa-pencil"></i>
									</button>
								</c:if>
								
								<c:if test="${mensajeG.gestionado eq 'S'}">
									<button type="button" id="ModificarBtn" class="btn btn-primary-check btn-xs" style="width: 80%;" data-toggle="modal" 
										data-target="#myModal<c:out value="${mensajeG.idMensaje}"/>">
										<i class="fa fa-check"></i>
									</button>
								</c:if>
								
							</div>
						</div>
						<!-- descripcion del mensaje -->
						
						<!-- detalle del mensaje -->
						<div class="details">
							<p>
								<a href="#"><c:out value="${mensajeG.asunto}" /></a> 
								<br />
								<c:out value="${mensajeG.mensaje}" />
								<br />
								<fmt:formatDate pattern = "dd-MM-yyyy hh:mm:ss a" value = "${mensajeG.fechaCreacion}" />
								<br />
							</p>
						</div>
						<!-- detalle del mensaje -->
						
					</div>
				</c:forEach>
				
				<!-- Paginador -->
				<input type="hidden" name="iEndPage_gest" value="${iEndPage_gest}"/>
	            <input type="hidden" name="lonPage_gest" value="${lonPage_gest}" id="lonPage_gest"/>
	            <input type="hidden" name="iStartPage_gest" value="${iStartPage_gest}"/>
	            <input type="hidden" name="iUltimaPagina_gest" value="${iUltimaPagina_gest}"/>
	            <input type="hidden" name="sizeLista_gest" id="sizeLista_gest" value="${sizeLista_gest}">
	            <input type="hidden" name="lonTotalRegistrosConsulta_gest" value="${lonTotalRegistrosConsulta_gest}">
	            
	            
	            <div align="center" style="float: left; width: 100%;">
	                <nav>
	                    <ul class="pagination">
	                        <c:if test="${lonPage_gest > 1}">
	                            <li class="active">
	                            	<a aria-label="Previous" style="cursor:pointer" class="pagination-log" data-page=1>
	                            		<span aria-hidden="true">&laquo;</span>
	                            	</a>
	                            </li>
	                        </c:if> 
	                        <c:forEach begin="${iStartPage_gest}" end="${iEndPage_gest}" var="p"> 
		                         <c:choose> 
		                             <c:when test="${lonPage_gest eq p}"> 
		                                 <li class="active">
			                                 <a style="cursor:pointer" class="pagination-log" data-page="${p}">
			                                 	<span aria-hidden="true">${p}</span> 
			                                 </a>
		                                </li>
		                            </c:when>
		                            <c:otherwise>
		                                <li>
		                                	<a style="cursor:pointer" class="pagination-log" data-page="${p}">
		                                		<span aria-hidden="true">${p}</span> 
		                                	</a>
		                                </li>
		                            </c:otherwise>
		                        </c:choose>
	                    	</c:forEach> 
	                    	<c:if test="${iUltimaPagina_gest > 0}">
	                        	<c:if test="${lonPage_gest != iUltimaPagina_gest}">
		                            <li>
		                            	<a aria-label="Next" style="cursor:pointer" class="pagination-log" data-page="${sessionScope.iUltimaPagina_gest}">
		                            		<span aria-hidden="true">&raquo;</span> 
		                            	</a>
		                            </li>
	                        	</c:if>
	                     	</c:if>
	                     </ul>
	                 </nav>
	             </div>
	            <!-- Fin Paginador -->
			</div>
		</c:if>
		


		<!--MENSAJES INFORMATIVOS-->
			<c:if test="${tAfcMensajeForm.listMensajesInformativos.size() > 0}">
		<div class="col-lg-6 ds">
			<h3><spring:message code="mensajes_informativos_title" text="default text" /></h3>
			
			<c:forEach items="${tAfcMensajeForm.listMensajesInformativos}" var="mensajeI" varStatus="loop">
				<div class="desc">
					<!-- descripcion del mensaje -->
					<div class="thumb">
						<span class="backInfo badge bg-theme"><i class="iconinfo fa fa-info"></i></span>
					</div>
					<!-- descripcion del mensaje -->
					
					<!-- detalle del mensaje -->
					<div class="details">
						<p>
							<a href="#"><c:out value="${mensajeI.asunto}" /></a>
							<br />
							<c:out value="${mensajeI.mensaje}" />
							<br />
								<fmt:formatDate pattern = "dd-MM-yyyy hh:mm:ss a" value = "${mensajeI.fechaCreacion}" />
							<br />
						</p>
					</div>
					<!-- detalle del mensaje -->
				</div>
			</c:forEach>
			
			<!-- Paginador -->
			<input type="hidden" name="iEndPage_info" value="${iEndPage_info}"/>
            <input type="hidden" name="lonPage_info" value="${lonPage_info}" id="lonPage_info"/>
            <input type="hidden" name="iStartPage_info" value="${iStartPage_info}"/>
            <input type="hidden" name="iUltimaPagina_info" value="${iUltimaPagina_info}"/>
            <input type="hidden" name="sizeLista_info" id="sizeLista_info" value="${sizeLista_info}">
            <input type="hidden" name="lonTotalRegistrosConsulta_info" value="${lonTotalRegistrosConsulta_info}">
            
            
            
            <div align="center">
                  <nav>
                      <ul class="pagination">
                          <c:if test="${sessionScope.lonPage_info > 1}">
                              <li>
                              	<a aria-label="Previous" style="cursor:pointer" class="pagination-log-info" data-page=1>
                              		<span aria-hidden="true">&laquo;</span>
                              	</a>
                              </li>
                          </c:if> 
                          <c:forEach begin="${sessionScope.iStartPage_info}" end="${sessionScope.iEndPage_info}" var="p">
	                          <c:choose>
	                              <c:when test="${sessionScope.lonPage_info eq p}">
	                                  <li class="active">
		                                  <a style="cursor:pointer" class="pagination-log-info" data-page="${p}">
		                                  	<span aria-hidden="true">${p}</span> 
		                                  </a>
	                                  </li>
	                              </c:when>
	                              <c:otherwise>
	                                  <li>
		                                  <a style="cursor:pointer" class="pagination-log-info" data-page="${p}">
		                                  	<span aria-hidden="true">${p}</span> 
		                                  </a>
	                                  </li>
	                              </c:otherwise>
	                          </c:choose>
	                      </c:forEach> 
	                      <c:if test="${sessionScope.iUltimaPagina_info > 0}">
	                          <c:if test="${sessionScope.lonPage_info != sessionScope.iUltimaPagina_info}">
	                              <li>
		                              <a aria-label="Next" style="cursor:pointer" class="pagination-log-info" data-page="${sessionScope.iUltimaPagina_info}">
		                              	<span aria-hidden="true">&raquo;</span> 
		                              </a>
	                              </li>
	                          </c:if>
                      	</c:if>
                      </ul>
                  </nav>
              </div>
            <!-- Fin Paginador -->
		</div>
</c:if>

		<!-- Mensajes para los Tooltips -->
		<spring:message code="TafcMensajes_asunto_tooltip" var="asunto_tooltip" />
		<spring:message code="TafcMensajes_mensaje_tooltip" var="mensaje_tooltip" />
		<spring:message code="TafcMensajes_observaciones_tooltip" var="observaciones_tooltip" />
		<spring:message code="TafcMensajes_gestionar_tooltip" var="gestionar_tooltip" />

		<!-- Mensajes para los titulos -->
		<spring:message code="TafcMensajes_asunto" var="asunto" />
		<spring:message code="TafcMensajes_mensaje" var="mensaje" />
		<spring:message code="TafcMensajes_observaciones" var="observaciones" />
		<spring:message code="TafcMensajes_gestionar" var="gestionar" />

		
		<!-- Se crea un modal de detalle/modificacion por cada uno de los mensjes que se muestran en pantalla-->
		<c:if test="${tAfcMensajeForm.listMensajesGestion.size() > 0}">
			<c:forEach items="${tAfcMensajeForm.listMensajesGestion}" var="mensajeP" varStatus="loop">
			
				<!-- Modal detalle mensaje-->
				<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
					tabindex="-1" id="myModal<c:out value="${mensajeP.idMensaje}"/>"
					class="modal fade in" data-keyboard="false" data-backdrop="static">
					
					<div class="modal-dialog">
						<div class="modal-content">
							
							<!-- header del modal -->
							<div class="modal-header">
								<button type="button" id="closeBtn" class="close closeBtn" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title"><spring:message code="TafcMensajes_mensaje" text="default text" /></h4>
							</div>
							<!-- header del modal -->
							
							<!-- body del modal -->
							<div id="feedback<c:out value="${mensajeP.idMensaje}"/>"></div>
							<div class="modal-body">
								<div class="form-group row">

									<!-- Asunto -->
									<label class="col-sm-3 control-label" id="asunto" data-toggle="popover" 
										data-placement="top" title="${asunto_tooltip}" data-content="${asunto_tooltip}">
										<spring:message code="TafcMensajes_asunto" text="default text" />:
									</label>
									<div class="col-sm-7"><b><c:out value="${mensajeP.asunto}" /></b></div>
								</div>
								<div class="form-group row">
									
									<!-- Mensaje -->
									<label class="col-sm-3 control-label" id="mensaje" data-toggle="popover" 
										data-placement="top" title="${mensaje}" data-content="${mensaje_tooltip}">
										<spring:message code="TafcMensajes_mensaje" text="default text" />
									</label>
									<div class="col-sm-7">
										<textarea class="form-control excluir_caracteres" name="mensaje" id="mensaje" style="resize: vertical;" maxlength="300" rows="5" wrap="soft" disabled="disabled"
										><c:out value="${mensajeP.mensaje}"/></textarea>
									</div>
								</div>
								<div class="form-group row">
									
									<!-- Observaciones -->
									<label class="col-sm-3 control-label" id="observaciones_label" data-toggle="popover" 
										data-placement="top" title="${observaciones}" data-content="${observaciones_tooltip}">
											<spring:message code="TafcMensajes_observaciones" text="default text" />
<%-- 											<font style="color: red;"><c:out value="  *" /></font> --%>
									</label>
									<div class="col-sm-7">
											<textarea class="form-control excluir_caracteres" name="observaciones" id="observaciones" style="resize: vertical;" maxlength="300" rows="5" wrap="soft" ${mensajeP.gestionado eq 'S' ? 'disabled' : ' '}>${mensajeP.observaciones}</textarea>
									</div>
								</div>
								<div class="form-group row">
									
									<!-- gestionado -->
									<label class="col-sm-3 control-label" data-toggle="popover"
										data-placement="top" title="${gestionar}" data-content="${gestionar_tooltip}"> 
										<spring:message code="TafcMensajes_gestionar" text="default text" />
<%-- 										<font style="color: red;"><c:out value="  *" /></font> --%>
									</label>
									<div class="col-sm-7">
										<c:if test="${mensajeP.gestionado == 'S'}">
											<input name="caja" type="checkbox" checked="checked"  disabled="true"/>
 										</c:if> 
 										<c:if test="${mensajeP.gestionado != 'S'}"> 
											<form:checkbox path="gestionado" id="gestionado" value="S" class="gestionado" /> 
 										</c:if> 
									</div>
								</div>
								<form:hidden path="idMensaje" value="${mensajeP.idMensaje}" />
							</div>
							<!-- body del modal -->
							
							<!-- footer del modal -->
							<div class="modal-footer">
									<button type="button" data-dismiss="modal"
										class="btn btn-default btnCancelar"
										id="btnCancelar<c:out value="${mensajeP.idMensaje}"/>">
										<spring:message code="cancelar" text="default text" />
									</button>
								<c:if test="${mensajeP.gestionado != 'S'}"> 
									<button type="submit" id="btn-search" alt='<c:out value="${mensajeP.idMensaje}"/>'class="btn btn-primary btn-search">
										<spring:message code="aceptar" text="default text" />
									</button>
								</c:if> 
								<c:if test="${mensajeP.gestionado == 'S'}"> 
									<button type="submit" class="btn btn-primary" disabled="true">
										<spring:message code="aceptar" text="default text" />
									</button>
								</c:if> 
								<div id="loading<c:out value="${mensajeP.idMensaje}"/>"
									align="center" hidden="true">
									&nbsp;<i class="fa fa-circle-o-notch fa-spin"
										style="font-size: 25px"></i>
								</div>
							</div>
							<!-- footer del modal -->
							
						</div>
					</div>
				</div>
				<!-- Fin modal detalle del mensaje-->
				
				<!-- Modal confirmacion de modificar -->
				<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
					tabindex="-1" id="myModalConfirmarActualizacion<c:out value="${mensajeP.idMensaje}"/>" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">

							<!-- header del modal -->
							<div class="modal-header">
								<button type="button" id="closeBtn" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><spring:message code="confirmacion" text="default text" /></h4>
							</div>
							<!-- header del modal -->

							<!-- body del modal -->
							<div class="modal-body">
								<h5><spring:message code="desea_guardar" text="default text" /></h5>
								<p class="text-warning">
									<small><spring:message code="si_no_guarda" text="default text" /></small>
								</p>
							</div>
							<!-- body del modal -->
							
							<!-- footer del modal -->
							<div class="modal-footer">
								<button type="button" id="cancelarBtn" class="btn btn-default" data-dismiss="modal">
									<spring:message code="cancelar" text="default text" />
								</button>
								<button type="submit" id="btn-search" alt='<c:out value="${mensajeP.idMensaje}"/>' class="btn btn-primary btn-search">
									<spring:message code="aceptar" text="default text" />
								</button>
							</div>
							<!-- footer del modal -->
						</div>
					</div>
				</div>
				<!-- Fin Modal confirmacion de modificar -->
			</c:forEach>
		</c:if>
		
		
		<input type="hidden" name="campoModi" id="campoModi">
	</form:form>
	<script src='<c:url value="/js/mensajes.js?v=1.0.0"/>'></script>
</div>
<!--sidebar end-->