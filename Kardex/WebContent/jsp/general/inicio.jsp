<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!--main content start-->
<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="row mt">
          	
          	<!-- Formulario -->
<%-- 			<form class="form-login" action="/Kardex/inicio" method="post"> --%>
				<form id="inicio" method="POST"
						action="<c:url value="/afc/kardexs"/>">
				<select class="form-control"
					name="producto" id="producto"
					style="min-width: 120px;">
						<option value="Camiseta"><spring:message code="camiseta"	text="default text" /></option>
						<option value="Vaso"><spring:message code="vaso" text="default text" /></option>
						<option value="Revista"><spring:message code="revista" text="default text" /></option>
				</select>
				
				<!-- boton comprar -->
               <button type="button" class="btn btn-theme btnNuevo" id="compraBtn"
                       type="submit">
                   <spring:message code="comprar" text="default text"/>
               </button>
               <button type="button" class="btn btn-theme btnNuevo" id="venderBtn"
                       type="submit">
                   <spring:message code="vender" text="default text"/>
               </button>
               <input type="hidden" name="tipoOperacion" id="tipoOperacion"/>
			</form>
			<script src=<c:url value="/js/inicio.js?v=1.0.0"/>></script>
    	</div>
	</section>
</section>


<!--main content end-->