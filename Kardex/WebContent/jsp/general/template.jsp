<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope['LOCALE']}" scope="session" />
<spring:message code="la_cuenta_no_existe" var="cuenta_no_existe"
	text="default text" />
<c:url var="home" value="/" scope="request" />

<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Dashboard">
<meta name="keyword"
	content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<c:choose>
	<c:when test="${Titulo1!=null}">
		<title>${Titulo1}</title>
	</c:when>
	<c:otherwise>
		<title><spring:message code="Way2Home" text="default text" /></title>
	</c:otherwise>
</c:choose>
<%-- Stop events when pressing Escape --%>
<script>
	var blockListener = function(e) {
		var n = function() {
			e.preventDefault(), e.stopImmediatePropagation()
		};
		27 === e.keyCode && n(), 8 === e.keyCode && "TEXTAREA" !== e.target.nodeName && "INPUT" !== e.target.nodeName && n()
	};
	window.addEventListener("keydown", blockListener, !1), document.addEventListener("keydown", blockListener, !1);
	var getInputlistener = function() {
		var e = document.querySelector('INPUT:not([type="hidden"])');
		e && e.focus()
	};
	window.addEventListener("beforeunload", getInputlistener, !1);
</script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script>
<link rel="shortcut icon" href="../img/afc_logo.ico">


<!-- CSS -->
<link rel="stylesheet" href='<c:url value="/css/bootstrap.css"/>'>
<link rel="stylesheet"
	href='<c:url value="/font-awesome/css/font-awesome.css"/>'>
<link rel="stylesheet"
	href='<c:url value="/css/style_${sessionScope['siglaBanco']}.css?20171107_01"/>'>
<link rel="stylesheet" href='<c:url value="/css/style-responsive.css"/>'>

<style type="text/css">
	.excelbtn>.popover>.popover-title {
		width: 200px;
	}
	
	.dropdown {
		cursor: pointer;
	}
	
	.sub {
		display: none;
	}
	
	.date-read-only {
		cursor: pointer !important;
	}
	
	.backInfo {
		background-color: #ffd200;
	}
	
	.iconinfo {
		font-size: 15px !important;
	}
	
	body {
		font-family: verdana, sans-serif;
	}
	
	.btn-M {
		float: left;
		width: 20px;
		border-radius: 4px;
		margin-right: 6px;
		font-size: 10px;
	}
	
	span.campoConError {
		color: red;
	}
</style>
<body style="overflow-y: hidden;">
	<section id="container">
		<!-- *******ENCABEZADO********** -->

		<!--header start-->
		<header class="header black-bg">

			<div class="logo">

			</div>

			<div class="top-menu">
				<ul class="nav pull-right top-menu">

					<li style="min-height: 60px; width: 60px;"><a id="linkLogout"
						style="text-align: center; cursor: pointer; width: 60px; border: none !important; min-height: 67px;">
							<div style="height: 8px"></div> <span
							class="glyphicon glyphicon-log-out"
							style="text-align: center; font-size: 21px;"></span> <span
							style="font-size: 10px; font-family: verdana, sans-serif;">Salir</span>
					</a></li>
				</ul>
			</div>
			<div style="float: left">
				<a id="topLogoWrapper" href="../irInicio.jsp" class="logo"
					style="margin-top: 2px; padding-top: 10px; padding-left: 10px;">
					
					width="37px">
				</a>
			</div>
			<div style="float: left">
				<c:choose>
					<c:when test="${Titulo1!=null}">
						<h3 id="tituloFijoTemplate"
							style="padding-left: 10px; color: white;">${Titulo1}</h3>
					</c:when>
					<c:otherwise>
						<h3 id="tituloFijoTemplate"
							style="padding-left: 10px; color: white;">
							<spring:message code="nombreTienda" text="default text" />
						</h3>
					</c:otherwise>
				</c:choose>
			</div>

			<script src='<c:url value="/js/jquery.js"/>'></script>

		</header>
		<!--header end-->

		<!-- *******MENU******* -->
		
		<!-- *******CONTENIDO******* -->
		<div id="page-wrapper">
			<div id="page-inner">
				<c:if test="${not empty formulario}">
					<c:import url="${formulario}" />
				</c:if>
			</div>
		</div>

		<!-- Modal caracteres no permitidos-->
		<div class="modal fade" id="modalCaracteresEspeciales" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button id="mo_confirma_guardar1" type="button" class="close"
							data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							<spring:message code="advertencia" text="default text" />
						</h4>
					</div>
					<div class="modal-body">
						<h5 id="mensaje1">
							<spring:message code="caracteres_no_validos" text="default text" />
						</h5>
					</div>
					<div class="modal-footer">
						<button id="mo_aceptar1" type="button" class="btn btn-theme"
							data-dismiss="modal">
							<spring:message code="aceptar" text="default text" />
						</button>
					</div>
				</div>
			</div>
		</div>
		<!--Modal caracteres no permitidos-->

		<!--Modal moneda erronea-->
		<div class="modal fade" id="wrongCurrencyModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button id="mo_confirma_guardar2" type="button" class="close"
							data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							<spring:message code="advertencia" text="default text" />
						</h4>
					</div>
					<div class="modal-body">
						<h5 id="mensaje2">
							<fmt:message key="wrong_currency_format">
								<fmt:param value='${sessionScope["MONEDA_NUMERO_DE_ENTEROS"]}' />
								<fmt:param value='${sessionScope["MONEDA_NUMERO_DE_DECIMALES"]}' />
								<fmt:param value='${sessionScope["MONEDA_SEPARADOR_MILES"]}' />
								<fmt:param value='${sessionScope["MONEDA_SEPARADOR_DECIMAL"]}' />
							</fmt:message>
						</h5>
					</div>
					<div class="modal-footer">
						<button id="mo_aceptar2" type="button" class="btn btn-theme"
							data-dismiss="modal">
							<spring:message code="aceptar" text="default text" />
						</button>
					</div>
				</div>
			</div>
		</div>
		<!--Modal moneda erronea-->



		<!-- Modal confirmar cerrar aplicacion -->
		<div class="modal fade" id="modalCerrarAplicacionConfirmar"
			role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button id="mo_num_cuenta_obligatorio4" type="button"
							class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							<spring:message code="advertencia" text="default text" />
						</h4>
					</div>
					<div class="modal-body">
						<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
						<strong> <spring:message code="sistemaencierre"
								text="default text" /></strong>
					</div>
					<div class="modal-footer">
						<button id="mo_aceptar6" type="button" class="btn btn-theme"
							data-dismiss="modal">
							<spring:message code="aceptar" text="default text" />
						</button>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" id="cookiehidenvalue">
		<!-- Modal confirmar cerrar aplicacion -->

		<!-- Modal validar logout -->
		<div class="modal fade" id="modalValidarLogout" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button id="mo_logout_validar" type="button" class="close"
							data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							<spring:message code="confirmacion" text="default text" />
						</h4>
					</div>
					<div class="modal-body">
						<h5 id="mensaje5">
							<spring:message code="logout_validar" text="default text" />
						</h5>
					</div>
					<div class="modal-footer">
						<button id="modalValidarLogout_cancelar" type="button"
							class="btn btn-default" data-dismiss="modal">
							<spring:message code="cancelar" text="default text" />
						</button>
						<button id="modalValidarLogout_aceptar" type="button"
							class="btn btn-theme" data-dismiss="modal">
							<spring:message code="aceptar" text="default text" />
						</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal validar logout -->

		<!-- *******FOOTER******* -->
		<footer class="site-footer">
			<div class="text-center">
				<h6>2018.12.20.15.00</h6>
			</div>
		</footer>
	</section>

	<!-- js placed at the end of the document so the pages load faster -->
	<script src='<c:url value="/js/bootstrap.min.js"/>'></script>
	<script src='<c:url value="/js/jquery.dcjqaccordion.2.7.js"/>'></script>
	<script src='<c:url value="/js/jquery.scrollTo.min.js"/>'></script>
	<script src='<c:url value="/js/jquery.nicescroll.js"/>'></script>
	<script src='<c:url value="/js/common-scripts.js"/>'></script>
	<script src='<c:url value="/js/jquery.mousewheel.min.js?123"/>'></script>
	<script src='<c:url value="/js/template.js?v=1.0.0"/>'></script>
</html>
