var kardex =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
/**
 *  @description Muestra un mensaje de información luego de realizar una consulta de Ajax.
 *  @param {Event} data Error de Ajax.
 * */
function display(data) {
    var $feedback = $('#feedback');
    $feedback.html("");
    var type;
    if (data.error_type === "C") {
        type = {
            divClass: 'alert-dismissible',
            spanClass: 'glyphicon-ok-sign',
            message: 'Exitoso.'
        };
        var content = "<div class='alert alert-success " + type.divClass + "' role='alert'><span class='glyphicon glyphicon-ok-sign " + type.spanClass + "' aria-hidden='true'></span><strong> " + type.message + "<br> " + data.message + "</strong><br/></div>";
        $feedback.append(content);
    }
    if (data.error_type === "E") {
        $feedback.append("<div class='alert alert-danger alert-dismissible' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><strong> " + data.message + "</strong><br/></div>");
    }
    if (data.error_type === "A") {
        $feedback.append("<div class='alert alert-warning alert-dismissible' role='alert'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span><strong> Alerta.<br> " + data.message + "</strong><br/></div>");
    }
}
exports.display = display;
/**
 *  @description Itera cada elemento de un NodeList
 *  @example
 *  forEachNodeList(filterInputArray, (index, ele: HTMLInputElement) => {
 *      ele.addEventListener('change', lockExcel, false);
 *  });
 *  @param {NodeList} array Vector con elementos HTML.
 *  @param {Function} callback Llamado en cada iteración como: function([scope,]index, element)
 *  @param scope En caso de necesitar el Scope.
 * */
exports.forEachNodeList = function (array, callback, scope) {
    for (var i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]); // passes back stuff we need
    }
};
/**
 * @description Bloquea botón de excel al modificar campos en el formulario para forzar la consulta
 * */
exports.lockExcel = function () {
    var btnExportarExcel = document.getElementById('btnExportarExcel');
    if (btnExportarExcel)
        btnExportarExcel.disabled = true;
};
var fechaHoy = window.LOCALS.appDate;
var options = {
    format: window.LOCALS.calendarDateFormat,
    container: 'body',
    orientation: 'bottom auto',
    language: 'es',
    daysOfWeekHighlighted: '0',
    autoclose: true,
    clearBtn: true,
    beforeShowDay: function (date) {
        var dateFormat = ("0" + date.getDate()).slice(-2) + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getFullYear();
        if (dateFormat === fechaHoy)
            return { classes: 'today' };
    },
};
/**
 * @description Asigna un DatePicker con la fecha de hoy.
 * @example
 * setDateOnDatepicker('fechaDesdeBloqueoF');
 * @param nameField Nombre del HTMLInputElement donde irá el DatePicker
 * */
exports.setDateOnDatepicker = function (nameField) {
    var date_input = $("input[name=\"" + nameField + "\"]");
    if (date_input.val()) {
        date_input.datepicker(options);
    }
    else {
        date_input.datepicker(options);
        date_input.datepicker('setDate', fechaHoy);
        date_input.datepicker('update');
        date_input.val('');
    }
    date_input.datepicker().on('changeDate', function (e) {
        e.currentTarget.focus();
        setTimeout(function () { $('.datepicker').hide(); }, 100);
    });
    date_input.click(function (e) {
        e.currentTarget.blur();
        e.currentTarget.focus();
    });
};
/**
 * @description Solo permite ingresar enteros y '-' en los campos de fechas.
 * @return {boolean} Falso si hay que detener el evento.
 * */
function entero2() {
    var e_k = event['keyCode'];
    if (e_k < 45 || e_k > 57 || e_k === 46 || e_k === 47) {
        if (event.preventDefault)
            event.preventDefault();
        else
            event.returnValue = false;
        return false;
    }
}
exports.entero2 = entero2;
/**
 * @description Valida la expresión regular para validar caracteres permitidos en el evento blur.
 * @param cadena El texto a validar.
 * @return {boolean} Verdadero si hay caracteres permitidos.
 */
function validarExpresionNumeroCuenta(cadena) {
    var patron = window.LOCALS.regExp.accountNumberOnKeypress;
    var patron1 = window.LOCALS.regExp.accountNumberOnBlur;
    return !!(cadena && (!patron.test(cadena) || !patron1.test(cadena)));
}
exports.validarExpresionNumeroCuenta = validarExpresionNumeroCuenta;


/***/ }),

/***/ 7:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__common__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__common___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__common__);


window.seleccionarNuevo = false;
$("#filtarBtn").click(function () {
    consultarFiltro();
});

$("#limpiarBtn").click(function () {
    limpiar();
});

$("#nuevoBtn").click(function () {
    window.seleccionarNuevo = true;
    ModificarUsuario();
});

$("#closeBtn2").click(function () {
    if (window.seleccionarNuevo) {
        limpiar();
    } else {
        consultar();
    }
});

$("#kardex").keypress(function () {
    datosModificados();
});

$("#kardexForm").keypress(function (event) {
    return stopRKey(event)
});

$("#idPerfil").change(function () {
    datosModificados();
});

$("#codigoPerfil").change(function () {
    datosModificados();
});

$("#habilitado").change(function () {
    datosModificados();
});

$("#btnCancelar").click(function () {
    if (window.seleccionarNuevo) {
        limpiar();
    } else {
        consultar();
    }
});

$("#btnGuardar").click(function () {
    searchViaAjax();
});

$("#closeBtn1").click(function () {
    if (window.seleccionarNuevo) {
        limpiar();
    } else {
        consultar();
    }
})

$("#cancelarBtn").click(function () {
    if (window.seleccionarNuevo) {
        limpiar();
    } else {
        consultar();
    }
})

$("#btnEliminar").click(function () {
    eliminar();
});

$("#mo_aceptar_noguardo").click(function () {
    consultar();
});

//Enter para consultar
$('body').keyup(function (e) {
    var form = document.kardexForm;
    if (e.keyCode == 13 && !$(".wc-shellinput").is(":focus") && form.filtarBtn.disabled != true && !$("#modalCaracteresEspeciales").hasClass("in")) {
        consultarFiltro();
    }
});
var paginationAnchorList = $('.pagination-log');
var paginationAnchorOnClick = function (page) {
    document.kardexForm.lonPage.value = page;
    consultar();
};
paginationAnchorList.each(function (index, elem) {
    elem.addEventListener('click', paginationAnchorOnClick.bind(null, elem.dataset.page), false);
});

var modificarUsuarioEvt = function (rowSet) {
    document.kardexForm.kardex.readOnly = true;
    document.kardexForm.kardex.disabled = true;
    functionModal();
    ModificarUsuario(rowSet);
};

var eliminarUsuarioEvt = function (id) {
    EliminarUsuario(id);
};

var actionModalNodeList = document.getElementsByClassName('ModificarBtn');
var actionModalOnClick = function (index, elem) {
    elem.addEventListener('click', modificarUsuarioEvt.bind(null, elem.dataset.row), false);
};
Object(__WEBPACK_IMPORTED_MODULE_0__common__["forEachNodeList"])(actionModalNodeList, actionModalOnClick);

$(".EliminarBtn").each(function (index, elem) {
    elem.addEventListener('click', eliminarUsuarioEvt.bind(null, $(elem).attr("alt")), false);
});

// Limpiar filtro
function limpiar() {
    var form = document.getElementById("kardexForm");
    form.consultarTabla.value = "";
    form.kardexF.value = "";
    form.nombrePerfilF.value = "";
    form.habilitadoF.value = "";
    form.idPerfilF.value = "";
    form.lonPage.value = "";

    form.submit();
}

//Funcion que bloque el enter para que no saque error de POST
function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target
        : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type == "text")) {
        return false;
    }
}

// Funcion para identificar cambios en los campos del modal nuevo
function datosModificados() {
    if (document.getElementById('btnGuardar') != null) {
        $("#btnGuardar").prop("disabled", false);
    }
}

$('#kardex').keydown(function (tecla) {
    if (tecla.ctrlKey == true && tecla.keyCode == 86 || tecla.keyCode == 8) {
        datosModificados();
    }
});

//Funcion para validar campos de entrada
function validacionFiltro() {
    var idPerfilF = document.getElementById('idPerfilF').value;
    if (idPerfilF != "") {
        if (!window.LOCALS.regExp.accountNumberOnKeypress.test(idPerfilF)) {
            $("#idPerfilF").val('');
            $("#idPerfilF").focus();
            $('#modalCaracteresEspeciales').modal('show');
            return false;
        }
    }
    return true;
}

// Consultar
function consultar() {
    document.getElementById('consultaPaginacion').value = "S";
    var form = document.kardexForm;
    if (window.LOCALS.privileges.enquire) {
        var btnfiltrar = document.getElementById("filtarBtn");
        if (btnfiltrar !== null) {
            btnfiltrar.disabled = true;
        }
        var btnLimpiar = document.getElementById("limpiarBtn");
        if (btnLimpiar !== null) {
            btnLimpiar.disabled = true;
        }

        var nueveBtn = document.getElementById('nuevoBtn');
        if (nueveBtn !== null) {
            nueveBtn.disabled = true;
        }
        var label2V = document.getElementById("loading2").style.display = 'block';
        form.consultarTabla.value = "S";
        form.loading2 = label2V;
        form.submit();
    }
}

// Consultar filtro
function consultarFiltro() {
    document.getElementById('consultaPaginacion').value = "N";
    if (validacionFiltro()) {
        var btnfiltrar = document.getElementById("filtarBtn");
        if (btnfiltrar !== null) {
            btnfiltrar.disabled = true;
        }
        var btnLimpiar = document.getElementById("limpiarBtn");
        if (btnLimpiar !== null) {
            btnLimpiar.disabled = true;
        }

        var nueveBtn = document.getElementById('nuevoBtn');
        if (nueveBtn !== null) {
            nueveBtn.disabled = true;
        }
        var label2V = document.getElementById("loading2").style.display = 'block';
        var form = document.kardexForm;
        form.consultarTabla.value = "S";
        form.loading2 = label2V;
        form.submit();
    }
}

function ModificarUsuario(rawData) {
    $('#myModal #feedback').html("");

    if (rawData) {
    	var rowSet = JSON.parse(rawData);
    	
        $('#idKardex').val(rowSet.idKardex);
        $('#producto').val(rowSet.producto);
        $('#tipoOperacion').val(rowSet.tipoOperacion);
        $('#cantidadEntrada').val(rowSet.cantidadEntrada);
        $('#valorTotalEntrada').val(rowSet.valorTotalEntrada);
        $('#cantidadSalida').val(rowSet.cantidadSalida);
        
    } else {
        $('#idKardex').val("");
        $('#producto').val("");
        $('#tipoOperacion').val("");
        $('#cantidadEntrada').val("");
        $('#valorTotalEntrada').val("");
        $('#cantidadSalida').val("");
    }
}


// Funcion modificar
function searchViaAjax() {

    // Prevent the form from submitting via the browser.
    event.preventDefault ? event.preventDefault()
        : (event.returnValue = false);

    var search = {};
    search["idKardex"] = $("#idKardex").val();
    search["producto"] = $("#producto").val();
    search["tipoOperacion"] = $("#tipoOperacion").val();
    search["cantidadEntrada"] = $("#cantidadEntrada").val();
    search["valorTotalEntrada"] = $("#valorTotalEntrada").val();
    search["cantidadSalida"] = $("#cantidadSalida").val();
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: window.LOCALS.home + "afc/agregar_modificar_kardex",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 100000,
        beforeSend: function (data) {
            data.setRequestHeader(header, token);
            $('#myModalConfirmar').modal('hide');
            $("#btnGuardar").prop("disabled", true);
            $("#btnCancelar").prop("disabled", true);
            $("#loading").show();
        },
        success: function (data) {

            $("#btnCancelar").prop("disabled", false);
            $("#loading").hide();

            if (data.status === "lock_version") {
                $('#myModalLockVersion').modal('show');
            } else {
                if (data.status === "SUCCESS") {
                	 document.kardexForm.kardex.readOnly = true;
                	 document.kardexForm.kardex.disabled = true;
                    $("#idUsuario").val(data.codigo);
                    $("#lockVersion").val(data.lockVersion);
                    display(data);
                } else {
                    var errorInfo = "";
                    $("#btnGuardar").prop("disabled", false);
                    var feedback = $('#myModal #feedback');
                    feedback.html("");

                    for (var i = 0; i < data.result.length; i++) {
                        errorInfo += "<br>"
                            + (i + 1)
                            + ". <a href='javascript:void(0);' style='color:#a94442;'  onclick=$('#myModal"
                            + "').find('#"
                            + data.result[i].field
                            + "').focus()>"
                            + data.result[i].code
                            + "</a>";
                    }
                    $(feedback).append("<div class='alert alert-danger alert-dismissible' role='alert'><span class='glyphicon glyphicon-remove-sign' aria-hidden='true'></span><strong> Error." + "<br> " + data.message + errorInfo + "</strong><br/></div>");
                }
            }
        },
        error: function (e) {
            console.log("ERROR: ", e);
        },
        done: function (e) {
            console.log("DONE");
            enableSearchButton(true);
        }
    });

    function display(data) {

        var feedback = $('#myModal #feedback');
        feedback.html("");

        if (data.error_type == "C") {
            $(feedback)
                .append("<div class='alert alert-success alert-dismissible' role='alert'><span class='glyphicon glyphicon-ok-sign' aria-hidden='true'></span><strong> " + "" + data.message + "</strong><br/></div>");
        }

        if (data.error_type == "E") {
            $(feedback)
                .append("<div class='alert alert-danger alert-dismissible' role='alert'><span class='glyphicon glyphicon-remove-sign' aria-hidden='true'></span><strong> Error." + "<br> " + data.message + "</strong><br/></div>");
        }

        if (data.error_type == "A") {
            $(feedback)
                .append("<div class='alert alert-warning alert-dismissible' role='alert'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span><strong> Alerta." + "<br> " + data.message + "</strong><br/></div>");
        }
    }




    function display(data) {

        var feedbackEliminar = $('#myModalEliminar #feedbackEliminar');
        feedbackEliminar.html("");

        if (data.error_type == "C") {
            $(feedbackEliminar)
                .append("<div class='alert alert-success alert-dismissible' role='alert'><span class='glyphicon glyphicon-ok-sign' aria-hidden='true'></span><strong> Exitoso. " + "<br> " + data.message + "</strong><br/></div>");
        }

        if (data.error_type == "E") {
            $(feedbackEliminar)
                .append("<div class='alert alert-danger alert-dismissible' role='alert'><span class='glyphicon glyphicon-remove-sign' aria-hidden='true'></span><strong>" + " " + data.message + "</strong><br/></div>");
        }

        if (data.error_type == "A") {
            $(feedbackEliminar)
                .append("<div class='alert alert-warning alert-dismissible' role='alert'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span><strong>" + " " + data.message + "</strong><br/></div>");
        }
    }
}

//Funcion para desplegar modal     
function functionModal() {
    $('#myModal').modal('show');
}


/***/ })

/******/ });
//# sourceMappingURL=kardex.js.map