var template =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["CerrarScreenLock"] = CerrarScreenLock;
/* harmony export (immutable) */ __webpack_exports__["AbrirScreenLock"] = AbrirScreenLock;
/* harmony export (immutable) */ __webpack_exports__["crearComportamientoSubmit"] = crearComportamientoSubmit;
/* harmony export (immutable) */ __webpack_exports__["EstaRealizandoSubmit"] = EstaRealizandoSubmit;
/* harmony export (immutable) */ __webpack_exports__["disableF5"] = disableF5;
/**
 * @description Blocks the screen when a request is being processed.
 */
// variables globales
var ScreenLockModal;
var ScreenLoad;
var interno;

var ScreenLockModalTransparente;
var ScreenLoadTransparente;
var internoTransparente;

// Cierra el bloqueo de pantalla
function CerrarScreenLock() {
	$(ScreenLockModal).modal("hide");
	$(document).unbind('keydown');
}

// Muestra el bloqueo de pantalla
function AbrirScreenLock() {
	$(ScreenLockModal).modal({
		backdrop : 'static',
		keyboard: false
	});
	
	var resizerAbrirScreenLock = function() {
		$(interno).css({
			position : 'absolute',
			left : ($(window).width() - 50) / 2,
			top : ($(window).height() - 130) / 2
		})
	};
	$(window).bind("resize", resizerAbrirScreenLock);
	$(window).resize();
}




// Crear el modal para el bloqueo de pantalla
function crearScreenLockModal() {
	ScreenLockModal = document.createElement("DIV");
	$("body").append(ScreenLockModal);
	$(ScreenLockModal).addClass('modal fade');
	$(ScreenLockModal).attr('role', 'dialog');
	interno = document.createElement("DIV");
	$(ScreenLockModal).append(interno);
	$(interno).addClass('modal-dialog modal-lg');
	$(interno).attr('style',
			'width: 100px;height:100px;margin:0px; padding:0px');
	var interno2 = document.createElement("DIV");
	$(interno2).attr('style', 'margin:0px;');
	$(interno).append(interno2);
	var logo = document.createElement("i");
	$(logo).addClass('fa fa-circle-o-notch fa-spin');
	$(interno).attr('style', 'font-size:50px; color: #add2e8;');
	$(interno2).append(logo);
}


function crearScreenLockModalTransparente() {
	ScreenLockModalTransparente = document.createElement("DIV");
	$("body").append(ScreenLockModalTransparente);
	$(ScreenLockModalTransparente).addClass('modal fade');
	$(ScreenLockModalTransparente).attr('role', 'dialog');
	$(ScreenLockModalTransparente).attr('style', 'background:white;  opacity: 0 !important;');
	internoTransparente = document.createElement("DIV");
	$(ScreenLockModalTransparente).append(internoTransparente);
	$(internoTransparente).addClass('modal-dialog modal-lg');
	$(internoTransparente).attr('style',
			'width: 100px;height:100px;margin:0px; padding:0px;background:white;  opacity: 0 !important;');
	var internoTransparente2 = document.createElement("DIV");
	$(internoTransparente2).attr('style', 'margin:0px;');
	$(internoTransparente).append(internoTransparente2);
	var logo = document.createElement("i");
	$(internoTransparente).attr('style', 'font-size:50px; color: #add2e8;');
	
}

function AbrirScreenLockTransparente(){	
	$(document).bind('keydown', function(e){ e.stopPropagation(); return false;});
	$(ScreenLockModalTransparente).modal({
		backdrop : 'static',
		keyboard: false
	});
	
	var resizerAbrirScreenLockTransparente = function() {
		$(internoTransparente).css({
			position : 'absolute',
			left : ($(window).width() - 50) / 2,
			top : ($(window).height() - 130) / 2
		})
	};
	$(window).bind("resize", resizerAbrirScreenLockTransparente);
	$(window).resize();
	$( ".modal-backdrop").css( "opacity", "0" );
}
function CerrarScreenLockTransparente() {
	$(ScreenLockModalTransparente).modal("hide");
}

var comportamientocreado=false;;
var realizandoSubmit=false;

function crearComportamientoSubmit(){
	document.oncontextmenu = function(){return false};
	$("a").on("click",EstaRealizandoSubmit);	
	if(!comportamientocreado){
		matricularConfirmacionCierre();
		comportamientocreado=true;
		crearScreenLockModal();	
		crearScreenLockModalTransparente();
		HTMLFormElement.prototype._submit_bloqueo_pantalla = HTMLFormElement.prototype.submit;
		HTMLFormElement.prototype.submit = function() {
			realizandoSubmit=true;
			if(!(this.isdescarga || this.isNuevaVentana)){
							
				AbrirScreenLockTransparente();	
				var targetfound=document.getElementById(this.target);
				if (targetfound!==null){
					$(targetfound).bind("load",CerrarScreenLock);
					$(targetfound).load(function () {
						  this.contentWindow.onbeforeunload = null;
						});
				}			
				if(this.ismenu){
					setTimeout(AbrirScreenLock,2000);
					setTimeout(CerrarScreenLockTransparente,2000);
				}else{
					
					setTimeout(AbrirScreenLock,0);
					setTimeout(CerrarScreenLockTransparente,0);
				}    
			}
			this._submit_bloqueo_pantalla();	
		};
		
		
	}
}
// Sobrrescribe funcion sumbit para poder llamar el metodo AbrirScreenLock



function matricularConfirmacionCierre(){

    window.onbeforeunload = confirmarCierreAplicacion;    
  
    $("body").on('mouseleave', changeRealizandoSubmit);
    $(document).bind("keydown",changeRealizandoSubmit);
}

function changeRealizandoSubmit(){	
	realizandoSubmit=false;
}

function EstaRealizandoSubmit(){
	realizandoSubmit=true;	
}

//slight update to account for browsers not supporting e.which
function disableF5(e) { if ((e.which || e.keyCode) === 116) e.preventDefault(); }


function confirmarCierreAplicacion(e){
	console.log("confirmarCierreAplicacion");
	if(realizandoSubmit){	
		
	}else{			
		return "¿Desea realmente salir?";
	}
}

/***/ }),

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["validarExpresionNumeroCuenta"] = validarExpresionNumeroCuenta;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__bloqueo_pantalla__ = __webpack_require__(1);


/***
 *      _____
 *     |  ___|   _ _ __
 *     | |_ | | | | '_ \
 *     |  _|| |_| | | | |
 *     |_|   \__,_|_| |_|
 *
 */

//Enter para consultar
$(document).keyup(function (e) {
    var $Modal = $("#myModalValidacion");
    var $Moda11 = $("#myModalValidacion1");
    var $Modal2 = $("#modalCaracteresEspeciales");
    var $Modal3 = $("#modalNumeroCuentaObligatorio");
    var $Modal4 = $("#myModalValidacionCheck");
    if (e.keyCode === 13 && $Modal.hasClass("in") || e.keyCode === 13 && $Modal2.hasClass("in") || 
    		e.keyCode === 13 && $Moda11.hasClass("in") || e.keyCode === 13 && $Modal3.hasClass("in") || e.keyCode === 13 && $Modal4.hasClass("in")) {
    	$Modal.modal("hide");
    	$Modal3.modal("hide");
    	$Moda11.modal("hide");
    	$Modal2.modal("hide");
    	$Modal4.modal("hide");
    }
});

function deshabilitaRetroceso() {
    window.location.hash = "no-back-button";
    window.location.hash = "Again-No-back-button"; //chrome
    window.onhashchange = function () {
        window.location.hash = "";
    }
}

// Para quitar la lupa de los campos filtros
function quitarLupaCampo() {
    $("input").focus(function () {
        $($(this).parent().find('label')).removeClass('glyphicon-search');
    });
}

// Para poner la lupa en los campos filtros
function ponerLupaCampo() {
    $("input").blur(function () {
        var campoValue = $(this).val();
        if (!campoValue) {
            $($(this).parent().find('label')).addClass('glyphicon-search');
        }
    });
}

// Para quitar la lupa en los campos filtros al cargar la página
function quitarLupaCampoLoad() {
    $(".lupa").each(function () {
        var campo = $(this).parent().find('input');
        var campoValue = $(campo).val();
        if (campoValue) {
            $(this).removeClass('glyphicon-search');
        }
    });
}

function LogoutApp() {
    Object(__WEBPACK_IMPORTED_MODULE_0__bloqueo_pantalla__["EstaRealizandoSubmit"])();
    location.href = "/ModuloAFC/j_spring_security_logout";
}

function validarLogoutApp() {
    $("#modalValidarLogout").modal();
    $("#modalValidarLogout_aceptar").on("click", LogoutApp);

}

function linkNewWindow() {
	var form = document.createElement('FORM');
	form.method='POST';
	
    form.action = "afc/inicio";
	
	form.isNuevaVentana=true; //Si es nueva ventana para que no bloquee la pantalla
    window.open('../irInicio.jsp');
	
	form.submit();
	return false;
}

function linkNewChatbot() {
	var form = document.createElement('FORM');
	form.method='POST';
	
	form.action = "afc/chatbot";
	
	form.isNuevaVentana=true; //Si es nueva ventana para que no bloquee la pantalla
    window.open("../chatbotRedirect.jsp", "ChatBot", "toolbar=disabled, location=disabled, directories=disabled, status=disabled, " +
    		"menubar=disabled, scrollbars=disabled, status=disabled, resizable=disabled, width=433, height=413, top=240, left=900");

	form.submit();
	return false;
}

// Para los tooltip en las etiquetas
$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="popover2"]').popover({
        trigger: 'hover'
    });
    $("#linkLogout").on("click", validarLogoutApp);
    $("#linkNewWindow").on("click", linkNewWindow);
    $("#chatButton").on("click", linkNewChatbot);
    
   
    $("#cerrar").click(function () {
        $("#popup,#popup1").fadeOut();
    });

    //Cerrar modal clic cualquier parte
    $("html").click(function () {
        $("#popup,#popup1").fadeOut();
    });
    $(".sub").show();

    quitarLupaCampo();
    ponerLupaCampo();
    validarCajasTexto();

    //Scroll vertical y horizontal.
    $('.tabla2').bind('scroll', function () {
        $(".tabla1").scrollLeft($(this).scrollLeft());
        $(".popover").hide();
    });

    //Scroll vertical y horizontal.
    $('.tabla4').bind('scroll', function () {
        $(".tabla2").scrollTop($(this).scrollTop());
        $(".popover").hide();
    });

    //Scroll vertical y horizontal.
    $('.tabla1').bind('scroll', function () {
        $(".tabla2").scrollLeft($(this).scrollLeft());
        $(".popover").hide();
    });


    uppercaseCampo();
});

$(window).load(function () {
    quitarLupaCampoLoad();
    $(".sub").hide();
    Object(__WEBPACK_IMPORTED_MODULE_0__bloqueo_pantalla__["crearComportamientoSubmit"])();
    // To | f5
    /* jQuery < 1.7 */
    $(document).bind("keydown", __WEBPACK_IMPORTED_MODULE_0__bloqueo_pantalla__["disableF5"]);
    /* OR jQuery >= 1.7 */
    $(document).on("keydown", __WEBPACK_IMPORTED_MODULE_0__bloqueo_pantalla__["disableF5"]);
    var valida = window.LOCALS.errorType;
    if (valida) $('#modalSistemaCierre').modal('show');
    
  //Manejo de filtro expansion
  	$("#arrow-up,#arrow-down").click(function () {
  	    $("#FiltroEscondido, #contenido").slideToggle("slow");

  	    if ($("#arrow-up").css("display") == "block") {
  	        $("#arrow-up").css("display", "none");
  	        $("#arrow-down").css("display", "block");
  	    } else {
  	        $("#arrow-up").css("display", "block");
  	        $("#arrow-down").css("display", "none");
  	    }
  	});
});

// Cierra los tooltip de las etiquetas al dar click en cualquier parte de la pantalla
$(document).on('click',
    function (e) {
        $('[data-toggle="popover"]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0
                && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

/**
 * Valida la expresión regular para validar caracteres permitidos
 * en todas las cajas de texto
 */
function validarCajasTexto() {
    var patron;
    var inputOrTextarea = $("input, textarea");
    inputOrTextarea.keypress(function (e) {
        var te1 = $(this).val();
        var tecla = (document.all) ? e.keyCode : e.which;
        if (tecla === 8) return true;
        var te = te1 + String.fromCharCode(tecla);

        // Valida caracteres especiales permitidos en todos los campos de la aplicación en el evento keypress
        if ($(this).hasClass("excluir_caracteres") === false) {
            patron = window.LOCALS.regExp.charactersAllowed;
            validarExpresionRegularKeypress(e, patron, te);
        }

        /**
         * Valida caracteres numéricos con puntos y coma para decimales
         * permitidos en todos los campos moneda en el evento keypress.
         * */
        if ($(this).hasClass("caracteres_moneda") === true) {
            patron = window.LOCALS.regExp.coinOnKeypress;
            validarExpresionRegularKeypress(e, patron, te);
        }

        /**
         * Valida caracteres numéricos enteros permitidos en todos los campos
         * numéricos enteros en el evento keypress.
         */
        if ($(this).hasClass("caracteres_enteros") === true) {
            patron = window.LOCALS.regExp.integers;
            validarExpresionRegularKeypress(e, patron, te);
        }

        /**
         * Valida caracteres numéricos enteros permitidos en todos los campos
         * numéricos enteros en el evento keypress.
         */
        if ($(this).hasClass("numero_cuenta") === true) {
            patron = window.LOCALS.regExp.accountNumberOnKeypress;
            validarExpresionRegularKeypress(e, patron, te);
        }
    });

    inputOrTextarea.blur(function () {
        var cadena = $(this).val();

        // Valida caracteres especiales permitidos en todos los campos de la aplicación en el evento blur
        if ($(this).hasClass("excluir_caracteres") === false) {
            patron = window.LOCALS.regExp.charactersAllowed;
            validarExpresionRegularBlur(patron, cadena, this);
        }

        // Valida caracteres numéricos con puntos y coma para decimales
        // permitidos en todos los campos moneda en el evento blur
        if ($(this).hasClass("caracteres_moneda") === true) {
            patron = window.LOCALS.regExp.coinOnBlur;
            validarExpresionRegularBlur(patron, cadena, this);
        }

        // Valida caracteres numéricos enteros permitidos en todos los campos
        // numéricos enteros en el evento blur
        if ($(this).hasClass("caracteres_enteros") === true) {
            patron = window.LOCALS.regExp.integers;
            validarExpresionRegularBlur(patron, cadena, this);
        }

        // Valida caracteres numéricos enteros permitidos en todos los campos
        // numéricos enteros en el evento blur
        if ($(this).hasClass("numero_cuenta") === true) {
            patron = window.LOCALS.regExp.accountNumberOnKeypress;
            validarExpresionRegularBlur(patron, cadena, this);
        }
    });
}

/**
 * Validar la expresión regular para validar caracteres permitidos en el evento keypress
 */
function validarExpresionRegularKeypress(event, patron, texto) {
    if (!patron.test(texto)) {
        if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
        return false;
    }
}

/**
 * Validar la expresión regular para validar caracteres permitidos en el evento blur
 */
function validarExpresionRegularBlur(patron, cadena, input) {
    if (cadena && !patron.test(cadena)) {
        $(input).val('');
        ponerLupaCampo($(input).attr('id'));
        $('#modalCaracteresEspeciales').modal('show');
    }
}

/**
 * Validar la expresión regular para validar caracteres permitidos en el evento blur
 */
function validarExpresionNumeroCuenta(cadena) {
    var patron = window.LOCALS.regExp.accountNumberOnKeypress;
    var patron1 = window.LOCALS.regExp.accountNumberOnBlur;
    return !!(cadena && (!patron.test(cadena) || !patron1.test(cadena)));
}

// Función para poner uppercase a los campos código oficina
function uppercaseCampo() {
    $(".uppercase").blur(function () {
        $(this).val($(this).val().toUpperCase());
    });
}

$(document).ready(function () {
    var tablasDatos = document.getElementsByClassName("table table-bordered table-striped table-condensed");
    for (var i = 0; i < tablasDatos.length; i++) {
        var tabla = tablasDatos[i];
        $(tabla).bind('mousewheel', function (event) {
            try {
                var caja = $(this).parent().parent().find(".tabla4");
                if (caja.length > 0) {
                    $(caja).scrollTop($(caja).scrollTop() + event.originalEvent['deltaY']);
                } else {
                    caja = $(this).parent().parent().find(".tabla2");
                    $(caja).scrollTop($(caja).scrollTop() + event.originalEvent['deltaY']);
                }
            }
            catch (e) {
            }
            return false;
        });
    }
    	
});




/***
 *      _____                 _
 *     | ____|_   _____ _ __ | |_ ___
 *     |  _| \ \ / / _ \ '_ \| __/ __|
 *     | |___ \ V /  __/ | | | |_\__ \
 *     |_____| \_/ \___|_| |_|\__|___/
 *
 */

window.addEventListener('load', deshabilitaRetroceso, false);

var logoAnchor = document.getElementById('logoWrapper');
var topLogoAnchor = document.getElementById('topLogoWrapper');
var headerInboxBarLI = document.getElementById('header_inbox_bar');
logoAnchor.addEventListener('click', __WEBPACK_IMPORTED_MODULE_0__bloqueo_pantalla__["EstaRealizandoSubmit"], false);
topLogoAnchor.addEventListener('click', __WEBPACK_IMPORTED_MODULE_0__bloqueo_pantalla__["EstaRealizandoSubmit"], false);
headerInboxBarLI.addEventListener('click', function () {
    Object(__WEBPACK_IMPORTED_MODULE_0__bloqueo_pantalla__["EstaRealizandoSubmit"])();
    location.href = '../irInicio.jsp';
}, false);


/***/ })

/******/ });
//# sourceMappingURL=template.js.map