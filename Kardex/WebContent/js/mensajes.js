var mensajes =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ 9:
/***/ (function(module, exports) {

var guardarMensajeEvent = function(idMensaje){
	guardarMensaje(idMensaje);
}

var consultarEvent = function(){
	consultar();
}

var datosModificadosModificarEvent = function(){
	datosModificadosModificar();
}

var paginationAnchorList = $('.pagination-log');
var paginationAnchorOnClick = function (page) {
	document.mensajes.lonPage_gest.value =  page;
    paginar();
};
paginationAnchorList.each(function (index, elem) {
    elem.addEventListener('click', paginationAnchorOnClick.bind(null, elem.dataset.page), false);
});

var paginationInfoList = $('.pagination-log-info');
var paginationInfoClick = function (page) {
	document.mensajes.lonPage_info.value =  page;
    paginar();
};
paginationInfoList.each(function (index, elem) {
    elem.addEventListener('click', paginationInfoClick.bind(null, elem.dataset.page), false);
});

$(".closeBtn").each(function(index, elem){
	elem.addEventListener('click', consultarEvent.bind(null, null), false);
});

$(".btnCancelar").each(function(index, elem){
	elem.addEventListener('click', consultarEvent.bind(null, null), false);
});

$(".btn-search").each(function(index, elem){
	var $input = $( elem );
	elem.addEventListener('click', guardarMensajeEvent.bind(null, $input.attr("alt")), false);
});


$(".gestionado").each(function(index, elem){
	elem.addEventListener('change', datosModificadosModificarEvent.bind(null, null), false);
});


//Permite paginar el formulario de filtros
	    function paginar() {
	        var form = document.mensajes;
	        form.action = 'consultarMensajes';
	        form.submit();
	    }
		
	 	// Funcion para identificar cambios en los campos del modal modificar
		function datosModificadosModificar(idUsuarioModificar) {
			var idUsuarioV = $('#myModal'+ idUsuarioModificar).find("#idUsuario").val();
// 			if(document.getElementById('btnGuardarModificar' + idUsuarioV)!= null){
// 				$("#btnGuardarModificar" + idUsuarioV).prop("disabled", false);
// 			}
			document.getElementById('campoModi').value="S";
		}
	 	
	 	
		function display3(data, modal) {

			$('#feedback' + modal).html("");

			if (data.error_type == "C") {
				$('#feedback' + modal)
						.append("<div class='alert alert-success alert-dismissible' role='alert'><span class='glyphicon glyphicon-ok-sign' aria-hidden='true'></span><strong>" + " " + data.message + "</strong><br/></div>");
			}

			if (data.error_type == "E") {
				$('#feedback' + modal)
						.append("<div class='alert alert-danger alert-dismissible' role='alert'><span class='glyphicon glyphicon-remove-sign' aria-hidden='true'></span><strong>" + " " + data.message + "</strong><br/></div>");
			}

			if (data.error_type == "A") {
				$('#feedback' + modal)
						.append("<div class='alert alert-warning alert-dismissible' role='alert'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span><strong>" + " " + data.message + "</strong><br/></div>");
			}
		}
		
		
		// Funcion modificar
		function guardarMensaje(modal) {

			// Prevent the form from submitting via the browser.
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
// 			document.mensajes.idMensaje.value = modal;

			var search = {}
			search["idMensaje"] = $('#myModal' + modal).find("#idMensaje").val();
			search["gestionado"] = $('#myModal' + modal).find("#gestionado").prop("checked");
			search["observaciones"] = $('#myModal' + modal).find("#observaciones").val();

			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : window.LOCALS.vars.home+"afc/modificar_mensaje",
				data : JSON.stringify(search),
				dataType : 'json',
				cache : false,
				timeout : 100000,
				
				beforeSend: function (data) {
					data.setRequestHeader(header, token);
				},
				
				success : function(data) {
					// Se muestra el resultado de la actualizacion
					if (data.status == "SUCCESS") {						
						display3(data, modal);						
					} else {
						errorInfo = "";
						$('#feedback' + modal).append("<div class='alert alert-danger alert-dismissible' role='alert'><span class='glyphicon glyphicon-remove-sign' aria-hidden='true'></span><strong>" + " " + data.message + errorInfo + "</strong><br/></div>");
					}
					$('#myModal' + modal).modal('hide');
					consultar();
					
				},
				error : function(e) {
					display3(e, modal);
					$('#myModal' + modal).modal('hide');
					consultar();
				},
				done : function(e) {
// 					enableSearchButton(true);
				}
			});			
		}
		
		//Funcion para marcar los registros que fueron modificados
		function modificar(modal){
			$('#myModalConfirmarActualizacion' + modal).modal('show');
		}
		
		// Consultar
		function consultar() {
			var form = document.mensajes;
			form.action = 'consultarMensajes';
			form.submit();
		}
	


/***/ })

/******/ });
//# sourceMappingURL=mensajes.js.map