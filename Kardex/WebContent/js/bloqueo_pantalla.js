var bloqueo_pantalla =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["CerrarScreenLock"] = CerrarScreenLock;
/* harmony export (immutable) */ __webpack_exports__["AbrirScreenLock"] = AbrirScreenLock;
/* harmony export (immutable) */ __webpack_exports__["crearComportamientoSubmit"] = crearComportamientoSubmit;
/* harmony export (immutable) */ __webpack_exports__["EstaRealizandoSubmit"] = EstaRealizandoSubmit;
/* harmony export (immutable) */ __webpack_exports__["disableF5"] = disableF5;
/**
 * @description Blocks the screen when a request is being processed.
 */
// variables globales
var ScreenLockModal;
var ScreenLoad;
var interno;

var ScreenLockModalTransparente;
var ScreenLoadTransparente;
var internoTransparente;

// Cierra el bloqueo de pantalla
function CerrarScreenLock() {
	$(ScreenLockModal).modal("hide");
	$(document).unbind('keydown');
}

// Muestra el bloqueo de pantalla
function AbrirScreenLock() {
	$(ScreenLockModal).modal({
		backdrop : 'static',
		keyboard: false
	});
	
	var resizerAbrirScreenLock = function() {
		$(interno).css({
			position : 'absolute',
			left : ($(window).width() - 50) / 2,
			top : ($(window).height() - 130) / 2
		})
	};
	$(window).bind("resize", resizerAbrirScreenLock);
	$(window).resize();
}




// Crear el modal para el bloqueo de pantalla
function crearScreenLockModal() {
	ScreenLockModal = document.createElement("DIV");
	$("body").append(ScreenLockModal);
	$(ScreenLockModal).addClass('modal fade');
	$(ScreenLockModal).attr('role', 'dialog');
	interno = document.createElement("DIV");
	$(ScreenLockModal).append(interno);
	$(interno).addClass('modal-dialog modal-lg');
	$(interno).attr('style',
			'width: 100px;height:100px;margin:0px; padding:0px');
	var interno2 = document.createElement("DIV");
	$(interno2).attr('style', 'margin:0px;');
	$(interno).append(interno2);
	var logo = document.createElement("i");
	$(logo).addClass('fa fa-circle-o-notch fa-spin');
	$(interno).attr('style', 'font-size:50px; color: #add2e8;');
	$(interno2).append(logo);
}


function crearScreenLockModalTransparente() {
	ScreenLockModalTransparente = document.createElement("DIV");
	$("body").append(ScreenLockModalTransparente);
	$(ScreenLockModalTransparente).addClass('modal fade');
	$(ScreenLockModalTransparente).attr('role', 'dialog');
	$(ScreenLockModalTransparente).attr('style', 'background:white;  opacity: 0 !important;');
	internoTransparente = document.createElement("DIV");
	$(ScreenLockModalTransparente).append(internoTransparente);
	$(internoTransparente).addClass('modal-dialog modal-lg');
	$(internoTransparente).attr('style',
			'width: 100px;height:100px;margin:0px; padding:0px;background:white;  opacity: 0 !important;');
	var internoTransparente2 = document.createElement("DIV");
	$(internoTransparente2).attr('style', 'margin:0px;');
	$(internoTransparente).append(internoTransparente2);
	var logo = document.createElement("i");
	$(internoTransparente).attr('style', 'font-size:50px; color: #add2e8;');
	
}

function AbrirScreenLockTransparente(){	
	$(document).bind('keydown', function(e){ e.stopPropagation(); return false;});
	$(ScreenLockModalTransparente).modal({
		backdrop : 'static',
		keyboard: false
	});
	
	var resizerAbrirScreenLockTransparente = function() {
		$(internoTransparente).css({
			position : 'absolute',
			left : ($(window).width() - 50) / 2,
			top : ($(window).height() - 130) / 2
		})
	};
	$(window).bind("resize", resizerAbrirScreenLockTransparente);
	$(window).resize();
	$( ".modal-backdrop").css( "opacity", "0" );
}
function CerrarScreenLockTransparente() {
	$(ScreenLockModalTransparente).modal("hide");
}

var comportamientocreado=false;;
var realizandoSubmit=false;

function crearComportamientoSubmit(){
	document.oncontextmenu = function(){return false};
	$("a").on("click",EstaRealizandoSubmit);	
	if(!comportamientocreado){
		matricularConfirmacionCierre();
		comportamientocreado=true;
		crearScreenLockModal();	
		crearScreenLockModalTransparente();
		HTMLFormElement.prototype._submit_bloqueo_pantalla = HTMLFormElement.prototype.submit;
		HTMLFormElement.prototype.submit = function() {
			realizandoSubmit=true;
			if(!(this.isdescarga || this.isNuevaVentana)){
							
				AbrirScreenLockTransparente();	
				var targetfound=document.getElementById(this.target);
				if (targetfound!==null){
					$(targetfound).bind("load",CerrarScreenLock);
					$(targetfound).load(function () {
						  this.contentWindow.onbeforeunload = null;
						});
				}			
				if(this.ismenu){
					setTimeout(AbrirScreenLock,2000);
					setTimeout(CerrarScreenLockTransparente,2000);
				}else{
					
					setTimeout(AbrirScreenLock,0);
					setTimeout(CerrarScreenLockTransparente,0);
				}    
			}
			this._submit_bloqueo_pantalla();	
		};
		
		
	}
}
// Sobrrescribe funcion sumbit para poder llamar el metodo AbrirScreenLock



function matricularConfirmacionCierre(){

    window.onbeforeunload = confirmarCierreAplicacion;    
  
    $("body").on('mouseleave', changeRealizandoSubmit);
    $(document).bind("keydown",changeRealizandoSubmit);
}

function changeRealizandoSubmit(){	
	realizandoSubmit=false;
}

function EstaRealizandoSubmit(){
	realizandoSubmit=true;	
}

//slight update to account for browsers not supporting e.which
function disableF5(e) { if ((e.which || e.keyCode) === 116) e.preventDefault(); }


function confirmarCierreAplicacion(e){
	console.log("confirmarCierreAplicacion");
	if(realizandoSubmit){	
		
	}else{			
		return "¿Desea realmente salir?";
	}
}

/***/ })
/******/ ]);
//# sourceMappingURL=bloqueo_pantalla.js.map