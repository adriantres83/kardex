/**
 * @description Validator for currency.
 * @author César
 * @param regExp Regular expression for currency
 * @param inputObj The input element
 * @return {boolean}
 */
export let inputCoinValidator = (regExp: RegExp, inputObj: HTMLInputElement) => {
    return regExp.test(inputObj.value);
};