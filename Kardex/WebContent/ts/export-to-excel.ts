declare var header: string;
declare var token: string;

/**
 * @description Si parámetro es igual a b bloquea los botones de acciones, sino los activa.
 * @param {string} bloquea
 **/
export function bloquearbotonesacciones(bloquea): void {
    let filtarBtn = <HTMLButtonElement>document.getElementById("filtarBtn");
    let LimpiarBtn = <HTMLButtonElement>document.getElementById("LimpiarBtn");
    let btnExportarExcel = <HTMLButtonElement>document.getElementById('btnExportarExcel');
    if (filtarBtn !== null) filtarBtn.disabled = bloquea === 'b';
    if (LimpiarBtn !== null) LimpiarBtn.disabled = bloquea === 'b';
    if (btnExportarExcel !== null) btnExportarExcel.disabled = bloquea === 'b';
}

/**
 * @description Para exportar a excel en ajax.
 * @param {string} uriAction
 **/
export function excel(uriAction): void {
    bloquearbotonesacciones('b');
    $("#loading2").show();
    let xhr = new XMLHttpRequest();
    xhr.open('POST', window.LOCALS.home + uriAction, true);
    xhr.responseType = 'arraybuffer';
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader(header, token);
    xhr.send("tipo=excel");

    xhr.onload = function () {
        if (xhr.status == 200) {
            var blob = new Blob([xhr.response], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8'
            });
            if (window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(blob, xhr.getResponseHeader("Content-disposition"));
            } else {
                var downloadUrl = URL.createObjectURL(blob);
                var a = document.createElement("a");
                a.href = downloadUrl;
                a.download = xhr.getResponseHeader("Content-disposition");
                document.body.appendChild(a);
                a.click();
            }
            $("#loading2").hide();
            bloquearbotonesacciones('d');
        } else {
            $("#loading2").hide();
            bloquearbotonesacciones('d');
        }
    }

}