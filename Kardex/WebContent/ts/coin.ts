///<reference path="bignumber.d.ts"/>

import BigNumber from 'bignumber.js';

/**
 * @description Class sfor working with coins
 * @author César
 */
export class Coin {

    private static deIure = {
        symbol: '$',
        separator: ',',
        decimal: '.',
        decimalPrecision: 2,
        integerPrecision: 14
    };

    public static config(symbol, {separator = ',', decimal = '.', decimalPrecision = 2, integerPrecision = 14} = {}) {
        Coin.deIure = {
            symbol,
            separator,
            decimal,
            decimalPrecision,
            integerPrecision
        };
    }

    /**
     * @description Converts raw string or inputElement to a string with comma as decimal mark (####,##).
     * @author César
     * @param dirty {string|HTMLInputElement}
     * @param decimal
     * @return {String} Number with two decimals
     */
    public static cleanCurrency(dirty: string | HTMLInputElement, decimal = Coin.deIure.decimal): string {
        let valueInt: string;
        let valueDec: string;
        let oddCharactersRegExp: RegExp = new RegExp(`[^0-9${decimal}]`, 'g');
        let decimalRegExp: RegExp = new RegExp(`[${decimal}]`, 'g');
        let zeros = '';

        for (let i = 0; i < Coin.deIure.decimalPrecision; i++) {
            zeros += '0';
        }

        let value = '0';

        if (typeof dirty === 'string') value = dirty;

        if (typeof dirty === 'object' && dirty.nodeName === 'INPUT') value = dirty.value;

        //Removes odd characters
        value = value.replace(oddCharactersRegExp, '');

        //Removes zeros at the left.
        value = value.replace(/^0+/g, '');

        // Where more than one comma
        if (value.match(decimalRegExp) && value.match(decimalRegExp).length > 1) {
            let temp = value.split(decimal);
            value = temp.shift() + decimal + temp.join('');
        }

        // Value vectorized
        let valueArray = value.split(decimal);

        if (valueArray.length === 2) {
            valueInt = valueArray[0];
            valueDec = valueArray[1];
        } else {
            valueInt = valueArray[0];
        }

        if (valueDec) {
            if (valueDec.length < Coin.deIure.decimalPrecision) {
                while (true) {
                    valueDec += '0';
                    if (valueDec.length === Coin.deIure.decimalPrecision) break;
                }
            }
        } else {
            valueDec = zeros;
        }

        value = (valueInt ? valueInt : '0') + decimal + valueDec;

        if (typeof dirty === 'object' && dirty.nodeName === 'INPUT') {
            if (value === '0,00' || value === '0.00') value = '';
            dirty.value = value;
        }
        return value;
    }

    /**
     * @description Adds separators to a number.
     * @author César
     * @param {String} obj Raw number
     * @return {String} number with separators
     */
    public static addGroupSeparator(obj: string | HTMLInputElement): string {
        let decimal = Coin.deIure.decimal;
        let separator = Coin.deIure.separator;
        let strArray;
        let str: string;

        if (typeof obj === 'string') str = obj;

        if (typeof obj === 'object' && obj.nodeName === 'INPUT') str = obj.value;

        if (str) {
            strArray = str.split(decimal);
        } else {
            return;
        }

        if (strArray[0]) {
            let valueIntArray = strArray[0].split('');
            valueIntArray = valueIntArray.reverse();
            let intReduced = valueIntArray.reduce(function ($, ele, i) {
                let is3rd = (i === 0) ? 1 : i % 3;
                return $ + (is3rd === 0 ? separator : '') + ele;
            });
            intReduced = intReduced.split('').reverse().join('');
            strArray[0] = intReduced;
        }
        str = strArray.join(decimal);

        if (typeof obj === 'object' && obj.nodeName === 'INPUT') {
            obj.value = str;
        }

        return str;
    }

    /**
     * @description Converts raw string or inputElement to a string with comma as decimal mark (####,##).
     * @author César
     * @param dirty {string|HTMLInputElement}
     * @param decimal
     * @return {String} Number as string
     */
    public static clean(dirty: string | HTMLInputElement, decimal = Coin.deIure.decimal): string {
        let oddCharactersRegExp: RegExp = new RegExp(`[^0-9${decimal}]`, 'g');
        let decimalRegExp: RegExp = new RegExp(`[${decimal}]`, 'g');

        let value = '';

        if (typeof dirty === 'string') value = dirty;

        if (typeof dirty === 'object' && dirty.nodeName === 'INPUT') value = dirty.value;

        //Removes odd characters
        value = value.replace(oddCharactersRegExp, '');

        //Removes zeros at the left.
        value = value.replace(/^0+/g, '');

        //Removes zeros at the right (decimals).
        // value = value.replace(/,0+$/, ''); TODO: Add a parameter
        value = value.replace(`${decimal}00`, '');

        // When there's more than one comma
        if (value.match(decimalRegExp) && value.match(decimalRegExp).length > 1) {
            let temp = value.split(decimal);
            value = temp.shift() + decimal + temp.join('');
        }

        // Value vectorized
        let valueArray = value.split(decimal);

        if (valueArray.length === 2) {
            if (!valueArray[0] && valueArray[1]) {
                valueArray[0] = '0';
            }
            value = valueArray.join(decimal);
        } else {
            value = valueArray[0];
        }

        if (typeof dirty === 'object' && dirty.nodeName === 'INPUT') {
            if (value === '0,00' || value === '0.00') value = '';
            dirty.value = value;
        }
        return value;
    }

    /**
     * @description Converts to dot a comma as decimal mark (####,##) => (####.##).
     * @author César
     * @param {String} str Formatted number
     * @return {String} Formatted number
     */
    public static toDotCurrency(str: string): string {
        str = str.replace(',', '.');
        if (str.search('.') === -1) str = Coin.cleanCurrency(str, '.');
        return str;
    }

    /**
     * @description Converts to comma a dot as decimal mark (####.##) => (####,##).
     * @author César
     * @param {String} str Formatted number
     * @return {String} Formatted number
     */
    public static toCommaCurrency(str: string): string {
        str = str.replace('.', ',');
        if (str.search(',') === -1) str = Coin.cleanCurrency(str);
        return str;
    }

    /**
     * @description Converts a BigNumber to the actual currency (####.##) or (####,##).
     * @author César
     * @return {String} Formatted number
     * @param bigNumber
     */
    public static toCurrentCurrency(bigNumber: BigNumber): string {
        let str = bigNumber.toString();
        return Coin.deIure.decimal === ',' ? Coin.toCommaCurrency(str) : str;
    }

    /**
     * @description Converts the actual currency to BigNumber (####.##) or (####,##) => BigNumber.
     * @author César
     * @return {String} Formatted number
     * @param str
     */
    public static toBigNumber(str: string): BigNumber {
        return Coin.deIure.decimal === ',' ? new BigNumber(Coin.toDotCurrency(str)) : new BigNumber(str);
    }

    /**
     * @description sum values (####.##| ####,##).
     * @author César
     * @return {Array<String>} Formatted numbers
     * @param values
     */
    public static sumValues(...values: string[]): string {
        let bigNumberValues: BigNumber[];
        let sum = new BigNumber('0');
        bigNumberValues = values.map((ele) => {
            let re = /,/;
            if (re.exec(ele)) ele = ele.replace(',', '.');
            return new BigNumber(ele);
        });
        bigNumberValues.forEach((ele) => {
            sum = sum.plus(ele);
        });
        return sum.toString();
    }

    /**
     * @description sum values (####.##| ####,##) and return a value with comma as decimal mark.
     * @author César
     * @return {String} Formatted number
     * @param values
     */
    public static sumCommaValues(...values: string[]): string {
        return Coin.sumValues(...values).replace('.', ',');
    }

    /**
     * @description Converts unformatted or BigNumber value to a formatted value
     * @author César
     * @param {String | BigNumber} str Raw number
     * @param symbol
     * @param separator
     * @param decimal
     * @return {String} Formatted number
     */
    public static prettifyCurrency(str: string | BigNumber, {symbol = Coin.deIure.symbol, separator = Coin.deIure.separator, decimal = Coin.deIure.decimal} = {}): string {
        if (typeof str === 'object' && str['isBigNumber']) {
            str = Coin.toCurrentCurrency(str);
        } else {
            str = Coin.cleanCurrency(<string>str, decimal);
        }
        let strArray = str.split(decimal);
        let valueInt = strArray[0];
        let valueDec = strArray[1];

        let zeros = '';

        for (let i = 0; i < Coin.deIure.decimalPrecision; i++) {
            zeros += '0';
        }

        if (valueDec) {
            if (valueDec.length < Coin.deIure.decimalPrecision) {
                while (true) {
                    valueDec += '0';
                    if (valueDec.length === Coin.deIure.decimalPrecision) break;
                }
            }
        } else {
            valueDec = zeros;
        }

        let valueIntArray = valueInt.split('');
        valueIntArray = valueIntArray.reverse();
        valueInt = valueIntArray.reduce(function ($, ele, i) {
            let is3rd = (i === 0) ? 1 : i % 3;
            return $ + (is3rd === 0 ? separator : '') + ele;
        });
        valueInt = valueInt.split('').reverse().join('');
        str = symbol + valueInt + decimal + valueDec;

        return str;
    }

    /**
     * @description Converts non-formatted value to a formatted value
     * @author César
     * @param {HTMLInputElement} obj Raw number
     * @param {boolean} bool Format if true
     * @return {String} Formatted number
     */
    public static formatInputCurrency(obj: HTMLInputElement, bool: boolean) {
        if (bool) {
            let cleaned = Coin.cleanCurrency(obj.value);
            obj.value = Coin.prettifyCurrency(cleaned);
        }
    }

    /**
     * @deprecated Use pasteValidator instead.
     * @description It gets the text data on a paste event and reduces to the maximum permitted but filtering none valid characters.
     * However, the text will be pasted when the data is invalid if the text is invalid
     * @author César
     * @param {Event} event The paste event.
     */
    public static inputNumberLimiterOnPaste(event: ClipboardEvent) {
        let element = <HTMLInputElement>event.target;
        let rawText = event.clipboardData ? event.clipboardData.getData('Text') : (<any>event).view.clipboardData.getData('Text');
        let regexp = new RegExp('[0-9]');

        if (rawText && rawText.length > 0) {
            element.value = '';
            let array = rawText.split(Coin.deIure.decimal);
            if (array.length > 2) {
                // Paste the original text when is invalid.
                element.value = rawText;
            } else if (array.length === 2) {
                let integerArray = array[0].split('');
                let decimalArray = array[1].split('');
                integerArray = integerArray.filter((value) => {
                    if (regexp.test(value)) {
                        return value;
                    } else {
                        return '';
                    }
                });
                if (integerArray.length > Coin.deIure.integerPrecision) {
                    element.value = integerArray.slice(0, Coin.deIure.integerPrecision).join('');
                } else {
                    decimalArray = decimalArray.filter((value) => {
                        if (regexp.test(value)) {
                            return value;
                        } else {
                            return '';
                        }
                    });
                    if (decimalArray.length > Coin.deIure.decimalPrecision) decimalArray.splice(Coin.deIure.decimalPrecision);
                    element.value = integerArray.join('') + Coin.deIure.decimal + decimalArray.join('');
                }
            } else {
                let integerArray = array[0].split('');
                integerArray = integerArray.filter((value) => {
                    if (regexp.test(value)) {
                        return value;
                    } else {
                        return '';
                    }
                });
                if (integerArray.length > Coin.deIure.integerPrecision) integerArray.splice(Coin.deIure.integerPrecision);
                element.value = integerArray.join('');
            }
        }
        event.preventDefault();
    };

    /**
     * @description It gets the text data on a paste event and reduces to the maximum permitted but filtering none valid characters.
     * However, the text will be pasted when the data is invalid if the text is invalid
     * @author César
     * @param {Event} event The paste event.
     */
    public static pasteValidator(event: ClipboardEvent) {
        let element = <HTMLInputElement>event.target;
        let rawText = event.clipboardData ? event.clipboardData.getData('Text') : (<any>event).view.clipboardData.getData('Text');
        let regexp = window.LOCALS.regExp.coinOnBlur;

        if (regexp.test(rawText)) {
            element.value = Coin.prettifyCurrency(rawText);
        } else {
            element.value = '';
            $('#wrongCurrencyModal').modal('show');
            element.blur();
        }

        event.preventDefault();
    };

}