interface Document {
    consignacion_depositos: ConsignacionDepositosForm;
    consulta_planillas: ConsultaPlanillasForm;
    consulta_bloqueos: ConsultaBloqueosForm;
    cheque_rechazado: ChequeRechazadoForm;
    logProcesos: LogProcesosForm;
    consulta_beneficio_tributario: BeneficioTributarioForm;
    consulta_retiros: ConsultaRetirosForm;
    consulta_beneficio_cuenta: beneficioCuenta;
}

interface ConsignacionDepositosForm extends HTMLFormElement {
    lonPage: HTMLInputElement;
    idPate: HTMLInputElement;
    numCuentaF: HTMLInputElement;
    nombreTitularF: HTMLInputElement;
    idTitularF: HTMLInputElement;
    ahorroConBeneficioF: HTMLInputElement;
    retContingenteF: HTMLInputElement;
    febeneficioF: HTMLInputElement;
    ahorroSinBeneficioF: HTMLInputElement;
    valorTotalAhorroF: HTMLInputElement;
    certificadoF: HTMLInputElement;
    fuenteIngresosF: HTMLInputElement;
    mensajeFinacleF: HTMLInputElement;
    valorTotalAhorro_comparador: HTMLInputElement;
    retContingente_comparador: HTMLInputElement;
    ahorroConBeneficio_comparador: HTMLInputElement;
    ahorroSinBeneficio_comparador: HTMLInputElement;
    estadoConsignacionF: HTMLInputElement;
    guardarBtn: HTMLButtonElement;
    guardarBtn2: HTMLButtonElement;
    enviarBtn: HTMLButtonElement;
    enviarBtn2: HTMLButtonElement;
    cancelarBtn: HTMLButtonElement;
    cancelarBtn2: HTMLButtonElement;
    reversarPateBtn: HTMLButtonElement;
    reversarPateBtn2: HTMLButtonElement;
    filtrarBtn: HTMLButtonElement;
    limpiarBtn: HTMLButtonElement;
    nuevoBtn: HTMLButtonElement;
    reversar: HTMLInputElement;
    enviar: HTMLInputElement;
}

interface ConsultaPlanillasForm extends HTMLFormElement {
    lonPage: HTMLInputElement;
    idPate: HTMLInputElement;
    iEndPage: HTMLInputElement;
    iStartPage: HTMLInputElement;
    iUltimaPagina: HTMLInputElement;
    sizeLista: HTMLInputElement;
    lonTotalRegistrosConsulta: HTMLInputElement;
    flagreset: HTMLInputElement;
    tipoReporte: HTMLInputElement;
    pateF: HTMLInputElement;
    idPagadorF: HTMLInputElement;
    codSucursalF: HTMLInputElement;
    estadoPateF: HTMLInputElement;
    FE_Pate_desdeF: HTMLInputElement;
    FE_Pate_hastaF: HTMLInputElement;
}

interface ConsultaRetirosForm extends HTMLFormElement {
    lonPage: HTMLInputElement;
    idPate: HTMLInputElement;
    iEndPage: HTMLInputElement;
    iStartPage: HTMLInputElement;
    iUltimaPagina: HTMLInputElement;
    sizeLista: HTMLInputElement;
    lonTotalRegistrosConsulta: HTMLInputElement;
    flagreset: HTMLInputElement;
    retiroEmbargo: HTMLInputElement;
    retiroTraslado: HTMLInputElement;
}

interface ChequeRechazadoForm extends HTMLFormElement {
    fileInput: HTMLInputElement;
    cargar: HTMLButtonElement;
    aceptarBtn: HTMLButtonElement;
}

interface LogProcesosForm extends HTMLFormElement {
    proceso: HTMLSelectElement;
}

interface ConsultaBloqueosForm extends HTMLFormElement {
    filtarBtn: HTMLButtonElement;
    LimpiarBtn: HTMLButtonElement;
    btnExportarExcel: HTMLButtonElement;

    lonPage: HTMLInputElement;
    idPate: HTMLInputElement;
    iEndPage: HTMLInputElement;
    iStartPage: HTMLInputElement;
    iUltimaPagina: HTMLInputElement;
    sizeLista: HTMLInputElement;
    lonTotalRegistrosConsulta: HTMLInputElement;
    flagreset: HTMLInputElement;

    numCuentaF: HTMLInputElement;
    todosC: HTMLInputElement;
}

interface BeneficioTributarioForm extends HTMLFormElement {
    guardarBtn1?: HTMLButtonElement;
    guardarBtn2?: HTMLButtonElement;
    enviarBtn1?: HTMLButtonElement;
    enviarBtn2?: HTMLButtonElement;
    filtarBtn: HTMLButtonElement;
    LimpiarBtn: HTMLButtonElement;
    btnExportarExcel: HTMLButtonElement;

    lonPage: HTMLInputElement;
    idPate: HTMLInputElement;
    iEndPage: HTMLInputElement;
    iStartPage: HTMLInputElement;
    iUltimaPagina: HTMLInputElement;
    sizeLista: HTMLInputElement;
    lonTotalRegistrosConsulta: HTMLInputElement;
    flagreset: HTMLInputElement;
}

interface beneficioCuenta extends HTMLFormElement {
    guardarBtn1?: HTMLButtonElement;
    guardarBtn2?: HTMLButtonElement;
    enviarBtn1?: HTMLButtonElement;
    enviarBtn2?: HTMLButtonElement;
    filtarBtn: HTMLButtonElement;
    LimpiarBtn: HTMLButtonElement;
    btnExportarExcel: HTMLButtonElement;

    lonPage: HTMLInputElement;
    idPate: HTMLInputElement;
    iEndPage: HTMLInputElement;
    iStartPage: HTMLInputElement;
    iUltimaPagina: HTMLInputElement;
    sizeLista: HTMLInputElement;
    lonTotalRegistrosConsulta: HTMLInputElement;
    flagreset: HTMLInputElement;
}