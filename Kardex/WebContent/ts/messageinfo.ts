interface MessageInfo {
    descripcion?: string;
    mensaje_cuentanovalido?: string;
    fechaDesdeMayor?: string;
    debeTener?: string;
    cuentaNoExiste: string;
    laCuentaNoExiste: string;
    deseaEnviar?: string;
    deseaEnviarPlanillasParciales?: string;
    cuentaTieneCongelados?: string;
    cuentaPresentaBloqueos?: string;
    cuentaCancelada?: string;
    cuentaTieneSaldos?: string;
    formatoCuentaNoValido?: string;
    debeCrearUnRetiroConCancelacion?: string;
    valor?: string;
    superaValorTotalPlanilla?: string;
    errorAhorroSinBfConFechaBf?: string;
    TAfcDepositoFormFechabeneficioMayorFePate?: string;
    seleccionar?: string;
    tAdcConsultaBloqueosDescripcionBloqueo?: string;
    tAdcConsultaBloqueosDescripcionDesbloqueo?: string;
    tAdcConsultaBloqueosRazonBloqueo?: string;
    debeTenerColumna?: string;
    debeSeleccionarRangoFechas?: string;
    rangoFechasNoDebeSuperarDias?: string;
    rangoFechasnoCorrecto?: string;
}