interface Window {
    LOCALS: Locals;
}

interface Locals {
    dirty: boolean;
    inactivityTime: string;
    regExp: AfcRegExp;
    home: string | undefined;
    perfilSession: string;
    privileges?: MenuPrivileges;
    bt?: BeneficioTributario;
    dp?: DepositoPlanilla;
    pn?: ParametroNegocio;
    msg?: MessageInfo;
    cp?: ConsultaPlanilla;
    cb?: ConsultaBloqueos;
    lp?: LogProceso;
    tCat?: TipoCatalogo;
    currency: Currency;
    appDate: string;
    calendarDateFormat: string;
    errorType: boolean;
    hasSession: string;
    messageTime:number;
    dateFormat: string;
    diasRangoFechasConsulta?:number;
}

interface AfcRegExp {
    coinOnBlur: RegExp;
    coinOnKeypress: RegExp;
    coinIntegers: RegExp;
    accountNumberOnKeypress: RegExp;
    accountNumberOnBlur: RegExp;
    branch: RegExp;
    charactersAllowed: RegExp;
}

interface MenuPrivileges {
    create?: boolean;
    dump?: boolean;
    enquire?: boolean;
    modify?: boolean;
    remove?: boolean;
    reverse?: boolean;
    enquireAccount?: boolean;
    exportEnquiry?: boolean;
}

interface BeneficioTributario {
    formatoFecha: string;
    sizeLista: number;
    iUltimaPagina?: string;
    fechaHoy: string;
	sobrepasaLimiteMaximo:string;
}

interface DepositoPlanilla {
    dataChanged: boolean;
    iUltimaPagina: number;
    idPate: number;
    certificado: string;
    fuenteIngresos: string;
    estadoConsignacion: string;
    lockVersion: string;
    topeValor: string;
    sizeLista: number;
    sizeListaTotal: number;
}

interface ParametroNegocio {
    fechaHoy: string;
    formatoFecha: string;
    identifications: string;
}

interface ConsultaPlanilla {
    appDate: string;
    sizeLista: string;
    sobrepasaLimiteMaximo: boolean;
}

interface ConsultaBloqueos {
    sizeLista: number;
    sobrepasaLimiteMaximo: boolean;
    todosC: boolean;
}

interface LogProceso {
    sizeLista: number;
    sobrepasaLimiteMaximo: boolean;
}

interface TipoCatalogo {
    content: boolean;
}

interface Currency {
    symbol: string;
    separator: string;
    decimal: string;
    decimalPrecision: number;
    integerPrecision: number;
}