import { Coin } from "./coin";
import { inputCoinValidator } from "./currency.index";
import { forEachNodeList } from "./common";
import BigNumber from 'bignumber.js';

declare var token : string;
declare var header : string;

/**
 * Index for Template.jsp
 *
 * */

/**
 * @description Se configura primero la moneda
 * @author César
 * */
BigNumber.config({
    FORMAT: {
        // the decimal separator
        decimalSeparator: window.LOCALS.currency.decimal,
        // the grouping separator of the integer part
        groupSeparator: window.LOCALS.currency.separator,
        // the primary grouping size of the integer part
        groupSize: 3,
        // the secondary grouping size of the integer part
        secondaryGroupSize: 0,
        // the grouping separator of the fraction part
        fractionGroupSeparator: ' ',
        // the grouping size of the fraction part
        fractionGroupSize: 0
    }
});
Coin.config('$', {
    separator: window.LOCALS.currency.separator,
    decimal: window.LOCALS.currency.decimal,
    decimalPrecision: window.LOCALS.currency.decimalPrecision,
    integerPrecision: window.LOCALS.currency.integerPrecision
});

/**
 * Función que permite consultar el número de mensajes a mostrar
 */
function getNumberOfMessages() {
    let url = (window.LOCALS.home === "") ? window.LOCALS.home : `${window.LOCALS.home}afc/`;
    let perfilSession = window.LOCALS.perfilSession;

    $.ajax({
            type: "POST",
            contentType: "application/json",
            url: url + "consultarNumeroMensajes", 
            data: JSON.stringify(perfilSession),
            dataType: 'json',
            cache: false,
            beforeSend: function (data) {
                data.setRequestHeader(header, token);
                },
            success: function (data) {
        let span = <HTMLSpanElement>document.getElementById('numMensajes');
        if (span.innerText !== data.numeroMensajes
            && data.numeroMensajes !== 0) {
            span.style.display = "inline";

            while (span.firstChild) {
                span.removeChild(span.firstChild);
            }
            span.appendChild(document.createTextNode(data.numeroMensajes));

        } else if (data.numeroMensajes === 0) {
            span.style.display = "none";
        }
    }, 
            error: function (e) {
            }
       });
}


/***
 *      _____                 _
 *     | ____|_   _____ _ __ | |_ ___
 *     |  _| \ \ / / _ \ '_ \| __/ __|
 *     | |___ \ V /  __/ | | | |_\__ \
 *     |_____| \_/ \___|_| |_|\__|___/
 *
 */

window.addEventListener('load', () => {
    if (window.LOCALS.hasSession === "true") {
        setTimeout(getNumberOfMessages, 1000);
    }
    let inputCurrencyElements = document.getElementsByClassName('caracteres_moneda');
    forEachNodeList(inputCurrencyElements, (index, element) => {
        element.addEventListener('drop', function (event) {
            if (!event.target.readOnly) {
                if (event.dataTransfer.items) {
                    event.dataTransfer.items[0].getAsString(function (str) {
                        (<HTMLInputElement> event.target).value = Coin.prettifyCurrency(str);
                    });
                }
            }
        }, false);
        element.addEventListener('focus', function (event) {
            if (!event.target.readOnly) {
                let inputElement = <HTMLInputElement>event.target;
                Coin.clean(inputElement);
                Coin.addGroupSeparator(inputElement);
            }
        }, false);
        element.addEventListener('click', function (event) {
            if (!event.target.readOnly) {
                let inputElement = <HTMLInputElement>event.target;
                inputElement.setSelectionRange(inputElement.value.length, inputElement.value.length);
            }
        }, false);
        element.addEventListener('blur', function (event) {
            if (!event.target.readOnly) {
                Coin.formatInputCurrency(<HTMLInputElement>event.target, inputCoinValidator(window.LOCALS.regExp.coinOnBlur, <HTMLInputElement>event.target));
            }
        }, false);
        // Validador de moneda al pegar
        element.addEventListener('paste', function (event) {
            if (!event.target.readOnly) {
                Coin.pasteValidator(event);
            }
        }, false);
        // Formato de la moneda al teclear
        element.addEventListener('keypress', function (event) {
            if (!event.target.readOnly) {
                if (!/^(4[4689]|5[0-7])$/.test(event.keyCode)) {
                    event.preventDefault();
                    return;
                }
                let exp4Limit = new RegExp(`[${window.LOCALS.currency.decimal}]\\d{${window.LOCALS.currency.decimalPrecision},}`);
                if (exp4Limit.test(element.value)) {
                    event.preventDefault();
                    return;
                }
                let totalPlanillaText: string = element.value;
                let stringNumber = Coin.clean(totalPlanillaText);
                stringNumber += event.key.replace(/[.,]|Del/, window.LOCALS.currency.decimal);
                if (stringNumber.split(window.LOCALS.currency.decimal).length > 2) {
                    event.preventDefault();
                    return;
                }
                let cleanCurrency = Coin.cleanCurrency(stringNumber);
                var longestCurrency = window.LOCALS.currency.decimalPrecision + window.LOCALS.currency.integerPrecision + 2;
                if (cleanCurrency.length < longestCurrency && cleanCurrency.split(window.LOCALS.currency.decimal)[1].length < 3) {
                    element.value = Coin.addGroupSeparator(stringNumber);
                }
                event.preventDefault()
            }
        });
        element.addEventListener('keydown', (event) => {
            if (!event.key) return;
            let inputElement = <HTMLInputElement>event.target;
            if (!inputElement.readOnly) {
                if (event.key === 'ArrowLeft') event.preventDefault();
                if (event.key === 'Backspace') {
                    inputElement.value = inputElement.value.substring(0, inputElement.value.length - 1);
                    Coin.clean(inputElement);
                    Coin.addGroupSeparator(inputElement);
                    event.preventDefault();
                }
            }
        });
    });
}, false);

(<any>window).Coin = Coin;
(<any>window).BigNumber = BigNumber;