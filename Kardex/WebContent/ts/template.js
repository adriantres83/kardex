import { crearComportamientoSubmit, EstaRealizandoSubmit, disableF5 } from "./bloqueo_pantalla";

/***
 *      _____
 *     |  ___|   _ _ __
 *     | |_ | | | | '_ \
 *     |  _|| |_| | | | |
 *     |_|   \__,_|_| |_|
 *
 */

//Enter para consultar
$(document).keyup(function (e) {
    var $Modal = $("#myModalValidacion");
    var $Moda11 = $("#myModalValidacion1");
    var $Modal2 = $("#modalCaracteresEspeciales");
    var $Modal3 = $("#modalNumeroCuentaObligatorio");
    var $Modal4 = $("#myModalValidacionCheck");
    if (e.keyCode === 13 && $Modal.hasClass("in") || e.keyCode === 13 && $Modal2.hasClass("in") || 
    		e.keyCode === 13 && $Moda11.hasClass("in") || e.keyCode === 13 && $Modal3.hasClass("in") || e.keyCode === 13 && $Modal4.hasClass("in")) {
    	$Modal.modal("hide");
    	$Modal3.modal("hide");
    	$Moda11.modal("hide");
    	$Modal2.modal("hide");
    	$Modal4.modal("hide");
    }
});

function deshabilitaRetroceso() {
    window.location.hash = "no-back-button";
    window.location.hash = "Again-No-back-button"; //chrome
    window.onhashchange = function () {
        window.location.hash = "";
    }
}

// Para quitar la lupa de los campos filtros
function quitarLupaCampo() {
    $("input").focus(function () {
        $($(this).parent().find('label')).removeClass('glyphicon-search');
    });
}

// Para poner la lupa en los campos filtros
function ponerLupaCampo() {
    $("input").blur(function () {
        var campoValue = $(this).val();
        if (!campoValue) {
            $($(this).parent().find('label')).addClass('glyphicon-search');
        }
    });
}

// Para quitar la lupa en los campos filtros al cargar la página
function quitarLupaCampoLoad() {
    $(".lupa").each(function () {
        var campo = $(this).parent().find('input');
        var campoValue = $(campo).val();
        if (campoValue) {
            $(this).removeClass('glyphicon-search');
        }
    });
}

function LogoutApp() {
    EstaRealizandoSubmit();
    location.href = "/ModuloAFC/j_spring_security_logout";
}

function validarLogoutApp() {
    $("#modalValidarLogout").modal();
    $("#modalValidarLogout_aceptar").on("click", LogoutApp);

}

function linkNewWindow() {
	var form = document.createElement('FORM');
	form.method='POST';
	
    form.action = "afc/inicio";
	
	form.isNuevaVentana=true; //Si es nueva ventana para que no bloquee la pantalla
    window.open('../irInicio.jsp');
	
	form.submit();
	return false;
}

function linkNewChatbot() {
	var form = document.createElement('FORM');
	form.method='POST';
	
	form.action = "afc/chatbot";
	
	form.isNuevaVentana=true; //Si es nueva ventana para que no bloquee la pantalla
    window.open("../chatbotRedirect.jsp", "ChatBot", "toolbar=disabled, location=disabled, directories=disabled, status=disabled, " +
    		"menubar=disabled, scrollbars=disabled, status=disabled, resizable=disabled, width=433, height=413, top=240, left=900");

	form.submit();
	return false;
}

// Para los tooltip en las etiquetas
$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="popover2"]').popover({
        trigger: 'hover'
    });
    $("#linkLogout").on("click", validarLogoutApp);
    $("#linkNewWindow").on("click", linkNewWindow);
    $("#chatButton").on("click", linkNewChatbot);
    
   
    $("#cerrar").click(function () {
        $("#popup,#popup1").fadeOut();
    });

    //Cerrar modal clic cualquier parte
    $("html").click(function () {
        $("#popup,#popup1").fadeOut();
    });
    $(".sub").show();

    quitarLupaCampo();
    ponerLupaCampo();
    validarCajasTexto();

    //Scroll vertical y horizontal.
    $('.tabla2').bind('scroll', function () {
        $(".tabla1").scrollLeft($(this).scrollLeft());
        $(".popover").hide();
    });

    //Scroll vertical y horizontal.
    $('.tabla4').bind('scroll', function () {
        $(".tabla2").scrollTop($(this).scrollTop());
        $(".popover").hide();
    });

    //Scroll vertical y horizontal.
    $('.tabla1').bind('scroll', function () {
        $(".tabla2").scrollLeft($(this).scrollLeft());
        $(".popover").hide();
    });


    uppercaseCampo();
});

$(window).load(function () {
    quitarLupaCampoLoad();
    $(".sub").hide();
    crearComportamientoSubmit();
    // To | f5
    /* jQuery < 1.7 */
    $(document).bind("keydown", disableF5);
    /* OR jQuery >= 1.7 */
    $(document).on("keydown", disableF5);
    var valida = window.LOCALS.errorType;
    if (valida) $('#modalSistemaCierre').modal('show');
    
  //Manejo de filtro expansion
  	$("#arrow-up,#arrow-down").click(function () {
  	    $("#FiltroEscondido, #contenido").slideToggle("slow");

  	    if ($("#arrow-up").css("display") == "block") {
  	        $("#arrow-up").css("display", "none");
  	        $("#arrow-down").css("display", "block");
  	    } else {
  	        $("#arrow-up").css("display", "block");
  	        $("#arrow-down").css("display", "none");
  	    }
  	});
});

// Cierra los tooltip de las etiquetas al dar click en cualquier parte de la pantalla
$(document).on('click',
    function (e) {
        $('[data-toggle="popover"]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0
                && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

/**
 * Valida la expresión regular para validar caracteres permitidos
 * en todas las cajas de texto
 */
function validarCajasTexto() {
    var patron;
    var inputOrTextarea = $("input, textarea");
    inputOrTextarea.keypress(function (e) {
        var te1 = $(this).val();
        var tecla = (document.all) ? e.keyCode : e.which;
        if (tecla === 8) return true;
        var te = te1 + String.fromCharCode(tecla);

        // Valida caracteres especiales permitidos en todos los campos de la aplicación en el evento keypress
        if ($(this).hasClass("excluir_caracteres") === false) {
            patron = window.LOCALS.regExp.charactersAllowed;
            validarExpresionRegularKeypress(e, patron, te);
        }

        /**
         * Valida caracteres numéricos con puntos y coma para decimales
         * permitidos en todos los campos moneda en el evento keypress.
         * */
        if ($(this).hasClass("caracteres_moneda") === true) {
            patron = window.LOCALS.regExp.coinOnKeypress;
            validarExpresionRegularKeypress(e, patron, te);
        }

        /**
         * Valida caracteres numéricos enteros permitidos en todos los campos
         * numéricos enteros en el evento keypress.
         */
        if ($(this).hasClass("caracteres_enteros") === true) {
            patron = window.LOCALS.regExp.integers;
            validarExpresionRegularKeypress(e, patron, te);
        }

        /**
         * Valida caracteres numéricos enteros permitidos en todos los campos
         * numéricos enteros en el evento keypress.
         */
        if ($(this).hasClass("numero_cuenta") === true) {
            patron = window.LOCALS.regExp.accountNumberOnKeypress;
            validarExpresionRegularKeypress(e, patron, te);
        }
    });

    inputOrTextarea.blur(function () {
        var cadena = $(this).val();

        // Valida caracteres especiales permitidos en todos los campos de la aplicación en el evento blur
        if ($(this).hasClass("excluir_caracteres") === false) {
            patron = window.LOCALS.regExp.charactersAllowed;
            validarExpresionRegularBlur(patron, cadena, this);
        }

        // Valida caracteres numéricos con puntos y coma para decimales
        // permitidos en todos los campos moneda en el evento blur
        if ($(this).hasClass("caracteres_moneda") === true) {
            patron = window.LOCALS.regExp.coinOnBlur;
            validarExpresionRegularBlur(patron, cadena, this);
        }

        // Valida caracteres numéricos enteros permitidos en todos los campos
        // numéricos enteros en el evento blur
        if ($(this).hasClass("caracteres_enteros") === true) {
            patron = window.LOCALS.regExp.integers;
            validarExpresionRegularBlur(patron, cadena, this);
        }

        // Valida caracteres numéricos enteros permitidos en todos los campos
        // numéricos enteros en el evento blur
        if ($(this).hasClass("numero_cuenta") === true) {
            patron = window.LOCALS.regExp.accountNumberOnKeypress;
            validarExpresionRegularBlur(patron, cadena, this);
        }
    });
}

/**
 * Validar la expresión regular para validar caracteres permitidos en el evento keypress
 */
function validarExpresionRegularKeypress(event, patron, texto) {
    if (!patron.test(texto)) {
        if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
        return false;
    }
}

/**
 * Validar la expresión regular para validar caracteres permitidos en el evento blur
 */
function validarExpresionRegularBlur(patron, cadena, input) {
    if (cadena && !patron.test(cadena)) {
        $(input).val('');
        ponerLupaCampo($(input).attr('id'));
        $('#modalCaracteresEspeciales').modal('show');
    }
}

/**
 * Validar la expresión regular para validar caracteres permitidos en el evento blur
 */
export function validarExpresionNumeroCuenta(cadena) {
    var patron = window.LOCALS.regExp.accountNumberOnKeypress;
    var patron1 = window.LOCALS.regExp.accountNumberOnBlur;
    return !!(cadena && (!patron.test(cadena) || !patron1.test(cadena)));
}

// Función para poner uppercase a los campos código oficina
function uppercaseCampo() {
    $(".uppercase").blur(function () {
        $(this).val($(this).val().toUpperCase());
    });
}

$(document).ready(function () {
    var tablasDatos = document.getElementsByClassName("table table-bordered table-striped table-condensed");
    for (var i = 0; i < tablasDatos.length; i++) {
        var tabla = tablasDatos[i];
        $(tabla).bind('mousewheel', function (event) {
            try {
                var caja = $(this).parent().parent().find(".tabla4");
                if (caja.length > 0) {
                    $(caja).scrollTop($(caja).scrollTop() + event.originalEvent['deltaY']);
                } else {
                    caja = $(this).parent().parent().find(".tabla2");
                    $(caja).scrollTop($(caja).scrollTop() + event.originalEvent['deltaY']);
                }
            }
            catch (e) {
            }
            return false;
        });
    }
    	
});




/***
 *      _____                 _
 *     | ____|_   _____ _ __ | |_ ___
 *     |  _| \ \ / / _ \ '_ \| __/ __|
 *     | |___ \ V /  __/ | | | |_\__ \
 *     |_____| \_/ \___|_| |_|\__|___/
 *
 */

window.addEventListener('load', deshabilitaRetroceso, false);

var logoAnchor = document.getElementById('logoWrapper');
var topLogoAnchor = document.getElementById('topLogoWrapper');
var headerInboxBarLI = document.getElementById('header_inbox_bar');
logoAnchor.addEventListener('click', EstaRealizandoSubmit, false);
topLogoAnchor.addEventListener('click', EstaRealizandoSubmit, false);
headerInboxBarLI.addEventListener('click', function () {
    EstaRealizandoSubmit();
    location.href = '../irInicio.jsp';
}, false);
