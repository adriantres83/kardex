import { forEachNodeList } from "../common";

window.seleccionarNuevo = false;
$("#filtarBtn").click(function () {
    consultarFiltro();
});

$("#limpiarBtn").click(function () {
    limpiar();
});

$("#nuevoBtn").click(function () {
    window.seleccionarNuevo = true;
    ModificarUsuario();
});

$("#closeBtn2").click(function () {
    if (window.seleccionarNuevo) {
        limpiar();
    } else {
        consultar();
    }
});

$("#kardex").keypress(function () {
    datosModificados();
});

$("#kardexForm").keypress(function (event) {
    return stopRKey(event)
});

$("#idPerfil").change(function () {
    datosModificados();
});

$("#codigoPerfil").change(function () {
    datosModificados();
});

$("#habilitado").change(function () {
    datosModificados();
});

$("#btnCancelar").click(function () {
    if (window.seleccionarNuevo) {
        limpiar();
    } else {
        consultar();
    }
});

$("#btnGuardar").click(function () {
    searchViaAjax();
});

$("#closeBtn1").click(function () {
    if (window.seleccionarNuevo) {
        limpiar();
    } else {
        consultar();
    }
})

$("#cancelarBtn").click(function () {
    if (window.seleccionarNuevo) {
        limpiar();
    } else {
        consultar();
    }
})

$("#btnEliminar").click(function () {
    eliminar();
});

$("#mo_aceptar_noguardo").click(function () {
    consultar();
});

//Enter para consultar
$('body').keyup(function (e) {
    var form = document.kardexForm;
    if (e.keyCode == 13 && !$(".wc-shellinput").is(":focus") && form.filtarBtn.disabled != true && !$("#modalCaracteresEspeciales").hasClass("in")) {
        consultarFiltro();
    }
});
var paginationAnchorList = $('.pagination-log');
var paginationAnchorOnClick = function (page) {
    document.kardexForm.lonPage.value = page;
    consultar();
};
paginationAnchorList.each(function (index, elem) {
    elem.addEventListener('click', paginationAnchorOnClick.bind(null, elem.dataset.page), false);
});

var modificarUsuarioEvt = function (rowSet) {
    document.kardexForm.kardex.readOnly = true;
    document.kardexForm.kardex.disabled = true;
    functionModal();
    ModificarUsuario(rowSet);
};

var eliminarUsuarioEvt = function (id) {
    EliminarUsuario(id);
};

var actionModalNodeList = document.getElementsByClassName('ModificarBtn');
var actionModalOnClick = function (index, elem) {
    elem.addEventListener('click', modificarUsuarioEvt.bind(null, elem.dataset.row), false);
};
forEachNodeList(actionModalNodeList, actionModalOnClick);

$(".EliminarBtn").each(function (index, elem) {
    elem.addEventListener('click', eliminarUsuarioEvt.bind(null, $(elem).attr("alt")), false);
});

// Limpiar filtro
function limpiar() {
    var form = document.getElementById("kardexForm");
    form.consultarTabla.value = "";
    form.kardexF.value = "";
    form.nombrePerfilF.value = "";
    form.habilitadoF.value = "";
    form.idPerfilF.value = "";
    form.lonPage.value = "";

    form.submit();
}

//Funcion que bloque el enter para que no saque error de POST
function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target
        : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type == "text")) {
        return false;
    }
}

// Funcion para identificar cambios en los campos del modal nuevo
function datosModificados() {
    if (document.getElementById('btnGuardar') != null) {
        $("#btnGuardar").prop("disabled", false);
    }
}

$('#kardex').keydown(function (tecla) {
    if (tecla.ctrlKey == true && tecla.keyCode == 86 || tecla.keyCode == 8) {
        datosModificados();
    }
});

//Funcion para validar campos de entrada
function validacionFiltro() {
    var idPerfilF = document.getElementById('idPerfilF').value;
    if (idPerfilF != "") {
        if (!window.LOCALS.regExp.accountNumberOnKeypress.test(idPerfilF)) {
            $("#idPerfilF").val('');
            $("#idPerfilF").focus();
            $('#modalCaracteresEspeciales').modal('show');
            return false;
        }
    }
    return true;
}

// Consultar
function consultar() {
    document.getElementById('consultaPaginacion').value = "S";
    var form = document.kardexForm;
    if (window.LOCALS.privileges.enquire) {
        var btnfiltrar = document.getElementById("filtarBtn");
        if (btnfiltrar !== null) {
            btnfiltrar.disabled = true;
        }
        var btnLimpiar = document.getElementById("limpiarBtn");
        if (btnLimpiar !== null) {
            btnLimpiar.disabled = true;
        }

        var nueveBtn = document.getElementById('nuevoBtn');
        if (nueveBtn !== null) {
            nueveBtn.disabled = true;
        }
        var label2V = document.getElementById("loading2").style.display = 'block';
        form.consultarTabla.value = "S";
        form.loading2 = label2V;
        form.submit();
    }
}

// Consultar filtro
function consultarFiltro() {
    document.getElementById('consultaPaginacion').value = "N";
    if (validacionFiltro()) {
        var btnfiltrar = document.getElementById("filtarBtn");
        if (btnfiltrar !== null) {
            btnfiltrar.disabled = true;
        }
        var btnLimpiar = document.getElementById("limpiarBtn");
        if (btnLimpiar !== null) {
            btnLimpiar.disabled = true;
        }

        var nueveBtn = document.getElementById('nuevoBtn');
        if (nueveBtn !== null) {
            nueveBtn.disabled = true;
        }
        var label2V = document.getElementById("loading2").style.display = 'block';
        var form = document.kardexForm;
        form.consultarTabla.value = "S";
        form.loading2 = label2V;
        form.submit();
    }
}

function ModificarUsuario(rawData) {
    $('#myModal #feedback').html("");

    if (rawData) {
    	var rowSet = JSON.parse(rawData);
    	
        $('#idKardex').val(rowSet.idKardex);
        $('#producto').val(rowSet.producto);
        $('#tipoOperacion').val(rowSet.tipoOperacion);
        $('#cantidadEntrada').val(rowSet.cantidadEntrada);
        $('#valorTotalEntrada').val(rowSet.valorTotalEntrada);
        $('#cantidadSalida').val(rowSet.cantidadSalida);
        
    } else {
        $('#idKardex').val("");
        $('#producto').val("");
        $('#tipoOperacion').val("");
        $('#cantidadEntrada').val("");
        $('#valorTotalEntrada').val("");
        $('#cantidadSalida').val("");
    }
}


// Funcion modificar
function searchViaAjax() {

    // Prevent the form from submitting via the browser.
    event.preventDefault ? event.preventDefault()
        : (event.returnValue = false);

    var search = {};
    search["idKardex"] = $("#idKardex").val();
    search["producto"] = $("#producto").val();
    search["tipoOperacion"] = $("#tipoOperacion").val();
    search["cantidadEntrada"] = $("#cantidadEntrada").val();
    search["valorTotalEntrada"] = $("#valorTotalEntrada").val();
    search["cantidadSalida"] = $("#cantidadSalida").val();
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: window.LOCALS.home + "afc/agregar_modificar_kardex",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 100000,
        beforeSend: function (data) {
            data.setRequestHeader(header, token);
            $('#myModalConfirmar').modal('hide');
            $("#btnGuardar").prop("disabled", true);
            $("#btnCancelar").prop("disabled", true);
            $("#loading").show();
        },
        success: function (data) {

            $("#btnCancelar").prop("disabled", false);
            $("#loading").hide();

            if (data.status === "lock_version") {
                $('#myModalLockVersion').modal('show');
            } else {
                if (data.status === "SUCCESS") {
                	 document.kardexForm.kardex.readOnly = true;
                	 document.kardexForm.kardex.disabled = true;
                    $("#idUsuario").val(data.codigo);
                    $("#lockVersion").val(data.lockVersion);
                    display(data);
                } else {
                    var errorInfo = "";
                    $("#btnGuardar").prop("disabled", false);
                    var feedback = $('#myModal #feedback');
                    feedback.html("");

                    for (var i = 0; i < data.result.length; i++) {
                        errorInfo += "<br>"
                            + (i + 1)
                            + ". <a href='javascript:void(0);' style='color:#a94442;'  onclick=$('#myModal"
                            + "').find('#"
                            + data.result[i].field
                            + "').focus()>"
                            + data.result[i].code
                            + "</a>";
                    }
                    $(feedback).append("<div class='alert alert-danger alert-dismissible' role='alert'><span class='glyphicon glyphicon-remove-sign' aria-hidden='true'></span><strong> Error." + "<br> " + data.message + errorInfo + "</strong><br/></div>");
                }
            }
        },
        error: function (e) {
            console.log("ERROR: ", e);
        },
        done: function (e) {
            console.log("DONE");
            enableSearchButton(true);
        }
    });

    function display(data) {

        var feedback = $('#myModal #feedback');
        feedback.html("");

        if (data.error_type == "C") {
            $(feedback)
                .append("<div class='alert alert-success alert-dismissible' role='alert'><span class='glyphicon glyphicon-ok-sign' aria-hidden='true'></span><strong> " + "" + data.message + "</strong><br/></div>");
        }

        if (data.error_type == "E") {
            $(feedback)
                .append("<div class='alert alert-danger alert-dismissible' role='alert'><span class='glyphicon glyphicon-remove-sign' aria-hidden='true'></span><strong> Error." + "<br> " + data.message + "</strong><br/></div>");
        }

        if (data.error_type == "A") {
            $(feedback)
                .append("<div class='alert alert-warning alert-dismissible' role='alert'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span><strong> Alerta." + "<br> " + data.message + "</strong><br/></div>");
        }
    }




    function display(data) {

        var feedbackEliminar = $('#myModalEliminar #feedbackEliminar');
        feedbackEliminar.html("");

        if (data.error_type == "C") {
            $(feedbackEliminar)
                .append("<div class='alert alert-success alert-dismissible' role='alert'><span class='glyphicon glyphicon-ok-sign' aria-hidden='true'></span><strong> Exitoso. " + "<br> " + data.message + "</strong><br/></div>");
        }

        if (data.error_type == "E") {
            $(feedbackEliminar)
                .append("<div class='alert alert-danger alert-dismissible' role='alert'><span class='glyphicon glyphicon-remove-sign' aria-hidden='true'></span><strong>" + " " + data.message + "</strong><br/></div>");
        }

        if (data.error_type == "A") {
            $(feedbackEliminar)
                .append("<div class='alert alert-warning alert-dismissible' role='alert'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span><strong>" + " " + data.message + "</strong><br/></div>");
        }
    }
}

//Funcion para desplegar modal     
function functionModal() {
    $('#myModal').modal('show');
}
