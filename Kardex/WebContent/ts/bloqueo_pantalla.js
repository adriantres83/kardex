/**
 * @description Blocks the screen when a request is being processed.
 */
// variables globales
var ScreenLockModal;
var ScreenLoad;
var interno;

var ScreenLockModalTransparente;
var ScreenLoadTransparente;
var internoTransparente;

// Cierra el bloqueo de pantalla
export function CerrarScreenLock() {
	$(ScreenLockModal).modal("hide");
	$(document).unbind('keydown');
}

// Muestra el bloqueo de pantalla
export function AbrirScreenLock() {
	$(ScreenLockModal).modal({
		backdrop : 'static',
		keyboard: false
	});
	
	var resizerAbrirScreenLock = function() {
		$(interno).css({
			position : 'absolute',
			left : ($(window).width() - 50) / 2,
			top : ($(window).height() - 130) / 2
		})
	};
	$(window).bind("resize", resizerAbrirScreenLock);
	$(window).resize();
}




// Crear el modal para el bloqueo de pantalla
function crearScreenLockModal() {
	ScreenLockModal = document.createElement("DIV");
	$("body").append(ScreenLockModal);
	$(ScreenLockModal).addClass('modal fade');
	$(ScreenLockModal).attr('role', 'dialog');
	interno = document.createElement("DIV");
	$(ScreenLockModal).append(interno);
	$(interno).addClass('modal-dialog modal-lg');
	$(interno).attr('style',
			'width: 100px;height:100px;margin:0px; padding:0px');
	var interno2 = document.createElement("DIV");
	$(interno2).attr('style', 'margin:0px;');
	$(interno).append(interno2);
	var logo = document.createElement("i");
	$(logo).addClass('fa fa-circle-o-notch fa-spin');
	$(interno).attr('style', 'font-size:50px; color: #add2e8;');
	$(interno2).append(logo);
}


function crearScreenLockModalTransparente() {
	ScreenLockModalTransparente = document.createElement("DIV");
	$("body").append(ScreenLockModalTransparente);
	$(ScreenLockModalTransparente).addClass('modal fade');
	$(ScreenLockModalTransparente).attr('role', 'dialog');
	$(ScreenLockModalTransparente).attr('style', 'background:white;  opacity: 0 !important;');
	internoTransparente = document.createElement("DIV");
	$(ScreenLockModalTransparente).append(internoTransparente);
	$(internoTransparente).addClass('modal-dialog modal-lg');
	$(internoTransparente).attr('style',
			'width: 100px;height:100px;margin:0px; padding:0px;background:white;  opacity: 0 !important;');
	var internoTransparente2 = document.createElement("DIV");
	$(internoTransparente2).attr('style', 'margin:0px;');
	$(internoTransparente).append(internoTransparente2);
	var logo = document.createElement("i");
	$(internoTransparente).attr('style', 'font-size:50px; color: #add2e8;');
	
}

function AbrirScreenLockTransparente(){	
	$(document).bind('keydown', function(e){ e.stopPropagation(); return false;});
	$(ScreenLockModalTransparente).modal({
		backdrop : 'static',
		keyboard: false
	});
	
	var resizerAbrirScreenLockTransparente = function() {
		$(internoTransparente).css({
			position : 'absolute',
			left : ($(window).width() - 50) / 2,
			top : ($(window).height() - 130) / 2
		})
	};
	$(window).bind("resize", resizerAbrirScreenLockTransparente);
	$(window).resize();
	$( ".modal-backdrop").css( "opacity", "0" );
}
function CerrarScreenLockTransparente() {
	$(ScreenLockModalTransparente).modal("hide");
}

var comportamientocreado=false;;
var realizandoSubmit=false;

export function crearComportamientoSubmit(){
	document.oncontextmenu = function(){return false};
	$("a").on("click",EstaRealizandoSubmit);	
	if(!comportamientocreado){
		matricularConfirmacionCierre();
		comportamientocreado=true;
		crearScreenLockModal();	
		crearScreenLockModalTransparente();
		HTMLFormElement.prototype._submit_bloqueo_pantalla = HTMLFormElement.prototype.submit;
		HTMLFormElement.prototype.submit = function() {
			realizandoSubmit=true;
			if(!(this.isdescarga || this.isNuevaVentana)){
							
				AbrirScreenLockTransparente();	
				var targetfound=document.getElementById(this.target);
				if (targetfound!==null){
					$(targetfound).bind("load",CerrarScreenLock);
					$(targetfound).load(function () {
						  this.contentWindow.onbeforeunload = null;
						});
				}			
				if(this.ismenu){
					setTimeout(AbrirScreenLock,2000);
					setTimeout(CerrarScreenLockTransparente,2000);
				}else{
					
					setTimeout(AbrirScreenLock,0);
					setTimeout(CerrarScreenLockTransparente,0);
				}    
			}
			this._submit_bloqueo_pantalla();	
		};
		
		
	}
}
// Sobrrescribe funcion sumbit para poder llamar el metodo AbrirScreenLock



function matricularConfirmacionCierre(){

    window.onbeforeunload = confirmarCierreAplicacion;    
  
    $("body").on('mouseleave', changeRealizandoSubmit);
    $(document).bind("keydown",changeRealizandoSubmit);
}

function changeRealizandoSubmit(){	
	realizandoSubmit=false;
}

export function EstaRealizandoSubmit(){
	realizandoSubmit=true;	
}

//slight update to account for browsers not supporting e.which
export function disableF5(e) { if ((e.which || e.keyCode) === 116) e.preventDefault(); }


function confirmarCierreAplicacion(e){
	console.log("confirmarCierreAplicacion");
	if(realizandoSubmit){	
		
	}else{			
		return "¿Desea realmente salir?";
	}
}