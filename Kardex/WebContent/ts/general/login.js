	$("body").load(function(){
		deshabilitaRetroceso();
	});
	    $(window).load(function(){
	    	if(window.vars.mensaje)
	        	$('#myModal1').modal('show');
	    });
	    
	    function deshabilitaRetroceso(){
	        window.location.hash="no-back-button";
	        window.location.hash="Again-No-back-button"; //chrome
	        window.onhashchange=function(){window.location.hash="";}
	    }
	    var capslock=false;
	   
	    function matricularCapsLock(input){
	    	$( input).keypress( function(e) { 
	    		  this.pressionteclado=true;
		    	  var key_check = e.which;
		    	  var isUp = ( key_check >= 65 && key_check <= 90 ) ? true : false;
		    	  var isLow = ( key_check >= 97 && key_check <= 122 ) ? true : false;
		    	  var isShift = ( e.shiftKey ) ? e.shiftKey : ( ( key_check == 16 ) ? true : false );
		    	  if ( ( isUp && !isShift ) || ( isLow && isShift ) ) {
		    		  $(this).popover('show');
		    		  this.capslock=true;
		    	  }else{
		    		  $(this).popover('hide');
		    		  this.capslock=false;
		    	  }
		    });
	    	
	    	$( input).keyup( function(e) {  
	    		
	    		if(this.pressionteclado){
		    		if(e.which==20){
		    			this.capslock=!this.capslock;
			    		if(this.capslock){
			    			 $(this).popover('show');
			    		}else{
				    		  $(this).popover('hide');
			    		}
		    		}
	    		}
	    	});	
	    	
	    	$( input).focusout(function() { 
	    		$(this).popover('hide'); 
	    		this.pressionteclado=false;
	    		});
	    	
	    	$( input).focus(function() { 
	    		if(this.pressionteclado){
		    		if(this.capslock){
		    			 $(this).popover('show');
		    		}else{
			    		  $(this).popover('hide');
		    		}
	    		}else{
		    		  $(this).popover('hide');
	    		}
	    	});
	    }
	    
	    $(window).load(function(){
	    	matricularCapsLock( "#j_password");
	    });
