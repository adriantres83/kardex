/**
 *  @description Muestra un mensaje de información luego de realizar una consulta de Ajax.
 *  @param {Event} data Error de Ajax.
 * */
export function display(data) {
    var $feedback = $('#feedback');
    $feedback.html("");
    let type;
    if (data.error_type === "C") {
        type = {
            divClass: 'alert-dismissible',
            spanClass: 'glyphicon-ok-sign',
            message: 'Exitoso.'
        };
        var content = `<div class='alert alert-success ${type.divClass}' role='alert'><span class='glyphicon glyphicon-ok-sign ${type.spanClass}' aria-hidden='true'></span><strong> ${type.message}<br> ${data.message}</strong><br/></div>`;
        $feedback.append(content);
    }
    if (data.error_type === "E") {
        $feedback.append(`<div class='alert alert-danger alert-dismissible' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><strong> ${data.message}</strong><br/></div>`);
    }
    if (data.error_type === "A") {
        $feedback.append(`<div class='alert alert-warning alert-dismissible' role='alert'><span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span><strong> Alerta.<br> ${data.message}</strong><br/></div>`);
    }
}

/**
 *  @description Itera cada elemento de un NodeList
 *  @example
 *  forEachNodeList(filterInputArray, (index, ele: HTMLInputElement) => {
 *      ele.addEventListener('change', lockExcel, false);
 *  });
 *  @param {NodeList} array Vector con elementos HTML.
 *  @param {Function} callback Llamado en cada iteración como: function([scope,]index, element)
 *  @param scope En caso de necesitar el Scope.
 * */
export let forEachNodeList = function (array: HTMLCollectionOf<Element> | NodeList, callback: Function, scope?) {
    for (let i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]); // passes back stuff we need
    }
};

/**
 * @description Bloquea botón de excel al modificar campos en el formulario para forzar la consulta
 * */
export let lockExcel = function (): void {
    let btnExportarExcel = <HTMLButtonElement>document.getElementById('btnExportarExcel');
    if (btnExportarExcel) btnExportarExcel.disabled = true;
};

let fechaHoy = window.LOCALS.appDate;
let options = {
    format: window.LOCALS.calendarDateFormat,
    container: 'body',
    orientation: 'bottom auto',
    language: 'es',
    daysOfWeekHighlighted: '0',
    autoclose: true,
    clearBtn: true,
    beforeShowDay: function (date) {
        let dateFormat = ("0" + date.getDate()).slice(-2) + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getFullYear();
        if (dateFormat === fechaHoy) return {classes: 'today'};
    },
};

/**
 * @description Asigna un DatePicker con la fecha de hoy.
 * @example
 * setDateOnDatepicker('fechaDesdeBloqueoF');
 * @param nameField Nombre del HTMLInputElement donde irá el DatePicker
 * */
export let setDateOnDatepicker = function (nameField: String) {
    let date_input = $(`input[name="${nameField}"]`);
    if (date_input.val()) {
        date_input.datepicker(options);
    } else {
        date_input.datepicker(options);
        date_input.datepicker('setDate', fechaHoy);
        date_input.datepicker('update');
        date_input.val('');
    }
     date_input.datepicker().on('changeDate', function (e) {
         (<HTMLInputElement>e.currentTarget).focus();
        setTimeout(function(){ $('.datepicker').hide(); }, 100);
    });

    date_input.click(function (e) {
         (<HTMLInputElement>e.currentTarget).blur();
         (<HTMLInputElement>e.currentTarget).focus();
    });
    
};

/**
 * @description Solo permite ingresar enteros y '-' en los campos de fechas.
 * @return {boolean} Falso si hay que detener el evento.
 * */
export function entero2() {
    let e_k = event['keyCode'];
    if (e_k < 45 || e_k > 57 || e_k === 46 || e_k === 47) {
        if (event.preventDefault) event.preventDefault(); else event.returnValue = false;
        return false;
    }
}

/**
 * @description Valida la expresión regular para validar caracteres permitidos en el evento blur.
 * @param cadena El texto a validar.
 * @return {boolean} Verdadero si hay caracteres permitidos.
 */
export function validarExpresionNumeroCuenta(cadena: string) {
    var patron = window.LOCALS.regExp.accountNumberOnKeypress;
    var patron1 = window.LOCALS.regExp.accountNumberOnBlur;
    return !!(cadena && (!patron.test(cadena) || !patron1.test(cadena)));
}