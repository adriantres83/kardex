package com.kardex.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.kardex.model.Kardex;

public class KardexDao {
	
	public void insertarKardex(Connection sqlLite, Kardex kardex) throws Exception {
		Statement stmt = null;
		String sql1;
		try {
			
			stmt = sqlLite.createStatement();
			
			sql1 = "INSERT INTO Kardex (fecha, producto, tipoOperacion, cantidadEntrada, "
					+ "valorUnitarioEntrada, valorTotalEntrada, cantidadSalida, valorUnitarioSalida, valorTotalSalida"
					+ "cantidadSaldo, valorUnitarioSaldo, valorTotalSaldo)" + 
			"VALUES ('" + kardex.getFecha() + "','" + kardex.getProducto() + "','" 
					+ kardex.getTipoOperacion() + "','" + kardex.getCantidadEntrada() + "','"
					+ kardex.getValorUnitarioEntrada() + "','" + kardex.getValorTotalEntrada() + "','"
					+ kardex.getCantidadSalida() + "','" + kardex.getValorUnitarioSalida() + "','"+ kardex.getValorTotalSalida() + "','"
					+ kardex.getCantidadSaldo() + "','" + kardex.getValorUnitarioSaldo() + "','"
					+ kardex.getValorTotalSaldo() + "');";
			stmt.executeUpdate(sql1);
			sqlLite.commit();

		}
		finally {
			if (stmt != null ) {try {stmt.close();} catch (Exception e) {}}
		}
	}
	
	public Integer cantidadSaldo(Connection sqlLite, Kardex kardex) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		Integer resultado = null;
		try {
			stmt = sqlLite.createStatement();
			sql = "select max(idKardex), cantidadSaldo from kardex where producto='" + kardex.getProducto() + "';";
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
				resultado = rs.getInt(2); 
			}
		} finally {
			if (rs != null ) {try {rs.close();} catch (Exception e) {}}
			if (stmt != null ) {try {stmt.close();} catch (Exception e) {}}
		}
		return resultado;
	}
	
	/**
	 * Permite traer la lista completa de kardex
	 * 
	 * @return
	 */
	public List<Kardex> consultarKardexs(Connection sqlLite, Kardex kardex) throws Exception{
		List<Kardex> resultado = new ArrayList<Kardex>();
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		Kardex kardexRespuesta = null;
		try {
			stmt = sqlLite.createStatement();
			sql = "select idKardex,fecha, producto, tipoOperacion, cantidadEntrada," + 
			"valorUnitarioEntrada, valorTotalEntrada, cantidadSalida, valorUnitarioSalida, valorTotalSalida," + 
			"cantidadSaldo, valorUnitarioSaldo, valorTotalSaldo from kardex  where producto='" + kardex.getProducto() + "';";
			rs = stmt.executeQuery(sql);
			while ( rs.next() ) {
				kardexRespuesta = new Kardex();
				kardexRespuesta.setIdKardex(rs.getInt(1));
				kardexRespuesta.setFecha(rs.getDate(2));
				kardexRespuesta.setProducto(rs.getString(3));
				kardexRespuesta.setTipoOperacion(rs.getString(4));
				kardexRespuesta.setCantidadEntrada(rs.getInt(5));
				kardexRespuesta.setValorUnitarioEntrada(rs.getBigDecimal(6));
				kardexRespuesta.setValorTotalEntrada(rs.getBigDecimal(7));
				kardexRespuesta.setCantidadSalida(rs.getInt(8));
				kardexRespuesta.setValorUnitarioSalida(rs.getBigDecimal(9));
				kardexRespuesta.setValorTotalSalida(rs.getBigDecimal(10));
				kardexRespuesta.setCantidadSaldo(rs.getInt(11));
				kardexRespuesta.setValorUnitarioSaldo(rs.getBigDecimal(12));
				kardexRespuesta.setValorTotalSaldo(rs.getBigDecimal(13));
				resultado.add(kardexRespuesta); 
			}
		} finally {
			if (rs != null ) {try {rs.close();} catch (Exception e) {}}
			if (stmt != null ) {try {stmt.close();} catch (Exception e) {}}
		}
		return resultado;
	}
	
	public Kardex ultimoKardex(Connection sqlLite, Kardex kardex) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		Kardex kardexRespuesta = null;
		try {
			stmt = sqlLite.createStatement();
			sql = "select max(idKardex),fecha, producto, tipoOperacion, cantidadEntrada," + 
					"valorUnitarioEntrada, valorTotalEntrada, cantidadSalida, valorUnitarioSalida, valorTotalSalida," + 
					"cantidadSaldo, valorUnitarioSaldo, valorTotalSaldo from kardex where producto='" + kardex.getProducto() + "';";
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
				kardexRespuesta = new Kardex();
				kardexRespuesta.setIdKardex(rs.getInt(1));
				kardexRespuesta.setFecha(rs.getDate(2));
				kardexRespuesta.setProducto(rs.getString(3));
				kardexRespuesta.setTipoOperacion(rs.getString(4));
				kardexRespuesta.setCantidadEntrada(rs.getInt(5));
				kardexRespuesta.setValorUnitarioEntrada(rs.getBigDecimal(6));
				kardexRespuesta.setValorTotalEntrada(rs.getBigDecimal(7));
				kardexRespuesta.setCantidadSalida(rs.getInt(8));
				kardexRespuesta.setValorUnitarioSalida(rs.getBigDecimal(9));
				kardexRespuesta.setValorTotalSalida(rs.getBigDecimal(10));
				kardexRespuesta.setCantidadSaldo(rs.getInt(11));
				kardexRespuesta.setValorUnitarioSaldo(rs.getBigDecimal(12));
				kardexRespuesta.setValorTotalSaldo(rs.getBigDecimal(13)); 
			}
		} finally {
			if (rs != null ) {try {rs.close();} catch (Exception e) {}}
			if (stmt != null ) {try {stmt.close();} catch (Exception e) {}}
		}
		return kardexRespuesta;
	}
}
