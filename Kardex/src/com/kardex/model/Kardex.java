package com.kardex.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Kardex implements Serializable{
	private Integer idKardex;
	private Date fecha;
	private String producto;
	private String tipoOperacion;
	private Integer cantidadEntrada;
	private BigDecimal valorUnitarioEntrada;
	private BigDecimal valorTotalEntrada;
	private Integer cantidadSalida;
	private BigDecimal valorUnitarioSalida;
	private BigDecimal valorTotalSalida;
	private Integer cantidadSaldo;
	private BigDecimal valorUnitarioSaldo;
	private BigDecimal valorTotalSaldo;
	
	public Integer getIdKardex() {
		return idKardex;
	}
	public void setIdKardex(Integer idKardex) {
		this.idKardex = idKardex;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public Integer getCantidadEntrada() {
		return cantidadEntrada;
	}
	public void setCantidadEntrada(Integer cantidadEntrada) {
		this.cantidadEntrada = cantidadEntrada;
	}
	public BigDecimal getValorUnitarioEntrada() {
		return valorUnitarioEntrada;
	}
	public void setValorUnitarioEntrada(BigDecimal valorUnitarioEntrada) {
		this.valorUnitarioEntrada = valorUnitarioEntrada;
	}
	public BigDecimal getValorTotalEntrada() {
		return valorTotalEntrada;
	}
	public void setValorTotalEntrada(BigDecimal valorTotalEntrada) {
		this.valorTotalEntrada = valorTotalEntrada;
	}
	public Integer getCantidadSalida() {
		return cantidadSalida;
	}
	public void setCantidadSalida(Integer cantidadSalida) {
		this.cantidadSalida = cantidadSalida;
	}
	public BigDecimal getValorUnitarioSalida() {
		return valorUnitarioSalida;
	}
	public void setValorUnitarioSalida(BigDecimal valorUnitarioSalida) {
		this.valorUnitarioSalida = valorUnitarioSalida;
	}
	public BigDecimal getValorTotalSalida() {
		return valorTotalSalida;
	}
	public void setValorTotalSalida(BigDecimal valorTotalSalida) {
		this.valorTotalSalida = valorTotalSalida;
	}
	public Integer getCantidadSaldo() {
		return cantidadSaldo;
	}
	public void setCantidadSaldo(Integer cantidadSaldo) {
		this.cantidadSaldo = cantidadSaldo;
	}
	public BigDecimal getValorUnitarioSaldo() {
		return valorUnitarioSaldo;
	}
	public void setValorUnitarioSaldo(BigDecimal valorUnitarioSaldo) {
		this.valorUnitarioSaldo = valorUnitarioSaldo;
	}
	public BigDecimal getValorTotalSaldo() {
		return valorTotalSaldo;
	}
	public void setValorTotalSaldo(BigDecimal valorTotalSaldo) {
		this.valorTotalSaldo = valorTotalSaldo;
	}
	
	
}
