package com.kardex.servicios;

import java.math.BigDecimal;
import java.util.List;
import com.kardex.model.Kardex;

/**
 * 
 * @author JORGE.TRESPALACIOS
 *
 */
public interface IKardexServicios {
	
	public List<Kardex> consultarKardex(Kardex kardex);
	
	public int saveKardex(Kardex kardex);
	
	public BigDecimal calcularValorUnitarioEntrada(BigDecimal valorTotalEntrada, Integer cantidadEntrada);
	
	public BigDecimal calcularValorUnitarioSalida(Integer cantidadSalida, BigDecimal valorUnitarioSaldoAnterior);
	
	public BigDecimal calcularValorTotalSalida(Integer cantidadSalida, BigDecimal valorUnitarioSalida);
	
	public Integer calcularCantidadSaldo(Integer cantidadSaldoAnterior, Integer cantidadEntrada, Integer cantidadSalida);
	
	public BigDecimal calcularValorTotalSaldo(BigDecimal valorTotalSaldoAnterior, BigDecimal valorTotalEntrada, BigDecimal valorTotalSalida);
	
	public BigDecimal calcularValorUnitarioSaldo(BigDecimal valorTotalSaldo, Integer cantidadSaldo);

	
}