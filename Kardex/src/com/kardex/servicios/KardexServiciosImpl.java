package com.kardex.servicios;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.kardex.dao.KardexDao;
import com.kardex.model.Kardex;

/**
 * 
 * @author JORGE.TRESPALACIOS
 *
 */
public class KardexServiciosImpl implements IKardexServicios {

	private static final String RUTA_DB = "/Kardex/WebContent/WEB-INF/kardex.db";
	final Logger logger = Logger.getLogger(KardexServiciosImpl.class);
	
	/**
	 * Salvar Kardex
	 */
	public int saveKardex(Kardex kardex) {
		Connection sqlLite = null;
		KardexDao kardexDao = null;
		Kardex kardexAnterior = null;
		int respuesta = 0;
		try {
			Class.forName("org.sqlite.JDBC");
			sqlLite = DriverManager.getConnection(RUTA_DB);
			sqlLite.setAutoCommit(false);
			Kardex createdKardex = kardex;
			
			kardexDao = new KardexDao();
			
			kardexAnterior = kardexDao.ultimoKardex(sqlLite, createdKardex);
			
			if(kardexAnterior==null) {
				kardexAnterior = new Kardex();
			}
			
			createdKardex.setFecha(new Date());
			
			//Si es compra calculamos valor unitario entrada
			if(createdKardex.getTipoOperacion() !=null && createdKardex.getTipoOperacion().equals("Compra")) {
				createdKardex.setValorUnitarioEntrada(calcularValorUnitarioEntrada(createdKardex.getValorTotalEntrada(), createdKardex.getCantidadEntrada()));
			}
			
			//Si es venta calculamos valor unitario salida
			if(createdKardex.getTipoOperacion() !=null && createdKardex.getTipoOperacion().equals("Venta")) {
				createdKardex.setValorUnitarioSalida(calcularValorUnitarioSalida(createdKardex.getCantidadSalida(), 
						(kardexAnterior.getValorUnitarioSaldo()!=null?kardexAnterior.getValorUnitarioSaldo():new BigDecimal(0))));
				createdKardex.setValorTotalSalida(calcularValorTotalSalida(createdKardex.getCantidadSalida(), createdKardex.getValorUnitarioSalida()));
			}
			
			//Calculamos los Saldos
			createdKardex.setCantidadSaldo(calcularCantidadSaldo(kardexAnterior.getCantidadSaldo(), 
					createdKardex.getCantidadEntrada(), createdKardex.getCantidadSalida()));
			
			createdKardex.setValorTotalSaldo(calcularValorTotalSaldo(kardexAnterior.getValorTotalSaldo(),
					createdKardex.getValorTotalEntrada(), createdKardex.getValorTotalSalida()));
			
			createdKardex.setValorUnitarioSaldo(calcularValorUnitarioSaldo(createdKardex.getValorTotalSaldo(), 
					createdKardex.getCantidadSaldo()));
			
			//Realizamos la inserci�n 
			kardexDao.insertarKardex(sqlLite, createdKardex);
			sqlLite.commit();
		}catch(Exception e){
			logger.error(e.toString());	
			respuesta = 1;
			try {sqlLite.rollback();} catch (Exception ex) {}
			
		}finally {
			if (sqlLite != null ) {try {sqlLite.close();} catch (Exception e) {}}
		}
		return respuesta;
	}

	/**
	 * Consultar Kardex
	 */
	public List<Kardex> consultarKardex(Kardex kardex){
		Connection sqlLite = null;
		KardexDao kardexDao = new KardexDao();
		List<Kardex> listaKardex= new ArrayList<Kardex>();
		try {
			Class.forName("org.sqlite.JDBC");
			sqlLite = DriverManager.getConnection(RUTA_DB);
			listaKardex = kardexDao.consultarKardexs(sqlLite,kardex);
		}catch(Exception e){
			logger.error(e.toString());	
		}finally {
			if (sqlLite != null ) {try {sqlLite.close();} catch (Exception e) {}}
		}
		
		return listaKardex;
	}
	
	/**
	 * Calcular valor unitario de una compra
	 * @param valorTotalEntrada
	 * @param cantidadEntrada
	 * @return
	 */
	public BigDecimal calcularValorUnitarioEntrada(BigDecimal valorTotalEntrada, Integer cantidadEntrada) {
		BigDecimal respuesta = null;
		try {
			respuesta = valorTotalEntrada.divide(new BigDecimal(cantidadEntrada),2, RoundingMode.HALF_UP);
		}catch(ArithmeticException e) { //Validamos la excepci�n por divisi�n por cero
			respuesta = new BigDecimal(0);
			logger.error(e.toString());
		}
		return respuesta;
	}
	
	/**
	 * Calcular valor unitario en una venta
	 * @param cantidadSalida
	 * @param valorUnitarioSaldoAnterior
	 * @return
	 */
	public BigDecimal calcularValorUnitarioSalida(Integer cantidadSalida, BigDecimal valorUnitarioSaldoAnterior) {
		BigDecimal respuesta = null;
		if(cantidadSalida.doubleValue()>0) {
			respuesta = valorUnitarioSaldoAnterior;
		}else {
			respuesta = new BigDecimal(0);
		}
		return respuesta;
	}
	
	/**
	 * Calcular valor total en una venta
	 * @param cantidadSalida
	 * @param valorUnitarioSalida
	 * @return
	 */
	public BigDecimal calcularValorTotalSalida(Integer cantidadSalida, BigDecimal valorUnitarioSalida) {
		return new BigDecimal(cantidadSalida).multiply(valorUnitarioSalida);
	}
	
	/**
	 * Calcular cantidad saldo
	 * @param cantidadSaldoAnterior
	 * @param cantidadEntrada
	 * @param cantidadSalida
	 * @return
	 */
	public Integer calcularCantidadSaldo(Integer cantidadSaldoAnterior, Integer cantidadEntrada, Integer cantidadSalida) {
		
		if(cantidadSaldoAnterior == null) {
			cantidadSaldoAnterior = new Integer(0);
		}
		if(cantidadEntrada == null) {
			cantidadEntrada = new Integer(0);
		}
		if(cantidadSalida == null) {
			cantidadSalida = new Integer(0);
		}
		return cantidadSaldoAnterior+cantidadEntrada-cantidadSalida;
	}
	
	/**
	 * Calcular valor total saldo
	 * @param valorTotalSaldoAnterior
	 * @param valorTotalEntrada
	 * @param valorTotalSalida
	 * @return
	 */
	public BigDecimal calcularValorTotalSaldo(BigDecimal valorTotalSaldoAnterior, BigDecimal valorTotalEntrada, BigDecimal valorTotalSalida) {
		
		if(valorTotalSaldoAnterior == null) {
			valorTotalSaldoAnterior = new BigDecimal(0);
		}
		if(valorTotalEntrada == null) {
			valorTotalEntrada = new BigDecimal(0);
		}
		if(valorTotalSalida == null) {
			valorTotalSalida = new BigDecimal(0);
		}
		return valorTotalSaldoAnterior.add(valorTotalEntrada).subtract(valorTotalSalida);
	}
	
	/**
	 * Calcular valor unitario saldo
	 * @param valorTotalSaldo
	 * @param cantidadSaldo
	 * @return
	 */
	public BigDecimal calcularValorUnitarioSaldo(BigDecimal valorTotalSaldo, Integer cantidadSaldo) {
		BigDecimal respuesta = null;
		try {
			respuesta = valorTotalSaldo.divide(new BigDecimal(cantidadSaldo),2, RoundingMode.HALF_UP);
		}catch(ArithmeticException e) { //Validamos la excepci�n por divisi�n por cero
			respuesta = new BigDecimal(0);
			logger.error(e.toString());
		}
		return respuesta;
	}
	
}