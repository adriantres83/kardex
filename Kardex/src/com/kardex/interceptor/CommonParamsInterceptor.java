package com.kardex.interceptor;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.kardex.actions.CSRFTokenManager;


@Component
public class CommonParamsInterceptor extends HandlerInterceptorAdapter {


    //before the actual handler will be executed
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
    	
    	Object sessionIdBanco = request.getSession().getAttribute("idBanco");
    	
    	if(sessionIdBanco != null) {
    		Locale locale = null;
    		Long idBanco = Long.parseLong((String) sessionIdBanco);
    		String LOCALE = "es_CO";
    		if (LOCALE != null) {
    			locale = new Locale(LOCALE.substring(0, 2), LOCALE.substring(3, 5));
    			request.setAttribute("locale", locale);
    		}
    	}
    	
        if (!request.getMethod().equalsIgnoreCase("POST") ) {
        	if(request.getMethod().equalsIgnoreCase("PUT") || request.getMethod().equalsIgnoreCase("DELETE") || request.getMethod().equalsIgnoreCase("OPTIONS") ){
        		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Metodo prohibido.");
        		return false;
        	}
			// Not a POST - allow the request
			return true;
		} else {
			// This is a POST request - need to check the CSRF token
			String sessionToken = CSRFTokenManager.getTokenForSession(request.getSession());
			String requestToken = CSRFTokenManager.getTokenFromRequest(request);
			if(requestToken == null){
				requestToken = request.getHeader("_csrf_header");
			}
			if (sessionToken.equals(requestToken)) {
				return true;
			} else {
				response.sendError(HttpServletResponse.SC_FORBIDDEN, "Bad or missing CSRF value");
				return false;
			}
		}
    }

}