package com.kardex.security.provider;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Component;

@Component
public class TopSecretAuthenticationProvider implements AuthenticationProvider {

	private List<GrantedAuthority> authorities;
	private final SessionRegistry sessionRegistry;
	
	private final Log logger = LogFactory.getLog(getClass());
    
	public TopSecretAuthenticationProvider(SessionRegistry sessionRegistry) {
		this.sessionRegistry = sessionRegistry;
	}
	
	@Override
	public Authentication authenticate(Authentication auth) {
		String usuario = auth.getPrincipal().toString();
		String pss = auth.getCredentials().toString();
		
		String idBanco = ((ExtraParam)auth.getDetails()).getIdBanco();
		Long idBancoL = Long.parseLong(idBanco);
		
		
			return new UsernamePasswordAuthenticationToken(auth.getName(), auth.getCredentials(), authorities);
		
		//throw new BadCredentialsException("Username/Password does not match for " + auth.getPrincipal());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}


	/**
	 * @return the sessionRegistry
	 */
	public SessionRegistry getSessionRegistry() {
		return sessionRegistry;
	}

}