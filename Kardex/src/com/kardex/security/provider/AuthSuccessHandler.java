package com.kardex.security.provider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

/**
 * 
* Clase para el manejador de autenticación exitosa 
* 
* <p>
* Verifica la autenticación del usuario y reenvia a pagina de inicio.
* 
* 
* @author Sophos Solutions
* @since  Release 2 - Diciembre 2017 a Mayo 2018
*
 */

public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	
	/**
     * Verficia la autenticación del usuario y permisos que contiene para ser redirigido a la pantalla principal.
     *
     * @throws IOException, ServletException Delega el error.
     */
    public void onAuthenticationSuccess(HttpServletRequest request,
            HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    		Map permisos = (HashMap)request.getSession().getAttribute("mapMenuPermisos");
    		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
    		Set<String> setPermisos = permisos.keySet();
    		Map actions;
    		for(String key : setPermisos){
    			actions = (HashMap) permisos.get(key);
    			Set<String> setActions = ((HashMap) permisos.get(key)).keySet();
    			for(String action : setActions){
    				if(actions.get(action) != null)
    				if(actions.get(action).equals("S")){
    					authList.add(new SimpleGrantedAuthority(key+"_"+action));
    				}
    			}
    		}
    		SecurityContextHolder.getContext().setAuthentication(
    				new UsernamePasswordAuthenticationToken(auth.getName().toLowerCase(), auth.getCredentials(), authList));
           
    		response.sendRedirect("./irInicio.jsp");
    		
    }
 
}
