package com.kardex.security.provider;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.stereotype.Component;

@Component
public class ExtraParamSource implements AuthenticationDetailsSource<HttpServletRequest, ExtraParam> {
	
	@Override
	public ExtraParam buildDetails (HttpServletRequest context) {
	
		return new ExtraParam(context);
	}
}
