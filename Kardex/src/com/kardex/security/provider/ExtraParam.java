package com.kardex.security.provider;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

class ExtraParam extends WebAuthenticationDetails {

	private static final long serialVersionUID = 1L;
	private final String idBanco;
	
	public ExtraParam(HttpServletRequest request) {
	    super(request);
	    this.idBanco = request.getParameter("j_banco");
	}

	public String getIdBanco() {
		return idBanco;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = 31 * hash + ((idBanco == null) ? 0 : idBanco.hashCode());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExtraParam extraParam = (ExtraParam) obj;
		if (idBanco == null) {
			if (extraParam.idBanco != null)
				return false;
		} else if (!idBanco.equals(extraParam.idBanco))
			return false;
		return true;
	}
	
}

