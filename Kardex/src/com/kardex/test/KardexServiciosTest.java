package com.kardex.test;

import static org.junit.Assert.*;
import java.math.BigDecimal;
import org.junit.Test;
import com.kardex.servicios.KardexServiciosImpl;

/**
 * 
 * @author JORGE.TRESPALACIOS
 *
 */
public class KardexServiciosTest {

	KardexServiciosImpl KardexServiciosImpl;
	
	/**
	 * Calcular valor unitario de una compra
	 * @param valorTotalEntrada
	 * @param cantidadEntrada
	 * @return
	 */
	@Test
	public void probarCalcularValorUnitarioEntrada() {
		KardexServiciosImpl = new KardexServiciosImpl();
		assertTrue(KardexServiciosImpl.calcularValorUnitarioEntrada(new BigDecimal(15000000), 
				new Integer(5000)).doubleValue() == 3000);
		
	}
	
	/**
	 * Calcular valor unitario en una venta
	 * @param cantidadSalida
	 * @param valorUnitarioSaldoAnterior
	 * @return
	 */
	@Test
	public void probarCalcularValorUnitarioSalida() {
		KardexServiciosImpl = new KardexServiciosImpl();
		assertTrue(KardexServiciosImpl.calcularValorUnitarioSalida( 
				new Integer(1500), new BigDecimal(3000)).doubleValue() == 3000);
		
	}
	
	/**
	 * Calcular valor total en una venta
	 * @param cantidadSalida
	 * @param valorUnitarioSalida
	 * @return
	 */
	@Test
	public void probarCalcularValorTotalSalida() {
		KardexServiciosImpl = new KardexServiciosImpl();
		assertTrue(KardexServiciosImpl.calcularValorTotalSalida( 
				new Integer(1500), new BigDecimal(3000)).doubleValue() == 4500000);
	}
	
	/**
	 * Calcular cantidad saldo
	 * @param cantidadSaldoAnterior
	 * @param cantidadEntrada
	 * @param cantidadSalida
	 * @return
	 */
	@Test
	public void probarCalcularCantidadSaldo() {
		KardexServiciosImpl = new KardexServiciosImpl();
		assertTrue(KardexServiciosImpl.calcularCantidadSaldo( 
				new Integer(5000), null, new Integer(1500)).doubleValue() == 3500);
	}
	
	/**
	 * Calcular valor total saldo
	 * @param valorTotalSaldoAnterior
	 * @param valorTotalEntrada
	 * @param valorTotalSalida
	 * @return
	 */
	@Test
	public void probarCalcularValorTotalSaldo() {
		KardexServiciosImpl = new KardexServiciosImpl();
		assertTrue(KardexServiciosImpl.calcularValorTotalSaldo( 
				new BigDecimal(15000000), null, new BigDecimal(4500000)).doubleValue() == 10500000);
	}
	
	/**
	 * Calcular valor unitario saldo
	 * @param valorTotalSaldo
	 * @param cantidadSaldo
	 * @return
	 */
	@Test
	public void probarCalcularValorUnitarioSaldo() {
		KardexServiciosImpl = new KardexServiciosImpl();
		assertTrue(KardexServiciosImpl.calcularValorUnitarioSaldo( 
				new BigDecimal(16250000), new Integer(5500)).doubleValue() == 2954.55);
		
	}

}
