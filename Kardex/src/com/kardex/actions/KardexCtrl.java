package com.kardex.actions;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import java.util.LinkedList;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.SerializationUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.kardex.ajax.AjaxResponseBody;
import com.kardex.model.Kardex;
import com.kardex.validaciones.ValidadorKardex;
import com.kardex.servicios.IKardexServicios;

/**
 * Controlador de kardex
 * <p>
 * Consulta, crea y modifica los kardex.
 *
 * @author JORGE.TRESPALACIOS
 */
@RestController
@Scope("session")
public class KardexCtrl implements Controlador{

    private Kardex kardexFiltroPaginacion = new Kardex();
    private Kardex kardex;
    private Kardex kardexFiltro;
    int filas = 50;

    private final static String messages = "messages_es";
    private final static String application = "application";
    private static final String ocurrio_un_error_al_consultar_los_datos = "ocurrio_un_error_al_consultar_los_datos";
    ResourceBundle rbApplication = ResourceBundle.getBundle(application);
    ResourceBundle rb = ResourceBundle.getBundle(messages);
    
    final Logger logger = Logger.getLogger(KardexCtrl.class);
   
    @Autowired
	IKardexServicios IKardexServicios;
   
    /**
     * Bean para que se pueda poblar el formulario.<br>
     * El mismo que est� en el jsp: {@code <form:form commandName="kardex"></form> }
     *
     * @return Kardex
     */
    @ModelAttribute("kardex")
    public Kardex populateForm() {
        if (kardex == null) {
            kardex = new Kardex();
        }
        return kardex;
    }

    /**
     * Registramos el validador que se utilizar� en el formulario
     *
     * @param binder Enlaza el validador {@link ValidadorKardex}
     */
    @InitBinder
    protected void initBinder(WebDataBinder binder, HttpServletRequest request) {
    	binder.setValidator(new ValidadorKardex());
    }
    
    /**
     * Consulta todos los kardex para listarlos.
     *
     * @param request Necesario para {@link UtilsMenu} y para llamar el metodo consultarLista.
     * @return ModelAndView
     * @throws Exception Delega el error.
     */
    @RequestMapping(value = "/inicio")
    public ModelAndView inicio(HttpServletRequest request,HttpServletResponse response) throws Exception {
    	
        
        Map<String, Object> data = new HashMap<>();
    	
        try {
        
        	data.put("formulario", "/jsp/general/inicio.jsp");
            data.put("Titulo1", "Kardex");
            
        } catch (Exception e) {
        	logger.error(e.toString());
            data.put("error_type", "E");
            data.put("message", "Ocurrio un error");
        }
        return new ModelAndView(TEMPLATE, data);

    }
    

    /**
     * Consulta todos los kardex para listarlos.
     *
     * @param request Necesario para {@link UtilsMenu} y para llamar el metodo consultarLista.
     * @return ModelAndView
     * @throws Exception Delega el error.
     */
    @RequestMapping(value = "/kardexs", method = RequestMethod.POST)
    public ModelAndView consultarKardex(HttpServletRequest request,HttpServletResponse response) throws Exception {
    	
        
        Map<String, Object> data = new HashMap<>();
    	
        try {
        
        	data.put("formulario", "/jsp/general/inicio.jsp");
            
            
        } catch (Exception e) {
        	logger.error(e.toString());
            data.put("error_type", "E");
            data.put("message", "Ocurrio un error al consultar");
        }
        return new ModelAndView(TEMPLATE, data);

    }

    /**
     * Consulta todos los usuarios de la tabla T_AFC_USUARIO para listarlos
     *
     * @param request Obtiene los par�metro para la consulta.
     * @return Map
     */
    public Map<String, Object> consultarLista(HttpServletRequest request, boolean findAll) throws Exception {
        //Lista los kardex
        List<Kardex> lstKardex = new ArrayList<>();
        //Variable del paginador valor inicial 
        int iStartPage;
        //Total de p�ginas que se calcula con el contador de la consulta/n�mero de registros por p�gina
        int iUltimaPagina;
        //Variable del paginador valor final del rango de p�ginas s�lo muestra de a 10
        int iEndPage;

        //contador de la consulta
        Long lonTotalRegistrosConsulta = 0L;
        Long lonPage = 1L;
        Map<String, Object> data = new HashMap<>();        
        boolean hayDatosAMostrar = false;
        try {
        	if (!findAll) {
	            kardexFiltro = new Kardex();
	
	            // Producto
	            if (request.getParameter("producto") != null && !request.getParameter("producto").isEmpty()) {
	                kardexFiltro.setProducto(request.getParameter("producto"));
	                kardex.setProducto(request.getParameter("producto"));
	                data.put("producto", request.getParameter("producto"));
	                hayDatosAMostrar = true;
	            }
	            
	         // tipoOperacion
	            if (request.getParameter("tipoOperacion") != null && !request.getParameter("tipoOperacion").isEmpty()) {
	                kardexFiltro.setTipoOperacion(request.getParameter("tipoOperacion"));
	                kardex.setTipoOperacion(request.getParameter("tipoOperacion"));
	                data.put("tipoOperacion", request.getParameter("tipoOperacion"));
	                hayDatosAMostrar = true;
	            }
        	}
	
	        // LonPage
	        if (request.getParameter("lonPage") != null && !request.getParameter("lonPage").isEmpty()) {
	            lonPage = Long.valueOf(request.getParameter("lonPage"));
	        }
	
	        // Se consulta la lista de usuarios
	        if (request.getParameter("consultarTabla") != null && request.getParameter("consultarTabla").equals("S")) {
	        	
	        	// Si se va a paginar se restaura el filtro  
		        if (request.getParameter("consultaPaginacion") != null && request.getParameter("consultaPaginacion").equals("S")) { 
		        	kardexFiltro = (Kardex)SerializationUtils.clone(kardexFiltroPaginacion);  
		        } 
	 	         
		        // Se guarda el filtro antes de paginar  
		        else {  
		        	kardexFiltroPaginacion = (Kardex)SerializationUtils.clone(kardexFiltro); 
		        } 
		        
				lstKardex = IKardexServicios.consultarKardex(kardexFiltro);
				
	            data.put("consultarTabla", request.getParameter("consultarTabla"));
	        }
	
	        // Datos para la paginaci�n
	        iStartPage = (lonPage != null ? (int) (long) lonPage : 0) - 5 > 0 ? (lonPage != null ? (int) (long) lonPage : 0) - 5 : 1;
	        iUltimaPagina = ((int) (long) lonTotalRegistrosConsulta % filas == 0) ? ((int) (long) lonTotalRegistrosConsulta / filas) :
	                ((int) (long) lonTotalRegistrosConsulta / filas) + 1;
	        iEndPage = (iStartPage + 10) > iUltimaPagina ? iUltimaPagina : (iStartPage + 10);
	
	        // Se ponen los datos en el request para ser mostrado en el jsp
	        data.put("formulario", "/jsp/operaciones/kardex.jsp");
	        data.put("lstKardex", lstKardex);
		    data.put("iStartPage", iStartPage);
	        data.put("iEndPage", iEndPage);
	        data.put("lonTotalRegistrosConsulta", lonTotalRegistrosConsulta);
	        data.put("iUltimaPagina", iUltimaPagina);
	        data.put("lonPage", lonPage);
	        data.put("hayDatosAMostrar", hayDatosAMostrar);
	
	        request.getSession().setAttribute("iStartPage", iStartPage);
	        request.getSession().setAttribute("iEndPage", iEndPage);
	        request.getSession().setAttribute("iUltimaPagina", iUltimaPagina);
	        request.getSession().setAttribute("lonPage", lonPage);
	        request.getSession().setAttribute("lonTotalRegistrosConsulta", lonTotalRegistrosConsulta);
        }
        catch(Exception e) {
        	logger.error(e.toString());
        	data.put("error_type", "E");
            data.put("message", rb.getString(ocurrio_un_error_al_consultar_los_datos));
        }
        return data;
    }

    /**
     * Modifica los kardex.
     *
     * @param kardex nuevo o modificado.
     * @param result      Valida si hubo errores en las validaciones.
     * @return AjaxResponseBody
     * @throws Exception Delega el error.
     */
    @RequestMapping(value = "/agregar_modificar_kardex", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
    public @ResponseBody
    AjaxResponseBody modificar(@Valid @RequestBody Kardex kardex, BindingResult result, HttpServletRequest request) throws Exception {
    	AjaxResponseBody result2 = new AjaxResponseBody();

        try {

            if (!result.hasErrors()) {
            	
            	IKardexServicios.saveKardex(kardex);
                
            	result2.setError_type("C");
                result2.setMessage("Exitoso. <br> Datos guardados exitosamente.");
                result2.setStatus("SUCCESS");
                
            } else {
                result2.setError_type("E");
                result2.setMessage("Ocurrio un error al guardar");
                result2.setStatus("FAIL");
                result2.setResult(result.getAllErrors());
            }
        } catch (Exception e) {
        	logger.error(e.toString());						
            result2.setError_type("E");
            result2.setMessage("Ocurrio un error al guardar");
            result2.setStatus("FAIL");
            result2.setResult(result.getAllErrors());
        }
        return result2;
    }

}