package com.kardex.actions;

public interface Controlador {

	/**
	 * Ruta del jsp que se usa como template para la aplicacion. Dicho template
	 * permite reutilizar los elementos comunes a todas las paginas tales como
	 * el encabezado y la barra lateral de menu.
	 */
	public static final String TEMPLATE = "general/template";
	public static final String LOGIN = "general/login";
	
	public static final String CODIGO_RESPUESTA_EXITOSO = "00"; //C�digo de respuesta cuando toda la transacci�n fue exitosa
	public static final String CODIGO_RESPUESTA_EXITOSO_CORE = "000"; //C�digo de respuesta core cuando toda la transacci�n fue exitosa
	public static final String ESPACIO = " ";
}
