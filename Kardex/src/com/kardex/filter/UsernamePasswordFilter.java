package com.kardex.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.util.Assert;


import com.kardex.utils.UtilsModel;
import com.kardex.security.provider.ExtraParamSource;

public class UsernamePasswordFilter extends UsernamePasswordAuthenticationFilter {

	private static final Log log = LogFactory.getLog(UsernamePasswordFilter.class);
	private SessionAuthenticationStrategy sessionStrategy = new NullAuthenticatedSessionStrategy();
	private boolean continueChainBeforeSuccessfulAuthentication = false;
	private final SessionRegistry sessionRegistry;
	private ExtraParamSource ExtraParamSource;
	
	public UsernamePasswordFilter(SessionRegistry sessionRegistry) {
		Assert.notNull(sessionRegistry, "The sessionRegistry cannot be null");
		this.sessionRegistry = sessionRegistry;
	}

	@Autowired
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		
		String idBanco = "";
	     
		if (!requiresAuthentication(request, response)) {
			chain.doFilter(request, response);
			return;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Request is to process authentication");
		}

		Authentication authResult;
		
		try {
			
			HttpSession session2 = request.getSession();
			idBanco = request.getParameter("j_banco");
			
			
	        session2.setAttribute("idBanco", idBanco);
	       
			
			authResult = super.attemptAuthentication(request, response);
			
			if (authResult == null) {

				// return immediately as subclass has indicated that it hasn't completed authentication
				return;
			}
			
			final List<SessionInformation> sessions = sessionRegistry.getAllSessions(authResult.getPrincipal(), false);
			HttpSession session = request.getSession(false);
			if (session != null) {
				
				if(request.getParameter("j_banco")!=null)
					idBanco = request.getParameter("j_banco");
				
				session.setAttribute("idBanco", idBanco);
				
				// Only permit it though if this request is associated with one of the already registered sessions
				for (SessionInformation si : sessions) {
					if (!si.getSessionId().equals(session.getId())) {
						session.setAttribute("concurrentSession", "Ya exist�a una sesi�n activa para este usuario la cual fue cerrada autom�ticamente.");
					} 
				}
			}

			sessionStrategy.onAuthentication(authResult, request, response);

		} catch (AuthenticationException failed) {
			// Authentication failed
			unsuccessfulAuthentication(request, response, failed);
			return;
		}

		// Authentication success
		if (continueChainBeforeSuccessfulAuthentication) {
			chain.doFilter(request, response);
		}

		successfulAuthentication(request, response, chain, authResult);

		agregarEntradaLogAuditoria(authResult, UtilsModel.getClientIpAddress(request), idBanco);
	}

	/**
	 * Agrega la entrada del login al log de auditoria.
	 * 
	 * @param user 	String nombre de usuario
	 * @param ip 	Ip del usuario
	 */
	private void agregarEntradaLogAuditoria(Authentication authResult, String ip, String idBanco) {
		
		// Agregar entrada en log auditoria para el login.
		try {
			String user = "";
			
			if (authResult.getPrincipal() instanceof String) {
				user = (String) authResult.getPrincipal();
			} else if (authResult.getPrincipal() instanceof UserDetails) {
				user = ((UserDetails) authResult.getPrincipal()).getUsername();
			}
		
		} catch (Exception e) {
			log.error("Fall� la ejecucion en el m�todo  'agregarEntradaLogAuditoria' ", e);
		}
	}

	public void setSessionAuthenticationStrategy(SessionAuthenticationStrategy sessionStrategy) {
		this.sessionStrategy = sessionStrategy;
	}

	public void setContinueChainBeforeSuccessfulAuthentication(boolean continueChainBeforeSuccessfulAuthentication) {
		this.continueChainBeforeSuccessfulAuthentication = continueChainBeforeSuccessfulAuthentication;
	}

	public ExtraParamSource getExtraParamSource() {
		return ExtraParamSource;
	}

	public void setExtraParamSource(ExtraParamSource extraParamSource) {
		ExtraParamSource = extraParamSource;
	}
}
