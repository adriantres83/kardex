package com.kardex.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * 
* Clase para el control de accesos y filtros en el sistema.
* 
* <p>
* En esta clase se definen los controles y permisos que posee la navegación del sistema.
* 
* 
* @author Sophos Solutions
* @since  Release 2 - Diciembre 2017 a Mayo 2018
*
 */
public class FiltroCharset implements Filter {
	private String encoding;

	public void init(FilterConfig config) throws ServletException {
		encoding = config.getInitParameter("requestEncoding");

		if (encoding == null) {
			encoding = "UTF-8";
		}
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain next) throws IOException, ServletException {
		
		if (null == request.getCharacterEncoding()) {
			request.setCharacterEncoding(encoding);
		}
		setSecurityHeaders((HttpServletResponse)response);
        
		response.setContentType("text/html; charset=UTF-8");		
		next.doFilter(request, response);
	}
	 /**
	  * 
	 * Descripción de filtros de entrada y salida del sistema.
	 * @param response
	  */
	public void setSecurityHeaders(HttpServletResponse response) {
		response.setDateHeader("Expires", 0);
		response.setHeader("X-UA-Compatible", "IE=Edge"); 	
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate, max-age=0, post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        
        response.setHeader("X-Content-Type-Options", "nosniff");
        response.setHeader("X-Frame-Options", "SAMEORIGIN");
        response.setHeader("X-XSS-Protection", "1; mode=block");
        response.setHeader("X-Permitted-Cross-Domain-Policies", "master-only");
        
        response.setHeader("Access-Control-Allow-Origin", "SAMEORIGIN");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST");
        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        response.setHeader("Strict-Transport-Security", "max-age=31536000 ; includeSubDomains");
        response.setHeader("Content-Security-Policy", "default-src https: http:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https: http:; style-src http: https: 'unsafe-inline'; img-src *  data: https:; connect-src http: https: ws:;");
        
    }
	
	public void destroy() {
	}
}
