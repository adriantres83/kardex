package com.kardex.listener;

import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.kardex.actions.CSRFTokenManager;



/**
 * 
* Escuchador de session
* 
* <p>
* Implementación para recibir eventos de notificación sobre los cambios del ciclo de vida de HttpSession.
* 
* 
* @author Sophos Solutions
* @since  Release 2 - Diciembre 2017 a Mayo 2018
*
 */
public class SessionListener implements HttpSessionListener {
	// parametro de aplicacion - Tiempo de inactividad de sesion en minutos
	public static final String TIEMPO_INACTIVIDAD = "TIEMPO_INACTIVIDAD";

	// tiempo de inanctividad en minutos
	private static AtomicInteger inactiveInterval = new AtomicInteger(0);

	private static final Log log = LogFactory.getLog(SessionListener.class);

	// Valor por defecto si el parametro de aplicacion TIEMPO_INACTIVIDAD no
	// existe o no es numerico
	private static Integer VAL_TIEMPO_INACTIVIDAD;
	private static final Integer VAL_TIEMPO_INACTIVIDAD_DEFECTO = 20;

	static {

		try {
			ResourceBundle rbApplication = ResourceBundle.getBundle("application");
			// obtiene el valor del parametro de application.properties
			VAL_TIEMPO_INACTIVIDAD = Integer.parseInt(rbApplication.getString(TIEMPO_INACTIVIDAD));
			if (VAL_TIEMPO_INACTIVIDAD < 0) {
				VAL_TIEMPO_INACTIVIDAD = VAL_TIEMPO_INACTIVIDAD_DEFECTO;
			}
			// tiempo por defecto en caso de no encontrar valor del parametro en
			// el archivo properties o si es incorrecto
		} catch (Exception e) {
			VAL_TIEMPO_INACTIVIDAD = VAL_TIEMPO_INACTIVIDAD_DEFECTO;
			log.error("Error asignando VAL_TIEMPO_INACTIVIDAD, se deja valor por defecto", e);
		}
	}

	/**
	 * Se escucha la creacion de sesion para agregar el tiempo de inactividad.
	 * @param sessionEvent : evento de sesion
	 */
	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {
		// si el valor no esta inicializado lo consulta
		if (inactiveInterval == null || inactiveInterval.get() == 0) {
			// consultar parametro de aplicacion para determinar el tiempo de
			// inactividad en minutos
			populateInactiveInterval();
		}

		// asigna el tiempo de inactividad para la sesion. El valor de
		// inactiveInterval esta en minutos pero setMaxInactiveInterval asigna
		// el valor en segundos.
		sessionEvent.getSession().setMaxInactiveInterval(inactiveInterval.get() * 60);
		CSRFTokenManager.getTokenForSession(sessionEvent.getSession());
		log.info("[" + Calendar.getInstance().getTime() + "]Setting setMaxInactiveInterval - "
				+ (inactiveInterval.get() * 60) + " seconds - " + inactiveInterval.get() + " minute(s) for session "
				+ sessionEvent.getSession().getId() + ". Created at: "
				+ new Date(sessionEvent.getSession().getCreationTime()));
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		log.info("[" + Calendar.getInstance().getTime() + "]SessionDestroyed - session "
				+ sessionEvent.getSession().getId() + ". Created at: "
				+ new Date(sessionEvent.getSession().getCreationTime()));
	}

	/**
	 * Limpia el valor para forzar consultar nuevo valor si ha cambiado.
	 */
	public static void resetInactiveInterval() {
		if (inactiveInterval == null) {
			inactiveInterval = new AtomicInteger(0);
		} else {
			inactiveInterval.set(0);
		}

	}

	/**
	 * Busca el valor tiempo de inactividad
	 * 
	 * @return
	 */
	private void populateInactiveInterval() {
		
		if (inactiveInterval == null) {
			inactiveInterval = new AtomicInteger(0);
		}
		
		try {

			
			Integer valor = Integer.parseInt("60");
			if (valor < 0) {
				inactiveInterval.set(VAL_TIEMPO_INACTIVIDAD);
			} else {
				inactiveInterval.set(valor);
			}

		} catch (Exception e) {
			inactiveInterval.set(VAL_TIEMPO_INACTIVIDAD);
			log.error("Error asignando inactiveInterval, se deja valor por defecto", e);
		}

	}

}
