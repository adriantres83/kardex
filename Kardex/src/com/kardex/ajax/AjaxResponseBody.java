package com.kardex.ajax;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

public class AjaxResponseBody {
	String message;
	
	String error_type;
	
	private String status;
	
	 private List errorMessageList;
	
	private List result;
	
	private String lockVersion; 
	
	private BigDecimal codigo;

	public String getError_type() {
		return error_type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setError_type(String error_type) {
		this.error_type = error_type;
	}
	
	public List getErrorMessageList() {
		return this.errorMessageList;
	}
	public void setErrorMessageList(List errorMessageList) {
		this.errorMessageList = errorMessageList;
	}
	
	public String getStatus() {
	   return status;
	 }
	
	 public void setStatus(String status) {
	   this.status = status;
	 }

	public List getResult() {
		return result;
	}

	public void setResult(List result) {
		this.result = result;
	}

	public BigDecimal getCodigo() {
		return codigo;
	}

	public void setCodigo(BigDecimal codigo) {
		this.codigo = codigo;
	}

	public String getLockVersion() {
		return lockVersion;
	}

	public void setLockVersion(String lockVersion) {
		this.lockVersion = lockVersion;
	}
	 	 
}
