package com.kardex.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Clase que contiene funciones o acciones que se utilizan en varios lugares del
 * c�digo.
 *
 * @author JUAN.TORO
 */
public class UtilsModel {

	private static final Log log = LogFactory.getLog(UtilsModel.class);
	private static final String UNKNOWN = "unknown";

	@Autowired
	private HttpServletRequest context;

	/**
	 * Permite cerrar una conexion abierta a la base de datos
	 *
	 * @param conexion:
	 *            Objeto con la conexi�n a cerrar.
	 */
	public static void close_connection(Connection conexion) {
		try {
			if (conexion != null && !conexion.isClosed()) {
				conexion.close();
			}
		} catch (SQLException e) {

		}
	}

	/**
	 * Permite ensamblar el mensaje de respuesta de un procedimiento.
	 * <p>
	 * La respuesta puede ser: "OK", cuando el procedimiento se ejecuta
	 * correctamente; en caso contrario se concatenan las variables que dan
	 * trazabilidad al problema.
	 *
	 * @param onuRetval
	 *            : Valor de retorno
	 * @param onuErrCode
	 *            : C�digo de error
	 * @param ovcErrDesc
	 *            : Descripci�n del error
	 * @param ovcErrStatement
	 *            : Tipo de sentencia que present� el error
	 * @param ovcErrSqlCode
	 *            : Codigo de error ORACLE
	 * @param ovcErrTableName
	 *            : Nombre de la tabla
	 * @return Respuesta armada a partir del resultado del procedimiento llamado.
	 */
	public static String ws_response(Integer onuRetval, String onuErrCode, String ovcErrDesc, String ovcErrStatement,
			String ovcErrSqlCode, String ovcErrTableName) {

		// TODO: Como se maneja este tipo de respuestas en arquitecturas SOA.

		String result = "";

		if (onuRetval == 0) {
			result = "OK";
		}

		// Error funcional (1) o tecnico (-1)
		else {
			result = onuRetval + "|" + onuErrCode + "|" + ovcErrDesc + "|" + ovcErrStatement + "|" + ovcErrSqlCode + "|"
					+ ovcErrTableName;
		}

		return result;
	}

	/**
	 * Sumar y resta d�as a fechas
	 *
	 * @param fecha
	 *            *
	 * @param dias
	 *            *
	 * @return Date
	 */
	public static Date sumarRestarDiasFecha(Date fecha, int dias) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.add(Calendar.DAY_OF_YEAR, dias); // numero de d�as a a�adir, o
													// restar en caso de d�as
		return calendar.getTime(); // Devuelve el objeto Date con los nuevos
									// d�as a�adidos o restados
	}

	/**
	 * Funcion para convertir el formato String a Date tomando encuenta nulls Valida
	 * Formato ddMMyyyy
	 *
	 * @param fecha
	 *            *
	 * @return campo
	 */
	public static Date convertirFechaDate(String fecha, String formato_fecha) {
		if (fecha != null && !fecha.equals("")) {
			SimpleDateFormat format = new SimpleDateFormat(formato_fecha);
			try {
				return format.parse(fecha);
			} catch (ParseException e) {
				return null;
			}
		} else
			return null;
	}

	/**
	 * Convierte un objeto tipo Date a String Valida Formato ddMMyyyy (Tomado de la
	 * tabla de parametrizaci�n)
	 *
	 * @param fecha
	 * @param idBanco
	 * @return campo
	 */
	public static String convertirDateFecha(Date fecha, Long idBanco) {
		if (fecha != null && !String.valueOf(fecha).equals("")) {
			// Long idBanco = context
			
			String formato_fecha = "YYYY-MM-DD";

			SimpleDateFormat format = new SimpleDateFormat(formato_fecha);
			try {
				return format.format(fecha);
			} catch (Exception e) {
				return null;
			}
		} else
			return null;
	}

	/**
	 * Convierte un objeto tipo Date a String Valida Formato ddMMyyyy (Tomado de la
	 * tabla de parametrizaci�n)
	 *
	 * @param fecha
	 * @param idBanco
	 * @return campo
	 */
	public static String convertirDateFechaHora(Date fecha, Long idBanco) {
		if (fecha != null && !String.valueOf(fecha).equals("")) {
			// Long idBanco = context
			
			String formato_fecha = "YYYY-MM-DD HH:Mm:ss";

			SimpleDateFormat format = new SimpleDateFormat(formato_fecha);
			try {
				return format.format(fecha);
			} catch (Exception e) {
				return null;
			}
		} else
			return null;
	}

	/**
	 * Toma una cadena, y busca un caracter y lo cambia por otro
	 *
	 * @param streng
	 *            cadena inicial
	 * @param soeg
	 *            caracter a cambiar
	 * @param erstat
	 *            caracter que se reemplaza por el anterior
	 * @return String
	 */
	public static String replaceAll(String streng, String soeg, String erstat) {
		String st = null;

		if (streng != null && !streng.equals("")) {
			st = streng;
			if (soeg.length() == 0)
				return st;
			int idx = st.indexOf(soeg);
			while (idx >= 0) {
				st = st.substring(0, idx) + erstat + st.substring(idx + soeg.length());
				idx = st.indexOf(soeg);
			}
		}

		return st;
	}

	/**
	 * Ingresa a la fecha horas, minutos y segundos
	 *
	 * @param fecha
	 *            *
	 * @param horas
	 *            *
	 * @param minutos
	 *            *
	 * @param segundos
	 *            *
	 * @return Date
	 */
	public static Date addHoraFecha(Date fecha, int horas, int minutos, int segundos) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.set(Calendar.HOUR_OF_DAY, horas);
		calendar.set(Calendar.MINUTE, minutos);
		calendar.set(Calendar.SECOND, segundos);
		return calendar.getTime(); // Devuelve el objeto Date con los nuevos
									// d�as a�adidos o restados
	}

	/**
	 * Asigna el tipo de documento v�lido en el cat�logo
	 *
	 * @param tipoId
	 *            *
	 * @return String
	 */
	public static String cambiarTipoId(String tipoId) {
		String tipo;
		switch (tipoId) {
		case "NI":
			tipo = "FS003";
			break;
		case "CC":
			tipo = "FS001";
			break;
		case "CE":
			tipo = "FS002";
			break;
		case "TI":
			tipo = "FS004";
			break;
		case "PA":
			tipo = "FS005";
			break;
		case "CD":
			tipo = "FS000";
			break;
		case "RC":
			tipo = "FS009";
			break;
		case "NUI":
			tipo = "U";
			break;
		default:
			tipo = null;
			break;
		}
		return tipo;
	}

	/**
	 * Asigna el tipo de documento v�lido para los archivos planos de salida
	 *
	 * @param tipoId
	 *            *
	 * @return String
	 */
	public static String cambiarTipoIdRev(String tipoId) {
		String tipo;
		switch (tipoId) {
		case "FS003":
			tipo = "NI";
			break;
		case "FS001":
			tipo = "CC";
			break;
		case "FS002":
			tipo = "CE";
			break;
		case "FS004":
			tipo = "TI";
			break;
		default:
			tipo = "CC";
			break;
		}
		return tipo;
	}

	/**
	 * Asigna el origen de ahorro v�lido en el cat�logo
	 *
	 * @param origen
	 *            *
	 * @return String
	 */
	public static String cambiarOrigenAhorro(String origen) {
		if ("A".equals(origen)) { // Deducci�n n�mina
			return "D";
		}
		if ("B".equals(origen)) { // Dep�sito por venta de vivienda (Decreto
									// 2344)
			return "V";
		}
		if ("C".equals(origen)) { // Consignaci�n directa
			return "C";
		}
		if ("D".equals(origen)) { // Devoluci�n de GMF
			return "G";
		}
		return origen;
	}
	
	/**
	 * Asigna el origen de ahorro v�lido en el cat�logo
	 *
	 * @param origen
	 *            *
	 * @return String
	 */
	public static String cambiarOrigenAhorroInv(String origen) {
		if ("D".equals(origen)) { // Deducci�n n�mina
			return "A";
		}
		if ("V".equals(origen)) { // Dep�sito por venta de vivienda (Decreto2344)
			return "B";
		}
		if ("C".equals(origen)) { // Consignaci�n directa
			return "C";
		}
		if ("G".equals(origen)) { // Devoluci�n de GMF
			return "D";
		}
		return origen;
	}

	/**
	 * Aplica trim a todos los atributos de un objeto
	 *
	 * @param model
	 *            *
	 */
	public static void trimStringValues(Object model) {
		for (Field field : model.getClass().getDeclaredFields()) {
			try {
				field.setAccessible(true);
				Object value = field.get(model);
				if (value != null) {
					if (value instanceof String) {
						String trimmed = (String) value;
						field.set(model, trimmed.trim());
					}
				}
			} catch (Exception e) {
				log.error("El metodo 'trimStringValues' fall� ", e);
			}
		}
	}

	/**
	 * Entrega strings concatenados
	 *
	 * @param strings
	 *            *
	 * @return String
	 */
	public static String concat(String... strings) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < strings.length; i++)
			sb.append(strings[i]);
		return sb.toString();
	}

	/**
	 * Convierte un string con formato moneda a un BigDecimal, ej:
	 * <code>UtilsModel.parse("$30,000,000.00", Locale.ENGLISH)</code>
	 *
	 * @param amount
	 *            Valor con un formato de moneda
	 * @param locale
	 *            Localizaci�n que est� usando el valor.
	 * @return BigDecimal
	 * @throws ParseException
	 *             Delega el error
	 */
	public static BigDecimal parse(final String amount, final Locale locale) throws ParseException {
		final NumberFormat format = NumberFormat.getNumberInstance(locale);
		if (format instanceof DecimalFormat) {
			((DecimalFormat) format).setParseBigDecimal(true);
		}
		return (BigDecimal) format.parse(amount.replaceAll("[^\\d.,]", ""));
	}

	/**
	 * Convierte un string con formato moneda a un BigDecimal
	 *
	 * @param amount
	 *            *
	 * @param locale
	 *            *
	 * @return BigDecimal
	 * @throws ParseException
	 *             Delega el error
	 */
	public static BigDecimal parseNumber(final String amount, final Locale locale) {
		NumberFormat format = null;
		try {
			format = NumberFormat.getNumberInstance(locale);
			if (format instanceof DecimalFormat) {
				((DecimalFormat) format).setParseBigDecimal(true);
			}
			return (BigDecimal) format.parse(amount.replaceAll("[^\\d.,]", ""));
		} catch (ParseException e) {
			return new BigDecimal(amount);
		}
	}

	/**
	 * Funci�n para asignar el punto decimal a un n�mero que viene en una cadena de
	 * caracteres
	 *
	 * @param valor
	 *            *
	 * @param precision
	 *            *
	 * @return BigDecimal
	 */
	public static BigDecimal toDecimal(String valor, int precision) {
		while (valor.length() < precision) {
			valor = "0" + valor;
		}
		valor = new StringBuilder(valor).insert(valor.length() - precision, ".").toString();
		return new BigDecimal(valor);
	}

	/**
	 * Funci�n para insertar el punto decimal a un n�mero que viene en una cadena de
	 * caracteres
	 *
	 * @param valor
	 *            *
	 * @param precision
	 *            *
	 * @return String
	 */
	public static String setPuntoDecimal(String valor, int precision) {
		while (valor.length() <= precision) {
			valor = "0" + valor;
		}
		return new StringBuilder(valor).insert(valor.length() - precision, ".").toString();
	}

	// Suma o resta las horas recibidos a la fecha
	public static Date sumarRestarHorasMinutoSegundosFecha(Date fecha, int horas, int minutos, int segundos) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.add(Calendar.HOUR, horas); // numero de horas a a�adir o restar
											// en caso de horas<0
		calendar.add(Calendar.MINUTE, minutos); // numero de horas a a�adir o
												// restar en caso de horas<0
		calendar.add(Calendar.SECOND, segundos); // numero de horas a a�adir o
													// restar en caso de horas<0
		return calendar.getTime(); // Devuelve el objeto Date con las nuevas
									// horas a�adidas
	}

	/**
	 * Funci�n para rellenar un campo num�rico con ceros a la izquierda y decimales
	 *
	 * @param valor
	 *            *
	 * @param longitud
	 *            *
	 * @return String
	 */
	public static String completarCeros(String valor, String longitud) {
		String[] tamano = longitud.split(",");
		int cantidad;
		int decimal;
		try {
			cantidad = Integer.parseInt(tamano[0]);
			decimal = Integer.parseInt(tamano[1]);

		} catch (NumberFormatException nfe) {
			log.error("El metodo 'completarCeros' fall� ", nfe);
			cantidad = 20;
			decimal = 4;
		}
		if (valor.contains(".")) {
			decimal -= valor.substring(valor.indexOf(".") + 1, valor.length()).length();
			valor = valor.replace(".", "");
		}
		if (decimal > 0 && valor.length() < cantidad - decimal) {
			for (int i = 0; i < decimal; i++) {
				valor += "0";
			}
		}
		while (valor.length() < cantidad) {
			valor = "0" + valor;
		}
		return valor;
	}

	/**
	 * Obtiene el stack del error contenido en el objeto Throwable.
	 *
	 * @param aThrowable
	 *            *
	 * @return String
	 */
	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	/**
	 * Permite validar un valor BigDecimal si es nulo retorna cero y trunca al
	 * numero de decimales
	 *
	 * @param valor
	 *            *
	 * @param decimales
	 *            *
	 * @return BigDecimal
	 */
	public static BigDecimal truncarBigDecimal(BigDecimal valor, int decimales) {
		return valor != null ? valor.setScale(decimales, RoundingMode.DOWN) : new BigDecimal(0);
	}

	/**
	 * Permite remover los caracteres especiales de la cadena proporcionada
	 *
	 * @param cadena
	 *            *
	 * @return String
	 */
	public static String removerCaracteresNoPermitidos(String cadena) {
		if (cadena != null && !cadena.isEmpty()) {
			String expresion_regular = "[^0-9A-Za-z��������������#$/\\()��+*;,:._\\-\\? ]";
			cadena = cadena.replaceAll(expresion_regular, "");
		}
		return cadena;
	}

	/**
	 * Permite obtener una fecha sin hh:mm:ss
	 *
	 * @param date
	 *            *
	 * @return Date
	 */
	public static Date getDateOnly(Date date) {
		String fecha_dia = new SimpleDateFormat("yyyy-MM-dd").format(date);
		String[] fecha_separada = fecha_dia.split("-");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MILLISECOND, 0);

		// Se setea la fecha al calendar con hh:mm:ss en cero
		calendar.set(Integer.parseInt(fecha_separada[0]), Integer.parseInt(fecha_separada[1]) - 1,
				Integer.parseInt(fecha_separada[2]), 0, 0, 0);

		return calendar.getTime();
	}

	/**
	 * Permite rellenar por la derecha con el valor de relleno especificado
	 *
	 * @param valor
	 *            *
	 * @param relleno
	 *            *
	 * @return String
	 */
	public static String rpad(String valor, int tamano, String relleno) {
		int tamano_relleno = tamano - valor.length();
		if (valor.length() < tamano) {
			for (int i = 0; i < tamano_relleno; i++) {
				valor += relleno;
			}
		}
		return valor;
	}

	/**
	 * Permite rellenar por la izquierda con el valor de relleno especificado
	 *
	 * @param valor
	 *            *
	 * @param relleno
	 *            *
	 * @return String
	 */
	public static String lpad(String valor, int tamano, String relleno) {
		int tamano_relleno = tamano - valor.length();
		if (valor.length() < tamano) {
			for (int i = 0; i < tamano_relleno; i++) {
				valor = relleno + valor;
			}
		}
		return valor;
	}

	
	
	/**
	 * Permite obtener la direcci�n IP del usuario incluso de una redirecci�n de un
	 * balanceador
	 * 
	 * @param request
	 * @return IP address
	 */
	public static String getClientIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	
	/**
	 * Permite anhadir el numero de dias especificado a la fecha dada.
	 * 
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date addDaysToDate(Date date, int days) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, days);
		return c.getTime();
	}
}
