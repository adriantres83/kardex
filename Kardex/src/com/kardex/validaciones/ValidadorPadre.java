package com.kardex.validaciones;

import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
* Clase para validaci�n de campos.
* 
* <p>
* Esta clase valida los campos que vienen desde el formulario.
* 
* 
*@author JORGE.TRESPALACIOS
*
 */
public class ValidadorPadre {
	 
	protected Pattern pattern;
	protected Matcher matcher;
	 
	ResourceBundle rbApplication = ResourceBundle.getBundle("application");
	
	protected Long idBanco;
	protected static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	 protected String ID_PATTERN = "[0-9]+";
	 protected String STRING_SOLOLETRAS_PATTERN = "^[A-Za-z��������������_\\s]+$";
	 protected String STRING_ID_ALFANUMERICO_PATTERN = "^[A-Za-z0-9]+$";
	 protected String STRING_PATTERN;
	 protected String MOBILE_PATTERN = "[0-9]{10}";
	 protected String CODIGO_SUCURSAL_PATE;
	 protected String DECIMAL_PATTERN = "^[0-9]+([,][0-9]+)?$";
	 protected String DECIMAL = ".";
	 protected String DECIMAL_OPCIONAL_PATTERN = "^\\d+(?:["+DECIMAL+"]|["+DECIMAL+"]\\d+)?$";
	 protected String ENTERO_PATTERN = "^\\d+$";
	 protected String CERO_A_UNO_PATTERN = "^(?:0(?:["+DECIMAL+"]|["+DECIMAL+"]\\d+)?|1)$";
	 protected String CERO_A_CIEN_PATTERN = "^[0-9][0-9]?$|^100$";
	 protected String UNO_A_CIEN_PATTERN = "^[1-9][0-9]?$|^100$";
	
	 
	 public Long getIdBanco() {
		return idBanco;
	}
	
	public void setIdBanco(Long idBanco) {
		this.idBanco = idBanco;
	}
}
