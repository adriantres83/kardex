package com.kardex.validaciones;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.kardex.dao.KardexDao;
import com.kardex.model.Kardex;

/**
 * 
* Clase para validación de campos.
* 
* <p>
* Esta clase valida los campos que vienen desde el formulario.
* 
* 
* @author Jorge Trespalacios
* @since  Diciembre 2018
*
 */
public class ValidadorKardex implements Validator {
	
    public static final String INGRESE_VALOR_NUMERICO = "ingrese_valor_numerico";
    public static final String INGRESE_VALOR_CARACTER = "ingrese_valor_caracter";
    private static final String RUTA_DB = "/Kardex/WebContent/WEB-INF/kardex.db";
    
    private static final String producto = "producto";
    private static final String kardex_producto_required = "kardex_producto_required";
    private static final String tipoOperacion = "tipoOperacion";
    private static final String kardex_tipoOperacion_required = "kardex_tipoOperacion_required";
    private static final String cantidadSalida = "cantidadSalida"; 
    private static final String kardex_validarCantidadAlVender = "kardex_validarCantidadAlVender";
    private static final String cantidadEntrada = "cantidadEntrada";
    private static final String kardex_cantidadEntrada_required = "kardex_cantidadEntrada_required";
    private static final String valorTotalEntrada = "valorTotalEntrada";
    private static final String kardex_valorTotalEntrada_required = "kardex_valorTotalEntrada_required";
    private static final String kardex_cantidadSalida_required = "kardex_cantidadSalida_required";
    
    ResourceBundle rb = ResourceBundle.getBundle("messages_es");
    final Logger logger = Logger.getLogger(ValidadorKardex.class);

    public ValidadorKardex() {
    	
    }

	@Override
    public boolean supports(Class<?> clazz) {
        return Kardex.class.equals(clazz); // clase del bean al que da soporte este validador
    }

    @Override
    public void validate(Object target, Errors errors) {
    	try {
	        Kardex kardex = (Kardex) target;
	
	        // El producto es obligatorio
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, producto, rb.getString(kardex_producto_required));
	        
	     // El tipo operación es obligatorio
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, tipoOperacion, rb.getString(kardex_tipoOperacion_required));
	       
	        if(kardex.getTipoOperacion()!=null && kardex.getTipoOperacion().equals("Comprar")) {
	        	// La cantidad de entrada es obligatorio
		        ValidationUtils.rejectIfEmptyOrWhitespace(errors, cantidadEntrada, rb.getString(kardex_cantidadEntrada_required));
		        // El Valor Total Entrada es obligatorio
		        ValidationUtils.rejectIfEmptyOrWhitespace(errors, valorTotalEntrada, rb.getString(kardex_valorTotalEntrada_required));
	        }
	        
	        if(kardex.getTipoOperacion()!=null && kardex.getTipoOperacion().equals("Vender")) {
	        	// La cantidad de salida es obligatorio
		        ValidationUtils.rejectIfEmptyOrWhitespace(errors, cantidadSalida, rb.getString(kardex_cantidadSalida_required));
		        
	        }
	        
	        validarCantidadAlVender(kardex, errors); // 
    	}
    	catch(Exception e) {
    		logger.error(e.toString());
    	}

    }

   
    /**
     * Validamos que la cantidad solicitada al vender no supere lo disponible
     * @param kardex
     * @param errors
     * @throws Exception
     */
    private void validarCantidadAlVender(Kardex kardex, Errors errors) throws Exception{
    	Connection sqlLite = null;
        KardexDao kardexDao = new KardexDao();
        Integer cantidadDisponible = null;
        try {
        	sqlLite = DriverManager.getConnection(RUTA_DB);
        
        	cantidadDisponible = kardexDao.cantidadSaldo(sqlLite, kardex);
        			
        	if(cantidadDisponible-kardex.getCantidadSalida()<0) {
        		errors.rejectValue(cantidadSalida, rb.getString(kardex_validarCantidadAlVender)+cantidadDisponible);
        	}
        
        }catch(Exception e) {
    		logger.error(e.toString());
    	}finally {
        	if (sqlLite != null ) {try {sqlLite.close();} catch (Exception e) {}}
        }
    }
}